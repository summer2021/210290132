/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.service.queue;

import org.apache.dolphinscheduler.service.exceptions.TaskPriorityQueueException;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
=======
package org.apache.dolphinscheduler.service.queue;

import java.util.concurrent.PriorityBlockingQueue;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

import org.springframework.stereotype.Service;

/**
<<<<<<< HEAD
 * A singleton of a task queue implemented using PriorityBlockingQueue
=======
 * A singleton of a task queue implemented with zookeeper
 * tasks queue implementation
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
 */
@Service
public class TaskPriorityQueueImpl implements TaskPriorityQueue<TaskPriority> {
    /**
     * queue size
     */
    private static final Integer QUEUE_MAX_SIZE = 3000;

    /**
     * queue
     */
    private PriorityBlockingQueue<TaskPriority> queue = new PriorityBlockingQueue<>(QUEUE_MAX_SIZE);

    /**
     * put task takePriorityInfo
     *
     * @param taskPriorityInfo takePriorityInfo
<<<<<<< HEAD
     * @throws TaskPriorityQueueException
     */
    @Override
    public void put(TaskPriority taskPriorityInfo) throws TaskPriorityQueueException {
=======
     */
    @Override
    public void put(TaskPriority taskPriorityInfo) {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        queue.put(taskPriorityInfo);
    }

    /**
     * take taskInfo
     *
     * @return taskInfo
<<<<<<< HEAD
     * @throws TaskPriorityQueueException
     */
    @Override
    public TaskPriority take() throws TaskPriorityQueueException, InterruptedException {
=======
     */
    @Override
    public TaskPriority take() throws InterruptedException {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return queue.take();
    }

    /**
<<<<<<< HEAD
     * poll taskInfo with timeout
     *
     * @param timeout
     * @param unit
     * @return
     * @throws TaskPriorityQueueException
     * @throws InterruptedException
     */
    @Override
    public TaskPriority poll(long timeout, TimeUnit unit) throws TaskPriorityQueueException, InterruptedException {
        return queue.poll(timeout,unit);
    }

    /**
     * queue size
     *
     * @return size
     * @throws TaskPriorityQueueException
     */
    @Override
    public int size() throws TaskPriorityQueueException {
=======
     * queue size
     *
     * @return size
     */
    @Override
    public int size() {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return queue.size();
    }
}
