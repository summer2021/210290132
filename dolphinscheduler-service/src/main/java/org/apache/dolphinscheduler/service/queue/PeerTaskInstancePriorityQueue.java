/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.service.queue;

import org.apache.dolphinscheduler.dao.entity.TaskInstance;
import org.apache.dolphinscheduler.service.exceptions.TaskPriorityQueueException;
=======
package org.apache.dolphinscheduler.service.queue;


import org.apache.dolphinscheduler.dao.entity.TaskInstance;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;
<<<<<<< HEAD
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

/**
 * Task instances priority queue implementation
 * All the task instances are in the same process instance.
 */
public class PeerTaskInstancePriorityQueue implements TaskPriorityQueue<TaskInstance> {
    /**
     * queue size
     */
    private static final Integer QUEUE_MAX_SIZE = 3000;

    /**
     * queue
     */
    private PriorityQueue<TaskInstance> queue = new PriorityQueue<>(QUEUE_MAX_SIZE, new TaskInfoComparator());

    /**
<<<<<<< HEAD
     * Lock used for all public operations
     */
    private final ReentrantLock lock = new ReentrantLock(true);

    /**
     * put task instance to priority queue
     *
     * @param taskInstance taskInstance
     * @throws TaskPriorityQueueException
     */
    @Override
    public void put(TaskInstance taskInstance) throws TaskPriorityQueueException {
=======
     * put task instance to priority queue
     *
     * @param taskInstance taskInstance
     * @throws Exception
     */
    public void put(TaskInstance taskInstance) throws Exception {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        queue.add(taskInstance);
    }

    /**
     * take task info
<<<<<<< HEAD
     *
     * @return task instance
     * @throws TaskPriorityQueueException
     */
    @Override
    public TaskInstance take() throws TaskPriorityQueueException {
=======
     * @return task instance
     * @throws Exception
     */
    @Override
    public TaskInstance take() throws Exception {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return queue.poll();
    }

    /**
<<<<<<< HEAD
     * poll task info with timeout
     * <p>
     * WARN: Please use PriorityBlockingQueue if you want to use poll(timeout, unit)
     * because this method of override interface used without considering accuracy of timeout
     *
     * @param timeout
     * @param unit
     * @return
     * @throws TaskPriorityQueueException
     * @throws InterruptedException
     */
    @Override
    public TaskInstance poll(long timeout, TimeUnit unit) throws TaskPriorityQueueException {
        throw new TaskPriorityQueueException("This operation is not currently supported and suggest to use PriorityBlockingQueue if you want！");
    }

    /**
     * peek taskInfo
     *
     * @return task instance
     */
    public TaskInstance peek() {
=======
     * peek taskInfo
     *
     * @return task instance
     * @throws Exception
     */
    public TaskInstance peek() throws Exception {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return queue.peek();
    }

    /**
     * queue size
     *
     * @return size
     */
<<<<<<< HEAD
    @Override
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public int size() {
        return queue.size();
    }

    /**
     * whether contains the task instance
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param taskInstance task instance
     * @return true is contains
     */
    public boolean contains(TaskInstance taskInstance) {
        return queue.contains(taskInstance);
    }

<<<<<<< HEAD
    public boolean contains(int taskId) {

        Iterator<TaskInstance> iterator = this.queue.iterator();
        while (iterator.hasNext()) {
            TaskInstance taskInstance = iterator.next();
            if (taskId == taskInstance.getId()) {
                return true;
            }
        }
        return false;

    }

    /**
     * remove task
     *
     * @param taskInstance task instance
     * @return true if remove success
     */
    public boolean remove(TaskInstance taskInstance) {
=======
    /**
     * remove task
     * @param taskInstance task instance
     * @return true if remove success
     * @throws Exception
     */
    public boolean remove(TaskInstance taskInstance) throws Exception {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return queue.remove(taskInstance);
    }

    /**
     * get iterator
<<<<<<< HEAD
     *
     * @return Iterator
     */
    public Iterator<TaskInstance> iterator() {
=======
     * @return Iterator
     */
    public Iterator iterator(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return queue.iterator();
    }

    /**
     * TaskInfoComparator
     */
    private class TaskInfoComparator implements Comparator<TaskInstance> {

        /**
         * compare o1 o2
         *
         * @param o1 o1
         * @param o2 o2
         * @return compare result
         */
        @Override
        public int compare(TaskInstance o1, TaskInstance o2) {
            return o1.getTaskInstancePriority().compareTo(o2.getTaskInstancePriority());
        }
    }
}
