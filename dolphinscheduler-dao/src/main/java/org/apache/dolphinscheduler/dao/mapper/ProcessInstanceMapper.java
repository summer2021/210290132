/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.common.enums.ExecutionStatus;
import org.apache.dolphinscheduler.dao.entity.ExecuteStatusCount;
import org.apache.dolphinscheduler.dao.entity.ProcessInstance;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * process instance mapper interface
 */
public interface ProcessInstanceMapper extends BaseMapper<ProcessInstance> {

    /**
     * query process instance detail info by id
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param processId processId
     * @return process instance
     */
    ProcessInstance queryDetailById(@Param("processId") int processId);

    /**
     * query process instance by host and stateArray
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param host host
     * @param stateArray stateArray
     * @return process instance list
     */
    List<ProcessInstance> queryByHostAndStatus(@Param("host") String host,
                                               @Param("states") int[] stateArray);

    /**
     * query process instance by tenantId and stateArray
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param tenantId tenantId
     * @param states states array
     * @return process instance list
     */
    List<ProcessInstance> queryByTenantIdAndStatus(@Param("tenantId") int tenantId,
<<<<<<< HEAD
                                                   @Param("states") int[] states);

    /**
=======
                                               @Param("states") int[] states);

    /**
     * query process instance by worker group and stateArray
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param workerGroupName workerGroupName
     * @param states states array
     * @return process instance list
     */
    List<ProcessInstance> queryByWorkerGroupNameAndStatus(@Param("workerGroupName") String workerGroupName,
                                                          @Param("states") int[] states);

    /**
     * process instance page
     * @param page page
     * @param projectId projectId
     * @param processDefinitionId processDefinitionId
     * @param searchVal searchVal
<<<<<<< HEAD
     * @param executorId executorId
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param statusArray statusArray
     * @param host host
     * @param startTime startTime
     * @param endTime endTime
     * @return process instance IPage
     */

    /**
     * process instance page
<<<<<<< HEAD
     *
     * @param page page
     * @param projectCode projectCode
     * @param processDefinitionCode processDefinitionCode
=======
     * @param page page
     * @param projectId projectId
     * @param processDefinitionId processDefinitionId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param searchVal searchVal
     * @param executorId executorId
     * @param statusArray statusArray
     * @param host host
     * @param startTime startTime
     * @param endTime endTime
     * @return process instance page
     */
    IPage<ProcessInstance> queryProcessInstanceListPaging(Page<ProcessInstance> page,
<<<<<<< HEAD
                                                          @Param("projectCode") Long projectCode,
                                                          @Param("processDefinitionCode") Long processDefinitionCode,
=======
                                                          @Param("projectId") int projectId,
                                                          @Param("processDefinitionId") Integer processDefinitionId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                                                          @Param("searchVal") String searchVal,
                                                          @Param("executorId") Integer executorId,
                                                          @Param("states") int[] statusArray,
                                                          @Param("host") String host,
                                                          @Param("startTime") Date startTime,
                                                          @Param("endTime") Date endTime);

    /**
     * set failover by host and state array
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param host host
     * @param stateArray stateArray
     * @return set result
     */
    int setFailoverByHostAndStateArray(@Param("host") String host,
                                       @Param("states") int[] stateArray);

    /**
     * update process instance by state
<<<<<<< HEAD
     *
     * @param originState originState
=======
     * @param originState  originState
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param destState destState
     * @return update result
     */
    int updateProcessInstanceByState(@Param("originState") ExecutionStatus originState,
                                     @Param("destState") ExecutionStatus destState);

    /**
<<<<<<< HEAD
     * update process instance by tenantId
     *
=======
     *  update process instance by tenantId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param originTenantId originTenantId
     * @param destTenantId destTenantId
     * @return update result
     */
    int updateProcessInstanceByTenantId(@Param("originTenantId") int originTenantId,
                                        @Param("destTenantId") int destTenantId);

    /**
<<<<<<< HEAD
     * update process instance by worker groupId
     *
=======
     * update process instance by worker group name
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param originWorkerGroupName originWorkerGroupName
     * @param destWorkerGroupName destWorkerGroupName
     * @return update result
     */
    int updateProcessInstanceByWorkerGroupName(@Param("originWorkerGroupName") String originWorkerGroupName,
                                               @Param("destWorkerGroupName") String destWorkerGroupName);

    /**
     * count process instance state by user
<<<<<<< HEAD
     *
     * @param startTime startTime
     * @param endTime endTime
     * @param projectCodes projectCodes
=======
     * @param startTime startTime
     * @param endTime endTime
     * @param projectIds projectIds
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return ExecuteStatusCount list
     */
    List<ExecuteStatusCount> countInstanceStateByUser(
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime,
<<<<<<< HEAD
            @Param("projectCodes") Long[] projectCodes);

    /**
     * query process instance by processDefinitionCode
     *
     * @param processDefinitionCode processDefinitionCode
     * @param size size
     * @return process instance list
     */
    List<ProcessInstance> queryByProcessDefineCode(@Param("processDefinitionCode") Long processDefinitionCode,
                                                   @Param("size") int size);

    /**
     * query last scheduler process instance
     *
     * @param definitionCode definitionCode
=======
            @Param("projectIds") Integer[] projectIds);

    /**
     * query process instance by processDefinitionId
     * @param processDefinitionId processDefinitionId
     * @param size size
     * @return process instance list
     */
    List<ProcessInstance> queryByProcessDefineId(
            @Param("processDefinitionId") int processDefinitionId,
            @Param("size") int size);

    /**
     * query last scheduler process instance
     * @param definitionId processDefinitionId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param startTime startTime
     * @param endTime endTime
     * @return process instance
     */
<<<<<<< HEAD
    ProcessInstance queryLastSchedulerProcess(@Param("processDefinitionCode") Long definitionCode,
=======
    ProcessInstance queryLastSchedulerProcess(@Param("processDefinitionId") int definitionId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                                              @Param("startTime") Date startTime,
                                              @Param("endTime") Date endTime);

    /**
     * query last running process instance
<<<<<<< HEAD
     *
     * @param definitionCode definitionCode
=======
     * @param definitionId definitionId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param startTime startTime
     * @param endTime endTime
     * @param stateArray stateArray
     * @return process instance
     */
<<<<<<< HEAD
    ProcessInstance queryLastRunningProcess(@Param("processDefinitionCode") Long definitionCode,
=======
    ProcessInstance queryLastRunningProcess(@Param("processDefinitionId") int definitionId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                                            @Param("startTime") Date startTime,
                                            @Param("endTime") Date endTime,
                                            @Param("states") int[] stateArray);

    /**
     * query last manual process instance
<<<<<<< HEAD
     *
     * @param definitionCode definitionCode
=======
     * @param definitionId definitionId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param startTime startTime
     * @param endTime endTime
     * @return process instance
     */
<<<<<<< HEAD
    ProcessInstance queryLastManualProcess(@Param("processDefinitionCode") Long definitionCode,
                                           @Param("startTime") Date startTime,
                                           @Param("endTime") Date endTime);

    /**
     * query top n process instance order by running duration
     *
     * @param status process instance status
     * @return ProcessInstance list
     */

    List<ProcessInstance> queryTopNProcessInstance(@Param("size") int size,
                                                   @Param("startTime") Date startTime,
                                                   @Param("endTime") Date endTime,
                                                   @Param("status") ExecutionStatus status);

    /**
     * query process instance by processDefinitionCode and stateArray
     *
     * @param processDefinitionCode processDefinitionCode
     * @param states states array
     * @return process instance list
     */

    List<ProcessInstance> queryByProcessDefineCodeAndStatus(@Param("processDefinitionCode") Long processDefinitionCode,
                                                            @Param("states") int[] states);

    int updateGlobalParamsById(@Param("globalParams") String globalParams,
                               @Param("id") int id);
=======
    ProcessInstance queryLastManualProcess(@Param("processDefinitionId") int definitionId,
                                           @Param("startTime") Date startTime,
                                           @Param("endTime") Date endTime);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
