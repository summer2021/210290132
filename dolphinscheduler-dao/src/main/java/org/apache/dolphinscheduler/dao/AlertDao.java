/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.dao;

import org.apache.dolphinscheduler.common.enums.AlertEvent;
import org.apache.dolphinscheduler.common.enums.AlertStatus;
import org.apache.dolphinscheduler.common.enums.AlertWarnLevel;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.apache.dolphinscheduler.dao.datasource.ConnectionFactory;
import org.apache.dolphinscheduler.dao.entity.Alert;
import org.apache.dolphinscheduler.dao.entity.AlertPluginInstance;
import org.apache.dolphinscheduler.dao.entity.ProcessAlertContent;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
import org.apache.dolphinscheduler.dao.entity.ProcessInstance;
import org.apache.dolphinscheduler.dao.entity.ServerAlertContent;
import org.apache.dolphinscheduler.dao.mapper.AlertGroupMapper;
import org.apache.dolphinscheduler.dao.mapper.AlertMapper;
import org.apache.dolphinscheduler.dao.mapper.AlertPluginInstanceMapper;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
=======
package org.apache.dolphinscheduler.dao;


import org.apache.dolphinscheduler.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.dolphinscheduler.common.enums.AlertStatus;
import org.apache.dolphinscheduler.common.enums.AlertType;
import org.apache.dolphinscheduler.common.enums.ShowType;
import org.apache.dolphinscheduler.dao.datasource.ConnectionFactory;
import org.apache.dolphinscheduler.dao.entity.Alert;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
import org.apache.dolphinscheduler.dao.entity.ProcessInstance;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.AlertMapper;
import org.apache.dolphinscheduler.dao.mapper.UserAlertGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Date;
import java.util.List;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

@Component
public class AlertDao extends AbstractBaseDao {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AlertMapper alertMapper;

    @Autowired
<<<<<<< HEAD
    private AlertPluginInstanceMapper alertPluginInstanceMapper;

    @Autowired
    private AlertGroupMapper alertGroupMapper;
=======
    private UserAlertGroupMapper userAlertGroupMapper;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    @Override
    protected void init() {
        alertMapper = ConnectionFactory.getInstance().getMapper(AlertMapper.class);
<<<<<<< HEAD
        alertPluginInstanceMapper = ConnectionFactory.getInstance().getMapper(AlertPluginInstanceMapper.class);
        alertGroupMapper = ConnectionFactory.getInstance().getMapper(AlertGroupMapper.class);
=======
        userAlertGroupMapper = ConnectionFactory.getInstance().getMapper(UserAlertGroupMapper.class);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * insert alert
<<<<<<< HEAD
     *
     * @param alert alert
     * @return add alert result
     */
    public int addAlert(Alert alert) {
=======
     * @param alert alert
     * @return add alert result
     */
    public int addAlert(Alert alert){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return alertMapper.insert(alert);
    }

    /**
     * update alert
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param alertStatus alertStatus
     * @param log log
     * @param id id
     * @return update alert result
     */
<<<<<<< HEAD
    public int updateAlert(AlertStatus alertStatus, String log, int id) {
=======
    public int updateAlert(AlertStatus alertStatus,String log,int id){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Alert alert = alertMapper.selectById(id);
        alert.setAlertStatus(alertStatus);
        alert.setUpdateTime(new Date());
        alert.setLog(log);
        return alertMapper.updateById(alert);
    }

    /**
<<<<<<< HEAD
     * MasterServer or WorkerServer stoped
     *
     * @param alertGroupId alertGroupId
     * @param host host
     * @param serverType serverType
     */
    public void sendServerStopedAlert(int alertGroupId, String host, String serverType) {
        ServerAlertContent serverStopAlertContent = ServerAlertContent.newBuilder().
                type(serverType)
                .host(host)
                .event(AlertEvent.SERVER_DOWN)
                .warningLevel(AlertWarnLevel.SERIOUS).
                build();
        String content = JSONUtils.toJsonString(Lists.newArrayList(serverStopAlertContent));

        Alert alert = new Alert();
        alert.setTitle("Fault tolerance warning");
        alert.setAlertStatus(AlertStatus.WAIT_EXECUTION);
        alert.setContent(content);
        alert.setAlertGroupId(alertGroupId);
        alert.setCreateTime(new Date());
        alert.setUpdateTime(new Date());
        // we use this method to avoid insert duplicate alert(issue #5525)
        alertMapper.insertAlertWhenServerCrash(alert);
=======
     * query user list by alert group id
     * @param alerGroupId alerGroupId
     * @return user list
     */
    public List<User> queryUserByAlertGroupId(int alerGroupId){

        return userAlertGroupMapper.listUserByAlertgroupId(alerGroupId);
    }

    /**
     * MasterServer or WorkerServer stoped
     * @param alertgroupId alertgroupId
     * @param host host
     * @param serverType serverType
     */
    public void sendServerStopedAlert(int alertgroupId,String host,String serverType){
        Alert alert = new Alert();
        String content = String.format("[{'type':'%s','host':'%s','event':'server down','warning level':'serious'}]",
                serverType, host);
        alert.setTitle("Fault tolerance warning");
        saveTaskTimeoutAlert(alert, content, alertgroupId, null, null);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * process time out alert
<<<<<<< HEAD
     *
     * @param processInstance processInstance
     * @param processDefinition processDefinition
     */
    public void sendProcessTimeoutAlert(ProcessInstance processInstance, ProcessDefinition processDefinition) {
        int alertGroupId = processInstance.getWarningGroupId();
        Alert alert = new Alert();
        List<ProcessAlertContent> processAlertContentList = new ArrayList<>(1);
        ProcessAlertContent processAlertContent = ProcessAlertContent.newBuilder()
                .processId(processInstance.getId())
                .processName(processInstance.getName())
                .event(AlertEvent.TIME_OUT)
                .warningLevel(AlertWarnLevel.MIDDLE)
                .build();
        processAlertContentList.add(processAlertContent);
        String content = JSONUtils.toJsonString(processAlertContentList);
        alert.setTitle("Process Timeout Warn");
        saveTaskTimeoutAlert(alert, content, alertGroupId);
    }

    private void saveTaskTimeoutAlert(Alert alert, String content, int alertGroupId) {
        alert.setAlertGroupId(alertGroupId);
        alert.setContent(content);
=======
     * @param processInstance processInstance
     * @param processDefinition processDefinition
     */
    public void sendProcessTimeoutAlert(ProcessInstance processInstance, ProcessDefinition processDefinition){
        int alertgroupId = processInstance.getWarningGroupId();
        String receivers = processDefinition.getReceivers();
        String receiversCc = processDefinition.getReceiversCc();
        Alert alert = new Alert();
        String content = String.format("[{'id':'%d','name':'%s','event':'timeout','warnLevel':'middle'}]",
                processInstance.getId(), processInstance.getName());
        alert.setTitle("Process Timeout Warn");
        saveTaskTimeoutAlert(alert, content, alertgroupId, receivers, receiversCc);
    }

    private void  saveTaskTimeoutAlert(Alert alert, String content, int alertgroupId,
                                    String receivers,  String receiversCc){
        alert.setShowType(ShowType.TABLE);
        alert.setContent(content);
        alert.setAlertType(AlertType.EMAIL);
        alert.setAlertGroupId(alertgroupId);
        if (StringUtils.isNotEmpty(receivers)) {
            alert.setReceivers(receivers);
        }
        if (StringUtils.isNotEmpty(receiversCc)) {
            alert.setReceiversCc(receiversCc);
        }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        alert.setCreateTime(new Date());
        alert.setUpdateTime(new Date());
        alertMapper.insert(alert);
    }

<<<<<<< HEAD
    /**
     * task timeout warn
     *
     * @param alertGroupId alertGroupId
=======

    /**
     * task timeout warn
     * @param alertgroupId alertgroupId
     * @param receivers receivers
     * @param receiversCc receiversCc
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param processInstanceId processInstanceId
     * @param processInstanceName processInstanceName
     * @param taskId taskId
     * @param taskName taskName
     */
<<<<<<< HEAD
    public void sendTaskTimeoutAlert(int alertGroupId, int processInstanceId,
                                     String processInstanceName, int taskId, String taskName) {
        Alert alert = new Alert();
        List<ProcessAlertContent> processAlertContentList = new ArrayList<>(1);
        ProcessAlertContent processAlertContent = ProcessAlertContent.newBuilder()
                .processId(processInstanceId)
                .processName(processInstanceName)
                .taskId(taskId)
                .taskName(taskName)
                .event(AlertEvent.TIME_OUT)
                .warningLevel(AlertWarnLevel.MIDDLE)
                .build();
        processAlertContentList.add(processAlertContent);
        String content = JSONUtils.toJsonString(processAlertContentList);
        alert.setTitle("Task Timeout Warn");
        saveTaskTimeoutAlert(alert, content, alertGroupId);
=======
    public void sendTaskTimeoutAlert(int alertgroupId,String receivers,String receiversCc, int processInstanceId,
                                     String processInstanceName, int taskId,String taskName){
        Alert alert = new Alert();
        String content = String.format("[{'process instance id':'%d','task name':'%s','task id':'%d','task name':'%s'," +
                        "'event':'timeout','warnLevel':'middle'}]", processInstanceId, processInstanceName, taskId, taskName);
        alert.setTitle("Task Timeout Warn");
        saveTaskTimeoutAlert(alert, content, alertgroupId, receivers, receiversCc);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * list the alert information of waiting to be executed
<<<<<<< HEAD
     *
     * @return alert list
     */
    public List<Alert> listWaitExecutionAlert() {
=======
     * @return alert list
     */
    public List<Alert> listWaitExecutionAlert(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return alertMapper.listAlertByStatus(AlertStatus.WAIT_EXECUTION);
    }

    /**
<<<<<<< HEAD
     * for test
     *
     * @return AlertMapper
     */
    public AlertMapper getAlertMapper() {
        return alertMapper;
    }

    /**
     * list all alert plugin instance by alert group id
     *
     * @param alertGroupId alert group id
     * @return AlertPluginInstance list
     */
    public List<AlertPluginInstance> listInstanceByAlertGroupId(int alertGroupId) {
        String alertInstanceIdsParam = alertGroupMapper.queryAlertGroupInstanceIdsById(alertGroupId);
        if (StringUtils.isNotBlank(alertInstanceIdsParam)) {
            String[] idsArray = alertInstanceIdsParam.split(",");
            List<Integer> ids = Arrays.stream(idsArray)
                    .map(s -> Integer.parseInt(s.trim()))
                    .collect(Collectors.toList());
            return alertPluginInstanceMapper.queryByIds(ids);
        }
        return null;
    }

    public AlertPluginInstanceMapper getAlertPluginInstanceMapper() {
        return alertPluginInstanceMapper;
    }

    public void setAlertPluginInstanceMapper(AlertPluginInstanceMapper alertPluginInstanceMapper) {
        this.alertPluginInstanceMapper = alertPluginInstanceMapper;
    }

    public AlertGroupMapper getAlertGroupMapper() {
        return alertGroupMapper;
    }

    public void setAlertGroupMapper(AlertGroupMapper alertGroupMapper) {
        this.alertGroupMapper = alertGroupMapper;
=======
     * list user information by alert group id
     * @param alertgroupId alertgroupId
     * @return user list
     */
    public List<User> listUserByAlertgroupId(int alertgroupId){
        return userAlertGroupMapper.listUserByAlertgroupId(alertgroupId);
    }

    /**
     * for test
     * @return AlertMapper
     */
    public AlertMapper getAlertMapper() {
        return alertMapper;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }
}
