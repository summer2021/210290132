/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.dao.entity;

import org.apache.dolphinscheduler.common.enums.Flag;
import org.apache.dolphinscheduler.common.enums.ReleaseState;
import org.apache.dolphinscheduler.common.process.Property;
import org.apache.dolphinscheduler.common.utils.JSONUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
=======
package org.apache.dolphinscheduler.dao.entity;

import com.alibaba.fastjson.JSON;
import org.apache.dolphinscheduler.common.enums.Flag;
import org.apache.dolphinscheduler.common.enums.ReleaseState;
import org.apache.dolphinscheduler.common.process.Property;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

/**
 * process definition
 */
@TableName("t_ds_process_definition")
public class ProcessDefinition {
<<<<<<< HEAD

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    /**
     * code
     */
    private long code;

    /**
=======
    /**
     * id
     */
    @TableId(value="id", type=IdType.AUTO)
    private int id;

    /**
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * name
     */
    private String name;

    /**
     * version
     */
    private int version;

    /**
     * release state : online/offline
     */
    private ReleaseState releaseState;

    /**
<<<<<<< HEAD
     * project code
     */
    private long projectCode;
=======
     * project id
     */
    private int projectId;

    /**
     * definition json string
     */
    private String processDefinitionJson;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * description
     */
    private String description;

    /**
     * user defined parameters
     */
    private String globalParams;

    /**
     * user defined parameter list
     */
<<<<<<< HEAD
    @TableField(exist = false)
=======
    @TableField(exist=false)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private List<Property> globalParamList;

    /**
     * user define parameter map
     */
<<<<<<< HEAD
    @TableField(exist = false)
    private Map<String, String> globalParamMap;
=======
    @TableField(exist=false)
    private Map<String,String> globalParamMap;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * create time
     */
<<<<<<< HEAD
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private Date createTime;

    /**
     * update time
     */
<<<<<<< HEAD
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private Date updateTime;

    /**
     * process is valid: yes/no
     */
    private Flag flag;

    /**
     * process user id
     */
    private int userId;

    /**
     * user name
     */
    @TableField(exist = false)
    private String userName;

    /**
     * project name
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * locations array for web
     */
    private String locations;

    /**
<<<<<<< HEAD
     * schedule release state : online/offline
     */
    @TableField(exist = false)
=======
     * connects array for web
     */
    private String connects;

    /**
     * receivers
     */
    private String receivers;

    /**
     * receivers cc
     */
    private String receiversCc;

    /**
     * schedule release state : online/offline
     */
    @TableField(exist=false)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private ReleaseState scheduleReleaseState;

    /**
     * process warning time out. unit: minute
     */
    private int timeout;

    /**
     * tenant id
     */
    private int tenantId;

    /**
<<<<<<< HEAD
     * tenant code
     */
    @TableField(exist = false)
    private String tenantCode;

    /**
     * modify user name
     */
    @TableField(exist = false)
=======
     * modify user name
     */
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private String modifyBy;

    /**
     * resource ids
     */
<<<<<<< HEAD
    @TableField(exist = false)
    private String resourceIds;

    /**
     * warningGroupId
     */
    @TableField(exist = false)
    private int warningGroupId;

    public ProcessDefinition() {
    }

    public ProcessDefinition(long projectCode,
                             String name,
                             long code,
                             String description,
                             String globalParams,
                             String locations,
                             int timeout,
                             int userId,
                             int tenantId) {
        set(projectCode, name, description, globalParams, locations, timeout, tenantId);
        this.code = code;
        this.userId = userId;
        Date date = new Date();
        this.createTime = date;
        this.updateTime = date;
    }

    public void set(long projectCode,
                    String name,
                    String description,
                    String globalParams,
                    String locations,
                    int timeout,
                    int tenantId) {
        this.projectCode = projectCode;
        this.name = name;
        this.description = description;
        this.globalParams = globalParams;
        this.locations = locations;
        this.timeout = timeout;
        this.tenantId = tenantId;
        this.flag = Flag.YES;
    }
=======
    private String resourceIds;

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ReleaseState getReleaseState() {
        return releaseState;
    }

    public void setReleaseState(ReleaseState releaseState) {
        this.releaseState = releaseState;
    }

<<<<<<< HEAD
=======
    public String getProcessDefinitionJson() {
        return processDefinitionJson;
    }

    public void setProcessDefinitionJson(String processDefinitionJson) {
        this.processDefinitionJson = processDefinitionJson;
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

<<<<<<< HEAD
=======
    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Flag getFlag() {
        return flag;
    }

    public void setFlag(Flag flag) {
        this.flag = flag;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public String getGlobalParams() {
        return globalParams;
    }

    public void setGlobalParams(String globalParams) {
<<<<<<< HEAD
        this.globalParamList = JSONUtils.toList(globalParams, Property.class);
        if (this.globalParamList == null) {
            this.globalParamList = new ArrayList<>();
        }
=======
        this.globalParamList = JSON.parseArray(globalParams, Property.class);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        this.globalParams = globalParams;
    }

    public List<Property> getGlobalParamList() {
        return globalParamList;
    }

    public void setGlobalParamList(List<Property> globalParamList) {
<<<<<<< HEAD
=======
        this.globalParams = JSON.toJSONString(globalParamList);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        this.globalParamList = globalParamList;
    }

    public Map<String, String> getGlobalParamMap() {
<<<<<<< HEAD
        if (globalParamMap == null && StringUtils.isNotEmpty(globalParams)) {
            List<Property> propList = JSONUtils.toList(globalParams, Property.class);
=======
        List<Property> propList;

        if (globalParamMap == null && StringUtils.isNotEmpty(globalParams)) {
            propList = JSON.parseArray(globalParams, Property.class);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            globalParamMap = propList.stream().collect(Collectors.toMap(Property::getProp, Property::getValue));
        }

        return globalParamMap;
    }

    public void setGlobalParamMap(Map<String, String> globalParamMap) {
        this.globalParamMap = globalParamMap;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

<<<<<<< HEAD
=======
    public String getConnects() {
        return connects;
    }

    public void setConnects(String connects) {
        this.connects = connects;
    }

    public String getReceivers() {
        return receivers;
    }

    public void setReceivers(String receivers) {
        this.receivers = receivers;
    }

    public String getReceiversCc() {
        return receiversCc;
    }

    public void setReceiversCc(String receiversCc) {
        this.receiversCc = receiversCc;
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public ReleaseState getScheduleReleaseState() {
        return scheduleReleaseState;
    }

    public void setScheduleReleaseState(ReleaseState scheduleReleaseState) {
        this.scheduleReleaseState = scheduleReleaseState;
    }

    public String getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(String resourceIds) {
        this.resourceIds = resourceIds;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

<<<<<<< HEAD
    public String getTenantCode() {
        return tenantCode;
    }

    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

<<<<<<< HEAD
    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public long getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(long projectCode) {
        this.projectCode = projectCode;
    }

    public int getWarningGroupId() {
        return warningGroupId;
    }

    public void setWarningGroupId(int warningGroupId) {
        this.warningGroupId = warningGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcessDefinition that = (ProcessDefinition) o;
        return projectCode == that.projectCode
            && userId == that.userId
            && timeout == that.timeout
            && tenantId == that.tenantId
            && Objects.equals(name, that.name)
            && releaseState == that.releaseState
            && Objects.equals(description, that.description)
            && Objects.equals(globalParams, that.globalParams)
            && flag == that.flag
            && Objects.equals(locations, that.locations);
    }

    @Override
    public String toString() {
        return "ProcessDefinition{"
            + "id=" + id
            + ", code=" + code
            + ", name='" + name + '\''
            + ", version=" + version
            + ", releaseState=" + releaseState
            + ", projectCode=" + projectCode
            + ", description='" + description + '\''
            + ", globalParams='" + globalParams + '\''
            + ", globalParamList=" + globalParamList
            + ", globalParamMap=" + globalParamMap
            + ", createTime=" + createTime
            + ", updateTime=" + updateTime
            + ", flag=" + flag
            + ", userId=" + userId
            + ", userName='" + userName + '\''
            + ", projectName='" + projectName + '\''
            + ", locations='" + locations + '\''
            + ", scheduleReleaseState=" + scheduleReleaseState
            + ", timeout=" + timeout
            + ", tenantId=" + tenantId
            + ", tenantCode='" + tenantCode + '\''
            + ", modifyBy='" + modifyBy + '\''
            + ", resourceIds='" + resourceIds + '\''
            + ", warningGroupId=" + warningGroupId
            + '}';
    }
=======
    @Override
    public String toString() {
        return "ProcessDefinition{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", version=" + version +
                ", releaseState=" + releaseState +
                ", projectId=" + projectId +
                ", processDefinitionJson='" + processDefinitionJson + '\'' +
                ", description='" + description + '\'' +
                ", globalParams='" + globalParams + '\'' +
                ", globalParamList=" + globalParamList +
                ", globalParamMap=" + globalParamMap +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", flag=" + flag +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", projectName='" + projectName + '\'' +
                ", locations='" + locations + '\'' +
                ", connects='" + connects + '\'' +
                ", receivers='" + receivers + '\'' +
                ", receiversCc='" + receiversCc + '\'' +
                ", scheduleReleaseState=" + scheduleReleaseState +
                ", timeout=" + timeout +
                ", tenantId=" + tenantId +
                ", modifyBy='" + modifyBy + '\'' +
                ", resourceIds='" + resourceIds + '\'' +
                '}';
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
