/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.dao.entity.DefinitionGroupByUser;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
<<<<<<< HEAD
import org.apache.dolphinscheduler.dao.entity.ProcessDefinitionLog;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

=======
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
/**
 * process definition mapper interface
 */
public interface ProcessDefinitionMapper extends BaseMapper<ProcessDefinition> {

    /**
<<<<<<< HEAD
     * query process definition by code
     *
     * @param code code
     * @return process definition
     */
    ProcessDefinition queryByCode(@Param("code") long code);

    /**
     * query process definition by code list
     *
     * @param codes codes
     * @return process definition list
     */
    List<ProcessDefinition> queryByCodes(@Param("codes") Collection<Long> codes);

    /**
     * delete process definition by code
     *
     * @param code code
     * @return delete result
     */
    int deleteByCode(@Param("code") long code);

    /**
     * verify process definition by name
     *
     * @param projectCode projectCode
     * @param name name
     * @return process definition
     */
    ProcessDefinition verifyByDefineName(@Param("projectCode") long projectCode,
=======
     * verify process definition by name
     *
     * @param projectId projectId
     * @param name name
     * @return process definition
     */
    ProcessDefinition verifyByDefineName(@Param("projectId") int projectId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                                         @Param("processDefinitionName") String name);

    /**
     * query process definition by name
<<<<<<< HEAD
     *
     * @param projectCode projectCode
     * @param name name
     * @return process definition
     */
    ProcessDefinition queryByDefineName(@Param("projectCode") long projectCode,
=======
     * @param projectId projectId
     * @param name name
     * @return process definition
     */
    ProcessDefinition queryByDefineName(@Param("projectId") int projectId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                                        @Param("processDefinitionName") String name);

    /**
     * query process definition by id
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param processDefineId processDefineId
     * @return process definition
     */
    ProcessDefinition queryByDefineId(@Param("processDefineId") int processDefineId);

    /**
     * process definition page
<<<<<<< HEAD
     *
     * @param page page
     * @param searchVal searchVal
     * @param userId userId
     * @param projectCode projectCode
=======
     * @param page page
     * @param searchVal searchVal
     * @param userId userId
     * @param projectId projectId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param isAdmin isAdmin
     * @return process definition IPage
     */
    IPage<ProcessDefinition> queryDefineListPaging(IPage<ProcessDefinition> page,
                                                   @Param("searchVal") String searchVal,
                                                   @Param("userId") int userId,
<<<<<<< HEAD
                                                   @Param("projectCode") long projectCode,
=======
                                                   @Param("projectId") int projectId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                                                   @Param("isAdmin") boolean isAdmin);

    /**
     * query all process definition list
<<<<<<< HEAD
     *
     * @param projectCode projectCode
     * @return process definition list
     */
    List<ProcessDefinition> queryAllDefinitionList(@Param("projectCode") long projectCode);

    /**
     * query process definition by ids
     *
=======
     * @param projectId projectId
     * @return process definition list
     */
    List<ProcessDefinition> queryAllDefinitionList(@Param("projectId") int projectId);

    /**
     * query process definition by ids
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param ids ids
     * @return process definition list
     */
    List<ProcessDefinition> queryDefinitionListByIdList(@Param("ids") Integer[] ids);

    /**
     * query process definition by tenant
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param tenantId tenantId
     * @return process definition list
     */
    List<ProcessDefinition> queryDefinitionListByTenant(@Param("tenantId") int tenantId);

    /**
     * count process definition group by user
<<<<<<< HEAD
     *
     * @param userId userId
     * @param projectCodes projectCodes
=======
     * @param userId userId
     * @param projectIds projectIds
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param isAdmin isAdmin
     * @return process definition list
     */
    List<DefinitionGroupByUser> countDefinitionGroupByUser(
            @Param("userId") Integer userId,
<<<<<<< HEAD
            @Param("projectCodes") Long[] projectCodes,
=======
            @Param("projectIds") Integer[] projectIds,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            @Param("isAdmin") boolean isAdmin);

    /**
     * list all resource ids
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return resource ids list
     */
    @MapKey("id")
    List<Map<String, Object>> listResources();

    /**
     * list all resource ids by user id
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return resource ids list
     */
    @MapKey("id")
    List<Map<String, Object>> listResourcesByUser(@Param("userId") Integer userId);
<<<<<<< HEAD

    /**
     * list all project ids
     *
     * @return project ids list
     */
    List<Integer> listProjectIds();
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
