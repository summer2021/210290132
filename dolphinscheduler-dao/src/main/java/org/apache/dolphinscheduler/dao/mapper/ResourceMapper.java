/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.dao.entity.Resource;

=======
package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.dao.entity.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.ibatis.annotations.Param;

import java.util.List;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
/**
 * resource mapper interface
 */
public interface ResourceMapper extends BaseMapper<Resource> {

    /**
     * query resource list
     * @param fullName full name
     * @param userId userId
     * @param type type
     * @return resource list
     */
    List<Resource> queryResourceList(@Param("fullName") String fullName,
                                     @Param("userId") int userId,
                                     @Param("type") int type);

    /**
     * query resource list
     * @param userId userId
     * @param type type
<<<<<<< HEAD
     * @return resource list
     */
    List<Resource> queryResourceListAuthored(@Param("userId") int userId,
                                             @Param("type") int type);
=======
     * @param perm perm
     * @return resource list
     */
    List<Resource> queryResourceListAuthored(
                                     @Param("userId") int userId,
                                     @Param("type") int type,
                                     @Param("perm") int perm);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * resource page
     * @param page page
     * @param userId userId
     * @param id id
     * @param type type
     * @param searchVal searchVal
<<<<<<< HEAD
     * @param resIds resIds
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return resource page
     */
    IPage<Resource> queryResourcePaging(IPage<Resource> page,
                                        @Param("userId") int userId,
                                        @Param("id") int id,
                                        @Param("type") int type,
<<<<<<< HEAD
                                        @Param("searchVal") String searchVal,
                                        @Param("resIds") List<Integer> resIds);
=======
                                        @Param("searchVal") String searchVal);

    /**
     * query Authed resource list
     * @param userId userId
     * @return resource list
     */
    List<Resource> queryAuthorizedResourceList(@Param("userId") int userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     *  query resource except userId
     * @param userId userId
     * @return resource list
     */
    List<Resource> queryResourceExceptUserId(@Param("userId") int userId);

    /**
<<<<<<< HEAD
=======
     * query tenant code by name
     * @param resName resource name
     * @param resType resource type
     * @return tenant code
     */
    String queryTenantCodeByResourceName(@Param("resName") String resName,@Param("resType") int resType);

    /**
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * list authorized resource
     * @param userId userId
     * @param resNames resNames
     * @param <T> T
     * @return resource list
     */
<<<<<<< HEAD
    <T> List<Resource> listAuthorizedResource(@Param("userId") int userId, @Param("resNames") T[] resNames);

    /**
     * list resources by id
     * @param resIds resIds
     * @return resource list
     */
    List<Resource> queryResourceListById(@Param("resIds") List<Integer> resIds);
=======
    <T> List<Resource> listAuthorizedResource(@Param("userId") int userId,@Param("resNames")T[] resNames);


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * list authorized resource
     * @param userId userId
     * @param resIds resIds
     * @param <T> T
     * @return resource list
     */
    <T> List<Resource> listAuthorizedResourceById(@Param("userId") int userId,@Param("resIds")T[] resIds);

    /**
     * delete resource by id array
     * @param resIds resource id array
     * @return delete num
     */
    int deleteIds(@Param("resIds")Integer[] resIds);

    /**
     * list children
     * @param direcotyId directory id
     * @return resource id array
     */
    List<Integer> listChildren(@Param("direcotyId") int direcotyId);

    /**
     * query resource by full name or pid
     * @param fullName  full name
     * @param type      resource type
     * @return resource
     */
    List<Resource> queryResource(@Param("fullName") String fullName,@Param("type") int type);

    /**
     * list resource by id array
     * @param resIds resource id array
     * @return resource list
     */
    List<Resource> listResourceByIds(@Param("resIds")Integer[] resIds);

    /**
     * update resource
     * @param resourceList  resource list
     * @return update num
     */
    int batchUpdateResource(@Param("resourceList") List<Resource> resourceList);
<<<<<<< HEAD

    /**
     * check resource exist
     * @param fullName full name
     * @param userId userId
     * @param type type
     * @return true if exist else return null
     */
    Boolean existResource(@Param("fullName") String fullName,
                              @Param("userId") int userId,
                              @Param("type") int type);
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
