/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.common.enums.ReleaseState;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.dao.entity.DefinitionGroupByUser;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
import org.apache.dolphinscheduler.dao.entity.Project;
import org.apache.dolphinscheduler.dao.entity.Queue;
import org.apache.dolphinscheduler.dao.entity.Tenant;
import org.apache.dolphinscheduler.dao.entity.User;

import java.util.Date;
import java.util.List;
import java.util.Map;

=======
package org.apache.dolphinscheduler.dao.mapper;


import org.apache.dolphinscheduler.common.enums.ReleaseState;
import org.apache.dolphinscheduler.common.enums.UserType;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dolphinscheduler.dao.entity.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
=======
import java.util.Date;
import java.util.List;
import java.util.Map;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback(true)
public class ProcessDefinitionMapperTest {


    @Autowired
    ProcessDefinitionMapper processDefinitionMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    QueueMapper queueMapper;

    @Autowired
    TenantMapper tenantMapper;

    @Autowired
    ProjectMapper projectMapper;

    /**
     * insert
<<<<<<< HEAD
     *
     * @return ProcessDefinition
     */
    private ProcessDefinition insertOne() {
        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setCode(1L);
        processDefinition.setName("def 1");
        processDefinition.setProjectCode(1010L);
=======
     * @return ProcessDefinition
     */
    private ProcessDefinition insertOne(){
        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setName("def 1");
        processDefinition.setProjectId(1010);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        processDefinition.setUserId(101);
        processDefinition.setUpdateTime(new Date());
        processDefinition.setCreateTime(new Date());
        processDefinitionMapper.insert(processDefinition);
        return processDefinition;
    }

    /**
     * insert
<<<<<<< HEAD
     *
     * @return ProcessDefinition
     */
    private ProcessDefinition insertTwo() {
        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setCode(1L);
        processDefinition.setName("def 2");
        processDefinition.setProjectCode(1010L);
=======
     * @return ProcessDefinition
     */
    private ProcessDefinition insertTwo(){
        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setName("def 2");
        processDefinition.setProjectId(1010);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        processDefinition.setUserId(101);
        processDefinition.setUpdateTime(new Date());
        processDefinition.setCreateTime(new Date());
        processDefinitionMapper.insert(processDefinition);
        return processDefinition;
    }

    /**
     * test update
     */
    @Test
<<<<<<< HEAD
    public void testUpdate() {
=======
    public void testUpdate(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        ProcessDefinition processDefinition = insertOne();
        //update
        processDefinition.setUpdateTime(new Date());
        int update = processDefinitionMapper.updateById(processDefinition);
        Assert.assertEquals(1, update);
    }

    /**
     * test delete
     */
    @Test
<<<<<<< HEAD
    public void testDelete() {
=======
    public void testDelete(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        ProcessDefinition processDefinition = insertOne();
        int delete = processDefinitionMapper.deleteById(processDefinition.getId());
        Assert.assertEquals(1, delete);
    }

    /**
     * test query
     */
    @Test
    public void testQuery() {
        ProcessDefinition processDefinition = insertOne();
        //query
        List<ProcessDefinition> dataSources = processDefinitionMapper.selectList(null);
        Assert.assertNotEquals(dataSources.size(), 0);
    }

    /**
<<<<<<< HEAD
     * test verifyByDefineName
     */
    @Test
    public void testVerifyByDefineName() {
        Project project = new Project();
        project.setCode(1L);
        project.setName("ut project");
        project.setUserId(4);
        project.setCreateTime(new Date());
        projectMapper.insert(project);
        Queue queue = new Queue();
        queue.setQueue("queue");
        queue.setQueueName("queue name");
        queueMapper.insert(queue);
        Tenant tenant = new Tenant();
        tenant.setTenantCode("tenant");
        tenant.setQueueId(queue.getId());
        tenant.setDescription("t");
        tenantMapper.insert(tenant);
        User user = new User();
        user.setUserName("hello");
        user.setUserPassword("pwd");
        user.setUserType(UserType.GENERAL_USER);
        user.setTenantId(tenant.getId());
        userMapper.insert(user);
        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setCode(1L);
        processDefinition.setName("def 1");
        processDefinition.setProjectCode(project.getCode());
        processDefinition.setUpdateTime(new Date());
        processDefinition.setCreateTime(new Date());
        processDefinition.setTenantId(tenant.getId());
        processDefinition.setUserId(user.getId());
        processDefinitionMapper.insert(processDefinition);
        ProcessDefinition definition = processDefinitionMapper.verifyByDefineName(10L, "xxx");
        Assert.assertEquals(definition, null);
    }

    /**
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * test query by definition name
     */
    @Test
    public void testQueryByDefineName() {
        Project project = new Project();
        project.setName("ut project");
<<<<<<< HEAD
        project.setCode(1L);
        project.setUserId(4);
        project.setCreateTime(new Date());
=======
        project.setUserId(4);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        projectMapper.insert(project);

        Queue queue = new Queue();
        queue.setQueue("queue");
        queue.setQueueName("queue name");
        queueMapper.insert(queue);

        Tenant tenant = new Tenant();
        tenant.setTenantCode("tenant");
        tenant.setQueueId(queue.getId());
        tenant.setDescription("t");
        tenantMapper.insert(tenant);

        User user = new User();
        user.setUserName("hello");
        user.setUserPassword("pwd");
        user.setUserType(UserType.GENERAL_USER);
        user.setTenantId(tenant.getId());
        userMapper.insert(user);

        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
<<<<<<< HEAD
        processDefinition.setCode(1L);
        processDefinition.setName("def 1");
        processDefinition.setProjectCode(project.getCode());
=======
        processDefinition.setName("def 1");
        processDefinition.setProjectId(project.getId());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        processDefinition.setUpdateTime(new Date());
        processDefinition.setCreateTime(new Date());
        processDefinition.setTenantId(tenant.getId());
        processDefinition.setUserId(user.getId());
        processDefinitionMapper.insert(processDefinition);

<<<<<<< HEAD
        ProcessDefinition processDefinition1 = processDefinitionMapper.queryByDefineName(project.getCode(), "def 1");
=======
        ProcessDefinition processDefinition1 = processDefinitionMapper.queryByDefineName(project.getId(), "def 1");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Assert.assertNotEquals(processDefinition1, null);
    }

    /**
<<<<<<< HEAD
     * test queryDefinitionListByTenant
     */
    @Test
    public void testQueryDefinitionListByTenant() {
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setCode(1L);
        processDefinition.setName("def 1");
        processDefinition.setProjectCode(888L);
        processDefinition.setUpdateTime(new Date());
        processDefinition.setCreateTime(new Date());
        processDefinition.setTenantId(999);
        processDefinition.setUserId(1234);
        processDefinitionMapper.insert(processDefinition);
        List<ProcessDefinition> definitions = processDefinitionMapper.queryDefinitionListByTenant(999);
        Assert.assertNotEquals(definitions.size(), 0);
    }

    /**
     * test queryByDefineId
     */
    @Test
    public void testQueryByDefineId() {
        Project project = new Project();
        project.setCode(1L);
        project.setName("ut project");
        project.setUserId(4);
        project.setCreateTime(new Date());
        projectMapper.insert(project);

        Queue queue = new Queue();
        queue.setQueue("queue");
        queue.setQueueName("queue name");
        queueMapper.insert(queue);

        Tenant tenant = new Tenant();
        tenant.setTenantCode("tenant");
        tenant.setQueueId(queue.getId());
        tenant.setDescription("t");
        tenantMapper.insert(tenant);

        User user = new User();
        user.setUserName("hello");
        user.setUserPassword("pwd");
        user.setUserType(UserType.GENERAL_USER);
        user.setTenantId(tenant.getId());
        userMapper.insert(user);

        //insertOne
        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setCode(1L);
        processDefinition.setName("def 1");
        processDefinition.setProjectCode(project.getCode());
        processDefinition.setUpdateTime(new Date());
        processDefinition.setCreateTime(new Date());
        processDefinition.setTenantId(tenant.getId());
        processDefinition.setUserId(user.getId());
        processDefinitionMapper.insert(processDefinition);
        ProcessDefinition definition = processDefinitionMapper.queryByDefineId(333);
        Assert.assertEquals(definition, null);
    }

    /**
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * test page
     */
    @Test
    public void testQueryDefineListPaging() {
        ProcessDefinition processDefinition = insertOne();
<<<<<<< HEAD
        Page<ProcessDefinition> page = new Page(1, 3);
        IPage<ProcessDefinition> processDefinitionIPage = processDefinitionMapper.queryDefineListPaging(page, "def", 101, 1010L, true);
=======
        Page<ProcessDefinition> page = new Page(1,3);
        IPage<ProcessDefinition> processDefinitionIPage =  processDefinitionMapper.queryDefineListPaging(page, "def", 101, 1010,true);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Assert.assertNotEquals(processDefinitionIPage.getTotal(), 0);
    }

    /**
     * test query all process definition
     */
    @Test
    public void testQueryAllDefinitionList() {
        ProcessDefinition processDefinition = insertOne();
<<<<<<< HEAD
        List<ProcessDefinition> processDefinitionIPage = processDefinitionMapper.queryAllDefinitionList(1010L);
=======
        List<ProcessDefinition> processDefinitionIPage =  processDefinitionMapper.queryAllDefinitionList(1010);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Assert.assertNotEquals(processDefinitionIPage.size(), 0);
    }

    /**
     * test query process definition by ids
     */
    @Test
    public void testQueryDefinitionListByIdList() {

        ProcessDefinition processDefinition = insertOne();
        ProcessDefinition processDefinition1 = insertTwo();

        Integer[] array = new Integer[2];
        array[0] = processDefinition.getId();
        array[1] = processDefinition1.getId();

        List<ProcessDefinition> processDefinitions = processDefinitionMapper.queryDefinitionListByIdList(array);
        Assert.assertEquals(2, processDefinitions.size());

    }

    /**
     * test count process definition group by user
     */
    @Test
    public void testCountDefinitionGroupByUser() {

<<<<<<< HEAD
        User user = new User();
=======
        User user= new User();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        user.setUserName("user1");
        user.setUserPassword("1");
        user.setEmail("xx@123.com");
        user.setUserType(UserType.GENERAL_USER);
        user.setCreateTime(new Date());
        user.setTenantId(1);
        user.setUpdateTime(new Date());
        userMapper.insert(user);

        ProcessDefinition processDefinition = insertOne();
        processDefinition.setUserId(user.getId());
        processDefinitionMapper.updateById(processDefinition);

<<<<<<< HEAD
        Long[] projectCodes = new Long[1];
        projectCodes[0] = processDefinition.getProjectCode();
        List<DefinitionGroupByUser> processDefinitions = processDefinitionMapper.countDefinitionGroupByUser(
                processDefinition.getUserId(),
                projectCodes,
=======
        Integer[] projectIds = new Integer[1];
        projectIds[0] = processDefinition.getProjectId();
        List<DefinitionGroupByUser> processDefinitions = processDefinitionMapper.countDefinitionGroupByUser(
                processDefinition.getUserId(),
                projectIds,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                user.getUserType() == UserType.ADMIN_USER
        );
        Assert.assertNotEquals(processDefinitions.size(), 0);
    }

    @Test
<<<<<<< HEAD
    public void listResourcesTest() {
=======
    public void listResourcesTest(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        ProcessDefinition processDefinition = insertOne();
        processDefinition.setResourceIds("3,5");
        processDefinition.setReleaseState(ReleaseState.ONLINE);
        List<Map<String, Object>> maps = processDefinitionMapper.listResources();
        Assert.assertNotNull(maps);
    }

    @Test
<<<<<<< HEAD
    public void listResourcesByUserTest() {
=======
    public void listResourcesByUserTest(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        ProcessDefinition processDefinition = insertOne();
        processDefinition.setResourceIds("3,5");
        processDefinition.setReleaseState(ReleaseState.ONLINE);
        List<Map<String, Object>> maps = processDefinitionMapper.listResourcesByUser(processDefinition.getUserId());
        Assert.assertNotNull(maps);
    }
<<<<<<< HEAD

    @Test
    public void listProjectIds() {
        ProcessDefinition processDefinition = insertOne();
        List<Integer> projectIds = processDefinitionMapper.listProjectIds();
        Assert.assertNotNull(projectIds);
    }

}
=======
}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
