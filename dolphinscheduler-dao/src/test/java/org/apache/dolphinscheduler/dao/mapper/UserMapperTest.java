/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.AccessToken;
import org.apache.dolphinscheduler.dao.entity.AlertGroup;
import org.apache.dolphinscheduler.dao.entity.Queue;
import org.apache.dolphinscheduler.dao.entity.Tenant;
import org.apache.dolphinscheduler.dao.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

=======
package org.apache.dolphinscheduler.dao.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dolphinscheduler.common.enums.AlertType;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
=======
import java.util.Date;
import java.util.List;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback(true)
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    AlertGroupMapper alertGroupMapper;

    @Autowired
<<<<<<< HEAD
=======
    private UserAlertGroupMapper userAlertGroupMapper;

    @Autowired
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    AccessTokenMapper accessTokenMapper;

    @Autowired
    TenantMapper tenantMapper;

    @Autowired
    QueueMapper queueMapper;

    /**
     * insert one user
<<<<<<< HEAD
     *
     * @return User
     */
    private User insertOne() {
=======
     * @return User
     */
    private User insertOne(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserName("user1");
        user.setUserPassword("1");
        user.setEmail("xx@123.com");
        user.setUserType(UserType.GENERAL_USER);
        user.setCreateTime(new Date());
        user.setTenantId(1);
        user.setUpdateTime(new Date());
<<<<<<< HEAD
        user.setQueueName("test_queue");
        user.setQueue("queue");
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        userMapper.insert(user);
        return user;
    }

    /**
     * insert one user
<<<<<<< HEAD
     *
     * @param tenant tenant
     * @return User
     */
    private User insertOne(Tenant tenant) {
=======
     * @param tenant tenant
     * @return User
     */
    private User insertOne(Tenant tenant){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserName("user1");
        user.setUserPassword("1");
        user.setEmail("xx@123.com");
        user.setUserType(UserType.GENERAL_USER);
        user.setCreateTime(new Date());
        user.setTenantId(tenant.getId());
        user.setUpdateTime(new Date());
        userMapper.insert(user);
        return user;
    }

    /**
     * insert one user
<<<<<<< HEAD
     *
     * @param queue  queue
     * @param tenant tenant
     * @return User
     */
    private User insertOne(Queue queue, Tenant tenant) {
=======
     * @param queue queue
     * @param tenant tenant
     * @return User
     */
    private User insertOne(Queue queue,Tenant tenant){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserName("user1");
        user.setUserPassword("1");
        user.setEmail("xx@123.com");
        user.setUserType(UserType.GENERAL_USER);
        user.setCreateTime(new Date());
        user.setTenantId(tenant.getId());
        user.setQueue(queue.getQueueName());
        user.setUpdateTime(new Date());
        userMapper.insert(user);
        return user;
    }

    /**
     * insert one AlertGroup
<<<<<<< HEAD
     *
     * @return AlertGroup
     */
    private AlertGroup insertOneAlertGroup() {
=======
     * @return AlertGroup
     */
    private AlertGroup insertOneAlertGroup(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setGroupName("alert group 1");
        alertGroup.setDescription("alert test1");
<<<<<<< HEAD
=======
        alertGroup.setGroupType(AlertType.EMAIL);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        alertGroup.setCreateTime(new Date());
        alertGroup.setUpdateTime(new Date());
        alertGroupMapper.insert(alertGroup);
        return alertGroup;
    }

    /**
<<<<<<< HEAD
     * insert one AccessToken
     *
     * @param user user
     * @return AccessToken
     */
    private AccessToken insertOneAccessToken(User user) {
=======
     * insert one UserAlertGroup
     * @param user user
     * @param alertGroup alertGroup
     * @return UserAlertGroup
     */
    private UserAlertGroup insertOneUserAlertGroup(User user,AlertGroup alertGroup){
        UserAlertGroup userAlertGroup = new UserAlertGroup();
        userAlertGroup.setAlertgroupName(alertGroup.getGroupName());
        userAlertGroup.setAlertgroupId(alertGroup.getId());
        userAlertGroup.setUserId(user.getId());
        userAlertGroup.setCreateTime(new Date());
        userAlertGroup.setUpdateTime(new Date());
        userAlertGroupMapper.insert(userAlertGroup);
        return userAlertGroup;
    }

    /**
     * insert one AccessToken
     * @param user user
     * @return AccessToken
     */
    private AccessToken insertOneAccessToken(User user){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        AccessToken accessToken = new AccessToken();
        accessToken.setUserId(user.getId());
        accessToken.setToken("secrettoken");
        accessToken.setCreateTime(new Date());
        accessToken.setUpdateTime(new Date());
<<<<<<< HEAD
        accessToken.setExpireTime(DateUtils.getSomeHourOfDay(new Date(), 1));
=======
        accessToken.setExpireTime(DateUtils.getSomeHourOfDay(new Date(),1));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        accessTokenMapper.insert(accessToken);
        return accessToken;
    }

    /**
     * insert one Tenant
<<<<<<< HEAD
     *
     * @return Tenant
     */
    private Tenant insertOneTenant() {
        Tenant tenant = new Tenant();
        tenant.setTenantCode("dolphin");
=======
     * @return Tenant
     */
    private Tenant insertOneTenant(){
        Tenant tenant = new Tenant();
        tenant.setTenantCode("dolphin");
        tenant.setTenantName("dolphin test");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        tenant.setDescription("dolphin user use");
        tenant.setQueue("1");
        tenant.setCreateTime(new Date());
        tenant.setUpdateTime(new Date());
        tenantMapper.insert(tenant);
        return tenant;
    }

    /**
     * insert one Tenant
<<<<<<< HEAD
     *
     * @return Tenant
     */
    private Tenant insertOneTenant(Queue queue) {
        Tenant tenant = new Tenant();
        tenant.setTenantCode("dolphin");
=======
     * @return Tenant
     */
    private Tenant insertOneTenant(Queue queue){
        Tenant tenant = new Tenant();
        tenant.setTenantCode("dolphin");
        tenant.setTenantName("dolphin test");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        tenant.setDescription("dolphin user use");
        tenant.setQueueId(queue.getId());
        tenant.setQueue(queue.getQueue());
        tenant.setCreateTime(new Date());
        tenant.setUpdateTime(new Date());
        tenantMapper.insert(tenant);
        return tenant;
    }

    /**
     * insert one Queue
<<<<<<< HEAD
     *
     * @return Queue
     */
    private Queue insertOneQueue() {
=======
     * @return Queue
     */
    private Queue insertOneQueue(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Queue queue = new Queue();
        queue.setQueue("dolphin");
        queue.setQueueName("dolphin queue");
        queue.setCreateTime(new Date());
        queue.setUpdateTime(new Date());
        queueMapper.insert(queue);
        return queue;
    }

    /**
     * test update
     */
    @Test
<<<<<<< HEAD
    public void testUpdate() {
=======
    public void testUpdate(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        User user = insertOne();
        //update
        user.setEmail("xx-update@126.com");
        user.setUserName("user1_update");
        user.setUserType(UserType.ADMIN_USER);
        int update = userMapper.updateById(user);
        Assert.assertEquals(update, 1);
    }

    /**
     * test delete
     */
    @Test
<<<<<<< HEAD
    public void testDelete() {
=======
    public void testDelete(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        User user = insertOne();
        //delete
        int delete = userMapper.deleteById(user.getId());
        Assert.assertEquals(delete, 1);
    }

    /**
     * test query
     */
    @Test
    public void testQuery() {
        //insertOne
        User user = insertOne();
        //query
        List<User> userList = userMapper.selectList(null);
        Assert.assertNotEquals(userList.size(), 0);
    }

    /**
     * test query all general user
     */
    @Test
    public void testQueryAllGeneralUser() {
        //insertOne
        User user = insertOne();
        //queryAllGeneralUser
        List<User> userList = userMapper.queryAllGeneralUser();
        Assert.assertNotEquals(userList.size(), 0);
    }

<<<<<<< HEAD
=======
//    /**
//     * test query by username
//     */
//    @Test
//    public void testQueryByUserNameAccurately() {
//        //insertOne
//        User user = insertOne();
//        //queryByUserNameAccurately
//        User queryUser = userMapper.queryByUserNameAccurately(user.getUserName());
//        Assert.assertEquals(queryUser.getUserName(), user.getUserName());
//    }

//    /**
//     * test query by username and password
//     */
//    @Test
//    public void testQueryUserByNamePassword() {
//        //insertOne
//        User user = insertOne();
//        //queryUserByNamePassword
//        User queryUser = userMapper.queryUserByNamePassword(user.getUserName(),user.getUserPassword());
//        Assert.assertEquals(queryUser.getUserName(),user.getUserName());
//        Assert.assertEquals(queryUser.getUserPassword(), user.getUserPassword());
//    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * test page
     */
    @Test
    public void testQueryUserPaging() {
        //insertOneQueue
        Queue queue = insertOneQueue();
        //insertOneTenant
        Tenant tenant = insertOneTenant();
        //insertOne
<<<<<<< HEAD
        User user = insertOne(queue, tenant);
        //queryUserPaging
        Page<User> page = new Page(1, 3);
=======
        User user = insertOne(queue,tenant);
        //queryUserPaging
        Page<User> page = new Page(1,3);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        IPage<User> userIPage = userMapper.queryUserPaging(page, user.getUserName());
        Assert.assertNotEquals(userIPage.getTotal(), 0);
    }

    /**
     * test query detail by id
     */
    @Test
    public void testQueryDetailsById() {
        //insertOneQueue and insertOneTenant
        Queue queue = insertOneQueue();
        Tenant tenant = insertOneTenant(queue);
<<<<<<< HEAD
        User user = insertOne(queue, tenant);
=======
        User user = insertOne(queue,tenant);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //queryDetailsById
        User queryUser = userMapper.queryDetailsById(user.getId());
        Assert.assertEquals(user.getUserName(), queryUser.getUserName());
    }

    /**
<<<<<<< HEAD
=======
     * test query user list by alertgroupId
     */
    @Test
    public void testQueryUserListByAlertGroupId() {
        //insertOne
        User user = insertOne();
        //insertOneAlertGroup
        AlertGroup alertGroup = insertOneAlertGroup();
        //insertOneUserAlertGroup
        UserAlertGroup userAlertGroup = insertOneUserAlertGroup(user, alertGroup);
        //queryUserListByAlertGroupId
        List<User> userList = userMapper.queryUserListByAlertGroupId(userAlertGroup.getAlertgroupId());
        Assert.assertNotEquals(userList.size(), 0);

    }

    /**
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * test query tenant code by userId
     */
    @Test
    public void testQueryTenantCodeByUserId() {
        //insertOneTenant
        Tenant tenant = insertOneTenant();
        //insertOne
        User user = insertOne(tenant);
        //queryTenantCodeByUserId
        User queryUser = userMapper.queryTenantCodeByUserId(user.getId());
<<<<<<< HEAD
        Assert.assertEquals(queryUser, user);
=======
        Assert.assertEquals(queryUser,user);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * test query user by token
     */
    @Test
    public void testQueryUserByToken() {
        //insertOne
        User user = insertOne();
        //insertOneAccessToken
        AccessToken accessToken = insertOneAccessToken(user);
        //queryUserByToken
        User userToken = userMapper.queryUserByToken(accessToken.getToken());
<<<<<<< HEAD
        Assert.assertEquals(userToken, user);

    }

    @Test
    public void selectByIds() {
        //insertOne
        User user = insertOne();
        List<Integer> userIds = new ArrayList<>();
        userIds.add(user.getId());
        List<User> users = userMapper.selectByIds(userIds);
        Assert.assertFalse(users.isEmpty());
    }

    @Test
    public void testExistUser() {
        String queueName = "queue";
        Assert.assertNull(userMapper.existUser(queueName));
        insertOne();
        Assert.assertTrue(userMapper.existUser(queueName));
=======
        Assert.assertEquals(userToken,user);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }
}
