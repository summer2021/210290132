/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.dolphinscheduler.dao.mapper;


import org.apache.dolphinscheduler.dao.entity.Project;
import org.apache.dolphinscheduler.dao.entity.User;
<<<<<<< HEAD

import java.util.Date;
import java.util.List;

=======
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
=======
import java.util.Date;
import java.util.List;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback(true)
public class ProjectMapperTest {

    @Autowired
    ProjectMapper projectMapper;

    @Autowired
    UserMapper userMapper;

<<<<<<< HEAD
    @Autowired
    ProcessDefinitionLogMapper processDefinitionLogMapper;

    @Autowired
    ProcessDefinitionMapper processDefinitionMapper;

    /**
     * insert
     *
     * @return Project
     */
    private Project insertOne() {
=======

    /**
     * insert
     * @return Project
     */
    private Project insertOne(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        Project project = new Project();
        project.setName("ut project");
        project.setUserId(111);
<<<<<<< HEAD
        project.setCode(1L);
        project.setCreateTime(new Date());
        project.setUpdateTime(new Date());
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        projectMapper.insert(project);
        return project;
    }

    /**
     * test update
     */
    @Test
<<<<<<< HEAD
    public void testUpdate() {
=======
    public void testUpdate(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        Project project = insertOne();
        project.setCreateTime(new Date());
        //update
        int update = projectMapper.updateById(project);
        Assert.assertEquals(update, 1);
    }

    /**
     * test delete
     */
    @Test
<<<<<<< HEAD
    public void testDelete() {
=======
    public void testDelete(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Project projectMap = insertOne();
        int delete = projectMapper.deleteById(projectMap.getId());
        Assert.assertEquals(delete, 1);
    }

    /**
     * test query
     */
    @Test
    public void testQuery() {
        Project project = insertOne();
        //query
        List<Project> projects = projectMapper.selectList(null);
        Assert.assertNotEquals(projects.size(), 0);
    }

    /**
     * test query detail by id
     */
    @Test
    public void testQueryDetailById() {

        User user = new User();
        user.setUserName("ut user");
        userMapper.insert(user);

        Project project = insertOne();
        project.setUserId(user.getId());
        projectMapper.updateById(project);
        Project project1 = projectMapper.queryDetailById(project.getId());

        Assert.assertNotEquals(project1, null);
        Assert.assertEquals(project1.getUserName(), user.getUserName());
    }

    /**
     * test query project by name
     */
    @Test
    public void testQueryProjectByName() {
        User user = new User();
        user.setUserName("ut user");
        userMapper.insert(user);

        Project project = insertOne();
        project.setUserId(user.getId());
        projectMapper.updateById(project);
        Project project1 = projectMapper.queryByName(project.getName());

        Assert.assertNotEquals(project1, null);
    }

    /**
     * test page
     */
    @Test
    public void testQueryProjectListPaging() {
        Project project = insertOne();
<<<<<<< HEAD
=======
        Project project1 = insertOne();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        User user = new User();
        user.setUserName("ut user");
        userMapper.insert(user);
        project.setUserId(user.getId());
        projectMapper.updateById(project);

<<<<<<< HEAD
        Page<Project> page = new Page(1, 3);
=======
        Page<Project> page = new Page(1,3);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        IPage<Project> projectIPage = projectMapper.queryProjectListPaging(
                page,
                project.getUserId(),
                null
        );
        IPage<Project> projectIPage1 = projectMapper.queryProjectListPaging(
                page,
                project.getUserId(),
                project.getName()
        );
<<<<<<< HEAD
        Assert.assertEquals(projectIPage.getTotal(), 1);
        Assert.assertEquals(projectIPage1.getTotal(), 1);
=======
        Assert.assertNotEquals(projectIPage.getTotal(), 0);
        Assert.assertNotEquals(projectIPage1.getTotal(), 0);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * test query project create user
     */
    @Test
    public void testQueryProjectCreatedByUser() {
        Project project = insertOne();

        List<Project> projects = projectMapper.queryProjectCreatedByUser(project.getUserId());

        Assert.assertNotEquals(projects.size(), 0);

    }

    /**
     * test query authed prject list by userId
     */
    @Test
    public void testQueryAuthedProjectListByUserId() {
        Project project = insertOne();

        List<Project> projects = projectMapper.queryProjectCreatedByUser(project.getUserId());

        Assert.assertNotEquals(projects.size(), 0);
    }

    /**
     * test query project expect userId
     */
    @Test
    public void testQueryProjectExceptUserId() {
        Project project = insertOne();

        List<Project> projects = projectMapper.queryProjectExceptUserId(
                100000
        );

        Assert.assertNotEquals(projects.size(), 0);
    }
}