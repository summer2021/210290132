/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.dao.mapper;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.AlertGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

=======
package org.apache.dolphinscheduler.dao.mapper;


import org.apache.dolphinscheduler.common.enums.AlertType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.AccessToken;
import org.apache.dolphinscheduler.dao.entity.AlertGroup;
import org.apache.dolphinscheduler.dao.entity.UserAlertGroup;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
<<<<<<< HEAD
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * AlertGroup mapper test
=======

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 *  AlertGroup mapper test
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback(true)
public class AlertGroupMapperTest {

<<<<<<< HEAD
    @Autowired
    AlertGroupMapper alertGroupMapper;

=======

    @Autowired
    AlertGroupMapper alertGroupMapper;

    @Autowired
    UserAlertGroupMapper userAlertGroupMapper;


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * test insert
     */
    @Test
<<<<<<< HEAD
    public void testInsert() {
        AlertGroup alertGroup = createAlertGroup();
        assertNotNull(alertGroup);
        assertThat(alertGroup.getId(), greaterThan(0));
=======
    public void testInsert(){
        AlertGroup alertGroup = createAlertGroup();
        assertNotNull(alertGroup);
        assertThat(alertGroup.getId(),greaterThan(0));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }


    /**
     * test selectById
     */
    @Test
    public void testSelectById() {
        AlertGroup alertGroup = createAlertGroup();
        //query
        AlertGroup targetAlert = alertGroupMapper.selectById(alertGroup.getId());

        assertEquals(alertGroup, targetAlert);
    }


    /**
     * test page
     */
    @Test
    public void testQueryAlertGroupPage() {

        String groupName = "testGroup";

<<<<<<< HEAD
        Integer count = 1;

        Integer offset = 0;
        Integer size = 1;

        Map<Integer, AlertGroup> alertGroupMap = createAlertGroups(count, groupName);
=======
        Integer count = 4;

        Integer offset = 2;
        Integer size = 2;

        Map<Integer, AlertGroup> alertGroupMap = createAlertGroups(count,groupName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Page page = new Page(offset, size);

        IPage<AlertGroup> alertGroupIPage = alertGroupMapper.queryAlertGroupPage(page, groupName);

        List<AlertGroup> alertGroupList = alertGroupIPage.getRecords();

        assertEquals(alertGroupList.size(), size.intValue());

<<<<<<< HEAD
        for (AlertGroup alertGroup : alertGroupList) {
            AlertGroup resultAlertGroup = alertGroupMap.get(alertGroup.getId());
            if (resultAlertGroup != null) {
                assertEquals(alertGroup, resultAlertGroup);
            }
        }

=======
        for (AlertGroup alertGroup : alertGroupList){
            AlertGroup resultAlertGroup = alertGroupMap.get(alertGroup.getId());
            if (resultAlertGroup != null){
                assertEquals(alertGroup,resultAlertGroup);
            }
        }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * test update
     */
    @Test
<<<<<<< HEAD
    public void testUpdate() {

        AlertGroup alertGroup = createAlertGroup();
        alertGroup.setGroupName("modify GroupName");
=======
    public void testUpdate(){

        AlertGroup alertGroup = createAlertGroup();
        alertGroup.setGroupName("modify GroupName");
        alertGroup.setGroupType(AlertType.SMS);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        alertGroup.setDescription("modify GroupName");
        alertGroup.setUpdateTime(DateUtils.getCurrentDate());

        alertGroupMapper.updateById(alertGroup);

        AlertGroup resultAlertGroup = alertGroupMapper.selectById(alertGroup.getId());

<<<<<<< HEAD
        assertEquals(alertGroup, resultAlertGroup);
    }


=======
        assertEquals(alertGroup,resultAlertGroup);
    }



>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * test delete
     */
    @Test
<<<<<<< HEAD
    public void testDelete() {
=======
    public void testDelete(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        AlertGroup alertGroup = createAlertGroup();

        alertGroupMapper.deleteById(alertGroup.getId());

        AlertGroup resultAlertGroup = alertGroupMapper.selectById(alertGroup.getId());

        assertNull(resultAlertGroup);
    }


<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * test query by groupname
     */
    @Test
    public void testQueryByGroupName() {
<<<<<<< HEAD
        Integer count = 1;
=======
        Integer count = 4;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String groupName = "testGroup";

        Map<Integer, AlertGroup> alertGroupMap = createAlertGroups(count, groupName);

        List<AlertGroup> alertGroupList = alertGroupMapper.queryByGroupName("testGroup");

<<<<<<< HEAD
        compareAlertGroups(alertGroupMap, alertGroupList);
    }

    @Test
    public void testExistGroupName() {
        String groupName = "testGroup";
        createAlertGroups(1, groupName);

        Assert.assertTrue(alertGroupMapper.existGroupName(groupName));
=======

        compareAlertGroups(alertGroupMap, alertGroupList);
    }

    /**
     * test query by userId
     */
    @Test
    public void testQueryByUserId() {
        Integer count = 4;
        Integer userId = 1;

        Map<Integer, AlertGroup> alertGroupMap =
                createAlertGroups(count, userId);

        List<AlertGroup> alertGroupList =
                alertGroupMapper.queryByUserId(userId);

        compareAlertGroups(alertGroupMap,alertGroupList);

    }

    /**
     * test query by alert type
     */
    @Test
    public void testQueryByAlertType() {
        Integer count = 4;

        Map<Integer, AlertGroup> alertGroupMap = createAlertGroups(count);
        List<AlertGroup> alertGroupList = alertGroupMapper.queryByAlertType(AlertType.EMAIL);

        compareAlertGroups(alertGroupMap,alertGroupList);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * test query all group list
     */
    @Test
    public void testQueryAllGroupList() {
<<<<<<< HEAD
        Integer count = 1;
=======
        Integer count = 4;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<Integer, AlertGroup> alertGroupMap = createAlertGroups(count);

        List<AlertGroup> alertGroupList = alertGroupMapper.queryAllGroupList();

<<<<<<< HEAD
        compareAlertGroups(alertGroupMap, alertGroupList);

    }

    /**
     * compare AlertGruops
     *
     * @param alertGroupMap  alertGroupMap
     * @param alertGroupList alertGroupList
     */
    private void compareAlertGroups(Map<Integer, AlertGroup> alertGroupMap,
                                    List<AlertGroup> alertGroupList) {
        // greaterThanOrEqualTo，beacuse maybe db have already exists
        assertThat(alertGroupList.size(), greaterThanOrEqualTo(alertGroupMap.size()));

        for (AlertGroup alertGroup : alertGroupList) {
            AlertGroup resultAlertGroup = alertGroupMap.get(alertGroup.getId());
            if (resultAlertGroup != null) {
                assertEquals(alertGroup, resultAlertGroup);
            }
        }
    }

    /**
     * insert
     *
     * @return AlertGroup
     */
    private AlertGroup createAlertGroup(String groupName) {
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setGroupName(groupName);
        alertGroup.setDescription("alert group 1");
=======
        compareAlertGroups(alertGroupMap,alertGroupList);

    }


    /**
     * compare AlertGruops
     * @param alertGroupMap alertGroupMap
     * @param alertGroupList alertGroupList
     */
    private void compareAlertGroups(Map<Integer,AlertGroup> alertGroupMap,
                                    List<AlertGroup> alertGroupList){
        // greaterThanOrEqualTo，beacuse maybe db have already exists
        assertThat(alertGroupList.size(),greaterThanOrEqualTo(alertGroupMap.size()));

        for (AlertGroup alertGroup : alertGroupList){
            AlertGroup resultAlertGroup = alertGroupMap.get(alertGroup.getId());
            if (resultAlertGroup != null){
                assertEquals(alertGroup,resultAlertGroup);
            }
        }
    }
    /**
     * insert
     * @return AlertGroup
     */
    private AlertGroup createAlertGroup(String groupName){
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setGroupName(groupName);
        alertGroup.setDescription("alert group 1");
        alertGroup.setGroupType(AlertType.EMAIL);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        alertGroup.setCreateTime(DateUtils.getCurrentDate());
        alertGroup.setUpdateTime(DateUtils.getCurrentDate());

        alertGroupMapper.insert(alertGroup);

        return alertGroup;
    }

    /**
     * insert
<<<<<<< HEAD
     *
     * @return AlertGroup
     */
    private AlertGroup createAlertGroup() {
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setGroupName("testGroup");
        alertGroup.setDescription("testGroup");
=======
     * @return AlertGroup
     */
    private AlertGroup createAlertGroup(){
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setGroupName("testGroup");
        alertGroup.setDescription("testGroup");
        alertGroup.setGroupType(AlertType.EMAIL);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        alertGroup.setCreateTime(DateUtils.getCurrentDate());
        alertGroup.setUpdateTime(DateUtils.getCurrentDate());

        alertGroupMapper.insert(alertGroup);

        return alertGroup;
    }

    /**
     * create AlertGroups
<<<<<<< HEAD
     *
     * @param count     create AlertGroup count
     * @param groupName groupName
     * @return AlertGroup map
     */
    private Map<Integer, AlertGroup> createAlertGroups(
        Integer count, String groupName) {
        Map<Integer, AlertGroup> alertGroupMap = new HashMap<>();

        AlertGroup alertGroup = null;
        for (int i = 0; i < count; i++) {
            alertGroup = createAlertGroup(groupName);
            alertGroupMap.put(alertGroup.getId(), alertGroup);
=======
     * @param count create AlertGroup count
     * @param groupName groupName
     * @return AlertGroup map
     */
    private Map<Integer,AlertGroup> createAlertGroups(
            Integer count,String groupName){
        Map<Integer,AlertGroup> alertGroupMap = new HashMap<>();

        AlertGroup  alertGroup = null;
        for (int i = 0 ; i < count; i++){
            alertGroup = createAlertGroup(groupName);
            alertGroupMap.put(alertGroup.getId(),alertGroup);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }

        return alertGroupMap;
    }

<<<<<<< HEAD
    /**
     * create AlertGroups
     *
     * @param count create AlertGroup count
     * @return AlertGroup map
     */
    private Map<Integer, AlertGroup> createAlertGroups(
        Integer count) {
        Map<Integer, AlertGroup> alertGroupMap = new HashMap<>();

        AlertGroup alertGroup = null;
        for (int i = 0; i < count; i++) {
            alertGroup = createAlertGroup();
            alertGroupMap.put(alertGroup.getId(), alertGroup);
=======

    /**
     * create AlertGroups
     * @param count create AlertGroup count
     * @return AlertGroup map
     */
    private Map<Integer,AlertGroup> createAlertGroups(
            Integer count){
        Map<Integer,AlertGroup> alertGroupMap = new HashMap<>();

        AlertGroup  alertGroup = null;
        for (int i = 0 ; i < count; i++){
            alertGroup = createAlertGroup();
            alertGroupMap.put(alertGroup.getId(),alertGroup);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }

        return alertGroupMap;
    }

<<<<<<< HEAD
=======

    /**
     * create AlertGroups
     * @param count create AlertGroup count
     * @return AlertGroup map
     */
    private Map<Integer,AlertGroup> createAlertGroups(
            Integer count,Integer userId){
        Map<Integer,AlertGroup> alertGroupMap = new HashMap<>();

        AlertGroup  alertGroup = null;
        for (int i = 0 ; i < count; i++){
            alertGroup = createAlertGroup();

            createUserAlertGroup(userId,alertGroup.getId());

            alertGroupMap.put(alertGroup.getId(),alertGroup);
        }

        return alertGroupMap;
    }

    /**
     * create AlertGroup
     * @param userId userId
     * @param alertgroupId alertgroupId
     * @return UserAlertGroup
     */
    private UserAlertGroup createUserAlertGroup(
            Integer userId,Integer alertgroupId){
        UserAlertGroup userAlertGroup = new UserAlertGroup();
        userAlertGroup.setAlertgroupId(alertgroupId);
        userAlertGroup.setUserId(userId);
        userAlertGroup.setCreateTime(DateUtils.getCurrentDate());
        userAlertGroup.setUpdateTime(DateUtils.getCurrentDate());

        userAlertGroupMapper.insert(userAlertGroup);

        return userAlertGroup;
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}