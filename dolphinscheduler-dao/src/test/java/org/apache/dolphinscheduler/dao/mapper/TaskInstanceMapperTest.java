/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.dao.mapper;

import org.apache.dolphinscheduler.common.enums.ExecutionStatus;
import org.apache.dolphinscheduler.common.enums.Flag;
import org.apache.dolphinscheduler.common.enums.TaskType;
import org.apache.dolphinscheduler.dao.entity.ExecuteStatusCount;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
import org.apache.dolphinscheduler.dao.entity.ProcessInstance;
import org.apache.dolphinscheduler.dao.entity.TaskInstance;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class TaskInstanceMapperTest {

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Autowired
    TaskInstanceMapper taskInstanceMapper;

    @Autowired
    ProcessDefinitionMapper processDefinitionMapper;

    @Autowired
    ProcessInstanceMapper processInstanceMapper;

    @Autowired
    ProcessInstanceMapMapper processInstanceMapMapper;

    private int processInstanceId;

    @Before
    public void before() {
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setWarningGroupId(0);
        processInstance.setCommandParam("");
        processInstanceMapper.insert(processInstance);
        processInstanceId = processInstance.getId();
    }

    /**
     * insert
     *
     * @return TaskInstance
     */
<<<<<<< HEAD
    private TaskInstance insertTaskInstance(int processInstanceId) {
        //insertOne
        return insertTaskInstance(processInstanceId, TaskType.SHELL.getDesc());
    }

    /**
     * insert
     *
     * @return ProcessInstance
     */
    private ProcessInstance insertProcessInstance() {
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setId(1);
        processInstance.setName("taskName");
        processInstance.setState(ExecutionStatus.RUNNING_EXECUTION);
        processInstance.setStartTime(new Date());
        processInstance.setEndTime(new Date());
        processInstance.setProcessDefinitionCode(1L);
        processInstanceMapper.insert(processInstance);
        return processInstanceMapper.queryByProcessDefineCode(1L,1).get(0);
=======
    private TaskInstance insertOne() {
        //insertOne
        return insertOne("us task", processInstanceId, ExecutionStatus.RUNNING_EXECUTION, TaskType.SHELL.toString(),1);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * construct a task instance and then insert
     */
<<<<<<< HEAD
    private TaskInstance insertTaskInstance(int processInstanceId, String taskType) {
        TaskInstance taskInstance = new TaskInstance();
        taskInstance.setFlag(Flag.YES);
        taskInstance.setName("us task");
        taskInstance.setState(ExecutionStatus.RUNNING_EXECUTION);
        taskInstance.setStartTime(new Date());
        taskInstance.setEndTime(new Date());
        taskInstance.setProcessInstanceId(processInstanceId);
        taskInstance.setTaskType(taskType);
=======
    private TaskInstance insertOne(String taskName, int processInstanceId, ExecutionStatus state, String taskType,int processDefinitionId) {
        TaskInstance taskInstance = new TaskInstance();
        taskInstance.setFlag(Flag.YES);
        taskInstance.setName(taskName);
        taskInstance.setState(state);
        taskInstance.setStartTime(new Date());
        taskInstance.setEndTime(new Date());
        taskInstance.setTaskJson("{}");
        taskInstance.setProcessInstanceId(processInstanceId);
        taskInstance.setTaskType(taskType);
        taskInstance.setProcessDefinitionId(processDefinitionId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskInstanceMapper.insert(taskInstance);
        return taskInstance;
    }

    /**
     * test update
     */
    @Test
    public void testUpdate() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance taskInstance = insertTaskInstance(processInstance.getId());
        // update
=======
        //insertOne
        TaskInstance taskInstance = insertOne();
        //update
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        int update = taskInstanceMapper.updateById(taskInstance);
        Assert.assertEquals(1, update);
        taskInstanceMapper.deleteById(taskInstance.getId());
    }

    /**
     * test delete
     */
    @Test
    public void testDelete() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance taskInstance = insertTaskInstance(processInstance.getId());

=======
        TaskInstance taskInstance = insertOne();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        int delete = taskInstanceMapper.deleteById(taskInstance.getId());
        Assert.assertEquals(1, delete);
    }

    /**
     * test query
     */
    @Test
    public void testQuery() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance taskInstance = insertTaskInstance(processInstance.getId());
=======
        TaskInstance taskInstance = insertOne();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //query
        List<TaskInstance> taskInstances = taskInstanceMapper.selectList(null);
        taskInstanceMapper.deleteById(taskInstance.getId());
        Assert.assertNotEquals(taskInstances.size(), 0);
    }

    /**
     * test query task instance by process instance id and state
     */
    @Test
    public void testQueryTaskByProcessIdAndState() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
        task.setProcessInstanceId(processInstance.getId());
=======
        TaskInstance task = insertOne();
        task.setProcessInstanceId(processInstanceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskInstanceMapper.updateById(task);
        List<Integer> taskInstances = taskInstanceMapper.queryTaskByProcessIdAndState(
                task.getProcessInstanceId(),
                ExecutionStatus.RUNNING_EXECUTION.ordinal()
        );
        taskInstanceMapper.deleteById(task.getId());
        Assert.assertNotEquals(taskInstances.size(), 0);
    }

    /**
     * test find valid task list by process instance id
     */
    @Test
    public void testFindValidTaskListByProcessId() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
        TaskInstance task2 = insertTaskInstance(processInstance.getId());
        task.setProcessInstanceId(processInstance.getId());
        task2.setProcessInstanceId(processInstance.getId());
=======
        TaskInstance task = insertOne();
        TaskInstance task2 = insertOne();
        task.setProcessInstanceId(processInstanceId);
        task2.setProcessInstanceId(processInstanceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskInstanceMapper.updateById(task);
        taskInstanceMapper.updateById(task2);

        List<TaskInstance> taskInstances = taskInstanceMapper.findValidTaskListByProcessId(
                task.getProcessInstanceId(),
                Flag.YES
        );

        task2.setFlag(Flag.NO);
        taskInstanceMapper.updateById(task2);
        List<TaskInstance> taskInstances1 = taskInstanceMapper.findValidTaskListByProcessId(task.getProcessInstanceId(),
                Flag.NO);

        taskInstanceMapper.deleteById(task2.getId());
        taskInstanceMapper.deleteById(task.getId());
        Assert.assertNotEquals(taskInstances.size(), 0);
        Assert.assertNotEquals(taskInstances1.size(), 0);
    }

    /**
     * test query by host and status
     */
    @Test
    public void testQueryByHostAndStatus() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
=======
        TaskInstance task = insertOne();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        task.setHost("111.111.11.11");
        taskInstanceMapper.updateById(task);

        List<TaskInstance> taskInstances = taskInstanceMapper.queryByHostAndStatus(
                task.getHost(), new int[]{ExecutionStatus.RUNNING_EXECUTION.ordinal()}
        );
        taskInstanceMapper.deleteById(task.getId());
        Assert.assertNotEquals(taskInstances.size(), 0);
    }

    /**
     * test set failover by host and state array
     */
    @Test
    public void testSetFailoverByHostAndStateArray() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
=======
        TaskInstance task = insertOne();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        task.setHost("111.111.11.11");
        taskInstanceMapper.updateById(task);

        int setResult = taskInstanceMapper.setFailoverByHostAndStateArray(
                task.getHost(),
                new int[]{ExecutionStatus.RUNNING_EXECUTION.ordinal()},
                ExecutionStatus.NEED_FAULT_TOLERANCE
        );
        taskInstanceMapper.deleteById(task.getId());
        Assert.assertNotEquals(setResult, 0);
    }

    /**
     * test query by task instance id and name
     */
    @Test
    public void testQueryByInstanceIdAndName() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
=======
        TaskInstance task = insertOne();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        task.setHost("111.111.11.11");
        taskInstanceMapper.updateById(task);

        TaskInstance taskInstance = taskInstanceMapper.queryByInstanceIdAndName(
                task.getProcessInstanceId(),
                task.getName()
        );
        taskInstanceMapper.deleteById(task.getId());
        Assert.assertNotEquals(taskInstance, null);
    }

    /**
     * test count task instance
     */
    @Test
    public void testCountTask() {
<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
        ProcessDefinition definition = new ProcessDefinition();
        definition.setCode(1L);
        definition.setProjectCode(1111L);
        definition.setCreateTime(new Date());
        definition.setUpdateTime(new Date());
        processDefinitionMapper.insert(definition);
        //task.setProcessDefinitionId(definition.getId());
        taskInstanceMapper.updateById(task);

        int countTask = taskInstanceMapper.countTask(
                new Long[0],
                new int[0]
        );
        int countTask2 = taskInstanceMapper.countTask(
                new Long[]{definition.getProjectCode()},
=======
        TaskInstance task = insertOne();

        ProcessDefinition definition = new ProcessDefinition();
        definition.setProjectId(1111);
        processDefinitionMapper.insert(definition);
        task.setProcessDefinitionId(definition.getId());
        taskInstanceMapper.updateById(task);

        int countTask = taskInstanceMapper.countTask(
                new Integer[0],
                new int[0]
        );
        int countTask2 = taskInstanceMapper.countTask(
                new Integer[]{definition.getProjectId()},
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                new int[]{task.getId()}
        );
        taskInstanceMapper.deleteById(task.getId());
        processDefinitionMapper.deleteById(definition.getId());
<<<<<<< HEAD
        Assert.assertEquals(countTask, 0);
        Assert.assertEquals(countTask2, 0);
=======
        Assert.assertNotEquals(countTask, 0);
        Assert.assertNotEquals(countTask2, 0);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9


    }

    /**
     * test count task instance state by user
     */
    @Test
    public void testCountTaskInstanceStateByUser() {

<<<<<<< HEAD
        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());
        ProcessDefinition definition = new ProcessDefinition();
        definition.setCode(1111L);
        definition.setProjectCode(1111L);
        definition.setCreateTime(new Date());
        definition.setUpdateTime(new Date());
        processDefinitionMapper.insert(definition);
        //task.setProcessDefinitionId(definition.getId());
=======
        TaskInstance task = insertOne();
        ProcessDefinition definition = new ProcessDefinition();
        definition.setProjectId(1111);
        processDefinitionMapper.insert(definition);
        task.setProcessDefinitionId(definition.getId());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskInstanceMapper.updateById(task);


        List<ExecuteStatusCount> count = taskInstanceMapper.countTaskInstanceStateByUser(
                null, null,
<<<<<<< HEAD
                new Long[]{definition.getProjectCode()}
=======
                new Integer[]{definition.getProjectId()}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        );

        processDefinitionMapper.deleteById(definition.getId());
        taskInstanceMapper.deleteById(task.getId());
    }
<<<<<<< HEAD

    /**
     * test page
     */
    @Test
    public void testQueryTaskInstanceListPaging() {
        ProcessDefinition definition = new ProcessDefinition();
        definition.setCode(1L);
        definition.setProjectCode(1111L);
        definition.setCreateTime(new Date());
        definition.setUpdateTime(new Date());
        processDefinitionMapper.insert(definition);

        // insert ProcessInstance
        ProcessInstance processInstance = insertProcessInstance();

        // insert taskInstance
        TaskInstance task = insertTaskInstance(processInstance.getId());

        Page<TaskInstance> page = new Page(1, 3);
        IPage<TaskInstance> taskInstanceIPage = taskInstanceMapper.queryTaskInstanceListPaging(
                page,
                definition.getProjectCode(),
                task.getProcessInstanceId(),
                "",
                "",
                "",
                0,
                new int[0],
                "",
                null, null
        );
        processInstanceMapper.deleteById(processInstance.getId());
        taskInstanceMapper.deleteById(task.getId());
        processDefinitionMapper.deleteById(definition.getId());
        Assert.assertEquals(taskInstanceIPage.getTotal(), 0);

    }
}
=======
}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
