/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.dolphinscheduler.dao.mapper;

<<<<<<< HEAD
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.AccessToken;
import org.apache.dolphinscheduler.dao.entity.User;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

=======
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.AccessToken;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dolphinscheduler.dao.entity.User;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
=======
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

/**
 * AccessToken mapper test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
<<<<<<< HEAD
@Rollback
=======
@Rollback(true)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
public class AccessTokenMapperTest {

    @Autowired
    AccessTokenMapper accessTokenMapper;

    @Autowired
    UserMapper userMapper;

    /**
     * test insert
<<<<<<< HEAD
     */
    @Test
    public void testInsert() throws Exception {
        Integer userId = 1;

        AccessToken accessToken = createAccessToken(userId);
        assertThat(accessToken.getId(), greaterThan(0));
    }

    /**
     * test delete AccessToken By UserId
     */
    @Test
    public void testDeleteAccessTokenByUserId() throws Exception {
        Integer userId = 1;
        int insertCount = 0;

        for (int i = 0; i < 10; i++) {
            try {
                createAccessToken(userId);
                insertCount++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int deleteCount = accessTokenMapper.deleteAccessTokenByUserId(userId);
        Assert.assertEquals(insertCount, deleteCount);
    }


    /**
     * test select by id
     *
     * @throws Exception
     */
    @Test
    public void testSelectById() throws Exception {
=======
     * @throws Exception
     */
    @Test
    public void testInsert() throws Exception{
        Integer userId = 1;

        AccessToken accessToken = createAccessToken(userId);
        assertNotNull(accessToken.getId());
        assertThat(accessToken.getId(), greaterThan(0));
    }


    /**
     * test select by id
     * @throws Exception
     */
    @Test
    public void testSelectById() throws Exception{
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Integer userId = 1;
        AccessToken accessToken = createAccessToken(userId);
        AccessToken resultAccessToken = accessTokenMapper.selectById(accessToken.getId());
        assertEquals(accessToken, resultAccessToken);
    }

    /**
     * test hashCode method
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @throws Exception
     */
    @Test
    public void testHashCodeMethod() throws Exception {
        Integer userId = 1;
        AccessToken accessToken = createAccessToken(userId);
        AccessToken resultAccessToken = accessTokenMapper.selectById(accessToken.getId());
        boolean flag = accessToken.equals(resultAccessToken);
        assertTrue(flag);
    }

    /**
     * test equals method
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @throws Exception
     */
    @Test
    public void testEqualsMethod() throws Exception {
        Integer userId = 1;
        AccessToken accessToken = createAccessToken(userId);
        int result = accessToken.hashCode();
        assertNotNull(result);
    }

    /**
     * test page
     */
    @Test
<<<<<<< HEAD
    public void testSelectAccessTokenPage() throws Exception {
=======
    public void testSelectAccessTokenPage() throws Exception{
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Integer count = 4;
        String userName = "zhangsan";

        Integer offset = 2;
        Integer size = 2;

        Map<Integer, AccessToken> accessTokenMap = createAccessTokens(count, userName);

        Page page = new Page(offset, size);
        IPage<AccessToken> accessTokenPage = accessTokenMapper.selectAccessTokenPage(page, userName, 0);

<<<<<<< HEAD
        assertEquals(Integer.valueOf(accessTokenPage.getRecords().size()), size);

        for (AccessToken accessToken : accessTokenPage.getRecords()) {
            AccessToken resultAccessToken = accessTokenMap.get(accessToken.getId());
            assertEquals(accessToken, resultAccessToken);
=======
        assertEquals(Integer.valueOf(accessTokenPage.getRecords().size()),size);

        for (AccessToken accessToken : accessTokenPage.getRecords()){
            AccessToken resultAccessToken = accessTokenMap.get(accessToken.getId());
            assertEquals(accessToken,resultAccessToken);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
    }


    /**
     * test update
     */
    @Test
<<<<<<< HEAD
    public void testUpdate() throws Exception {
=======
    public void testUpdate() throws Exception{
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Integer userId = 1;
        AccessToken accessToken = createAccessToken(userId);
        //update
        accessToken.setToken("56789");
        accessToken.setExpireTime(DateUtils.getCurrentDate());
        accessToken.setUpdateTime(DateUtils.getCurrentDate());
<<<<<<< HEAD
        int status = accessTokenMapper.updateById(accessToken);
        if (status != 1) {
            Assert.fail("update access token fail");
        }
=======
        accessTokenMapper.updateById(accessToken);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        AccessToken resultAccessToken = accessTokenMapper.selectById(accessToken.getId());
        assertEquals(accessToken, resultAccessToken);
    }

    /**
     * test delete
     */
    @Test
<<<<<<< HEAD
    public void testDelete() throws Exception {
        Integer userId = 1;

        AccessToken accessToken = createAccessToken(userId);
        int status = accessTokenMapper.deleteById(accessToken.getId());
        if (status != 1) {
            Assert.fail("delete access token data fail");
        }
=======
    public void testDelete() throws Exception{
        Integer userId = 1;

        AccessToken accessToken = createAccessToken(userId);
        accessTokenMapper.deleteById(accessToken.getId());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        AccessToken resultAccessToken =
                accessTokenMapper.selectById(accessToken.getId());
        assertNull(resultAccessToken);
    }


    /**
     * create accessTokens
<<<<<<< HEAD
     *
     * @param count    create accessToken count
=======
     * @param count create accessToken count
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param userName username
     * @return accessToken map
     * @throws Exception
     */
<<<<<<< HEAD
    private Map<Integer, AccessToken> createAccessTokens(
            Integer count, String userName) throws Exception {

        User user = createUser(userName);

        Map<Integer, AccessToken> accessTokenMap = new HashMap<>();
        for (int i = 1; i <= count; i++) {
            AccessToken accessToken = createAccessToken(user.getId(), userName);

            accessTokenMap.put(accessToken.getId(), accessToken);
=======
    private Map<Integer,AccessToken> createAccessTokens(
            Integer count,String userName) throws Exception{

        User user = createUser(userName);

        Map<Integer,AccessToken> accessTokenMap = new HashMap<>();
        for (int i = 1 ; i<= count ; i++){
            AccessToken accessToken = createAccessToken(user.getId(),userName);

            accessTokenMap.put(accessToken.getId(),accessToken);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }

        return accessTokenMap;
    }

    /**
     * create user
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param userName userName
     * @return user
     * @throws Exception
     */
<<<<<<< HEAD
    private User createUser(String userName) throws Exception {
=======
    private User createUser(String userName) throws Exception{
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserName(userName);
        user.setUserPassword("123");
        user.setUserType(UserType.GENERAL_USER);
        user.setEmail("test@qq.com");
        user.setPhone("13102557272");
        user.setTenantId(1);
        user.setCreateTime(DateUtils.getCurrentDate());
        user.setUpdateTime(DateUtils.getCurrentDate());
        user.setQueue("default");

<<<<<<< HEAD
        int status = userMapper.insert(user);

        if (status != 1) {
            Assert.fail("insert user data error");
        }
=======
        userMapper.insert(user);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        return user;
    }

    /**
     * create access token
<<<<<<< HEAD
     *
     * @param userId   userId
=======
     * @param userId userId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param userName userName
     * @return accessToken
     * @throws Exception
     */
<<<<<<< HEAD
    private AccessToken createAccessToken(Integer userId, String userName) throws Exception {
=======
    private AccessToken createAccessToken(Integer userId,String userName)throws Exception{
        Random random = new Random();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //insertOne
        AccessToken accessToken = new AccessToken();
        accessToken.setUserName(userName);
        accessToken.setUserId(userId);
<<<<<<< HEAD
        accessToken.setToken(String.valueOf(ThreadLocalRandom.current().nextLong()));
=======
        accessToken.setToken(String.valueOf(random.nextLong()));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        accessToken.setCreateTime(DateUtils.getCurrentDate());
        accessToken.setUpdateTime(DateUtils.getCurrentDate());
        accessToken.setExpireTime(DateUtils.getCurrentDate());

<<<<<<< HEAD
        int status = accessTokenMapper.insert(accessToken);

        if (status != 1) {
            Assert.fail("insert data error");
        }
=======
        accessTokenMapper.insert(accessToken);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return accessToken;
    }

    /**
     * create access token
<<<<<<< HEAD
     *
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param userId userId
     * @return accessToken
     * @throws Exception
     */
<<<<<<< HEAD
    private AccessToken createAccessToken(Integer userId) throws Exception {
        return createAccessToken(userId, null);
=======
    private AccessToken createAccessToken(Integer userId)throws Exception{
        return createAccessToken(userId,null);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

}