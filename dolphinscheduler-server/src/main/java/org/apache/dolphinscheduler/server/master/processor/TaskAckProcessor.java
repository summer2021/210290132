/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.server.master.processor;

<<<<<<< HEAD
import org.apache.dolphinscheduler.common.enums.ExecutionStatus;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
=======
import io.netty.channel.Channel;
import org.apache.dolphinscheduler.common.enums.ExecutionStatus;
import org.apache.dolphinscheduler.common.utils.Preconditions;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.remote.command.Command;
import org.apache.dolphinscheduler.remote.command.CommandType;
import org.apache.dolphinscheduler.remote.command.TaskExecuteAckCommand;
import org.apache.dolphinscheduler.remote.processor.NettyRequestProcessor;
import org.apache.dolphinscheduler.remote.utils.ChannelUtils;
<<<<<<< HEAD
=======
import org.apache.dolphinscheduler.remote.utils.FastJsonSerializer;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.server.master.cache.TaskInstanceCacheManager;
import org.apache.dolphinscheduler.server.master.cache.impl.TaskInstanceCacheManagerImpl;
import org.apache.dolphinscheduler.server.master.processor.queue.TaskResponseEvent;
import org.apache.dolphinscheduler.server.master.processor.queue.TaskResponseService;
<<<<<<< HEAD
import org.apache.dolphinscheduler.server.master.runner.WorkflowExecuteThread;
import org.apache.dolphinscheduler.service.bean.SpringApplicationContext;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

import io.netty.channel.Channel;

/**
 * task ack processor
=======
import org.apache.dolphinscheduler.service.bean.SpringApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *  task ack processor
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
 */
public class TaskAckProcessor implements NettyRequestProcessor {

    private final Logger logger = LoggerFactory.getLogger(TaskAckProcessor.class);

    /**
     * process service
     */
    private final TaskResponseService taskResponseService;

    /**
     * taskInstance cache manager
     */
    private final TaskInstanceCacheManager taskInstanceCacheManager;

<<<<<<< HEAD
    public TaskAckProcessor() {
=======
    public TaskAckProcessor(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        this.taskResponseService = SpringApplicationContext.getBean(TaskResponseService.class);
        this.taskInstanceCacheManager = SpringApplicationContext.getBean(TaskInstanceCacheManagerImpl.class);
    }

<<<<<<< HEAD
    public void init(ConcurrentHashMap<Integer, WorkflowExecuteThread> processInstanceExecMaps) {
        this.taskResponseService.init(processInstanceExecMaps);
    }

    /**
     * task ack process
     *
=======
    /**
     * task ack process
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param channel channel channel
     * @param command command TaskExecuteAckCommand
     */
    @Override
    public void process(Channel channel, Command command) {
        Preconditions.checkArgument(CommandType.TASK_EXECUTE_ACK == command.getType(), String.format("invalid command type : %s", command.getType()));
<<<<<<< HEAD
        TaskExecuteAckCommand taskAckCommand = JSONUtils.parseObject(command.getBody(), TaskExecuteAckCommand.class);
=======
        TaskExecuteAckCommand taskAckCommand = FastJsonSerializer.deserialize(command.getBody(), TaskExecuteAckCommand.class);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info("taskAckCommand : {}", taskAckCommand);

        taskInstanceCacheManager.cacheTaskInstance(taskAckCommand);

        String workerAddress = ChannelUtils.toAddress(channel).getAddress();

        ExecutionStatus ackStatus = ExecutionStatus.of(taskAckCommand.getStatus());

        // TaskResponseEvent
        TaskResponseEvent taskResponseEvent = TaskResponseEvent.newAck(ackStatus,
                taskAckCommand.getStartTime(),
                workerAddress,
                taskAckCommand.getExecutePath(),
                taskAckCommand.getLogPath(),
                taskAckCommand.getTaskInstanceId(),
<<<<<<< HEAD
                channel,
                taskAckCommand.getProcessInstanceId());
=======
                channel);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        taskResponseService.addResponse(taskResponseEvent);
    }

}
