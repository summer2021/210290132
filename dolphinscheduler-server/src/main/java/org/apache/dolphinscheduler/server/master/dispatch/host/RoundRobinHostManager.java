/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.server.master.dispatch.host;

<<<<<<< HEAD
import org.apache.dolphinscheduler.server.master.dispatch.host.assign.HostWorker;
import org.apache.dolphinscheduler.server.master.dispatch.host.assign.RoundRobinSelector;
=======
import org.apache.dolphinscheduler.remote.utils.Host;
import org.apache.dolphinscheduler.server.master.dispatch.host.assign.RoundRobinSelector;
import org.apache.dolphinscheduler.server.master.dispatch.host.assign.Selector;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

import java.util.Collection;

/**
 *  round robin host manager
 */
public class RoundRobinHostManager extends CommonHostManager {

    /**
     * selector
     */
<<<<<<< HEAD
    private final RoundRobinSelector selector;
=======
    private final Selector<Host> selector;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * set round robin
     */
    public RoundRobinHostManager() {
<<<<<<< HEAD
        this.selector = new RoundRobinSelector();
    }

    @Override
    public HostWorker select(Collection<HostWorker> nodes) {
=======
        this.selector = new RoundRobinSelector<>();
    }

    @Override
    public Host select(Collection<Host> nodes) {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return selector.select(nodes);
    }

}
