/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.server.builder;

<<<<<<< HEAD
import static org.apache.dolphinscheduler.common.Constants.SEC_2_MINUTES_TIME_UNIT;

import org.apache.dolphinscheduler.common.enums.TaskTimeoutStrategy;
import org.apache.dolphinscheduler.common.enums.TimeoutFlag;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
import org.apache.dolphinscheduler.dao.entity.ProcessInstance;
import org.apache.dolphinscheduler.dao.entity.TaskDefinition;
import org.apache.dolphinscheduler.dao.entity.TaskInstance;
import org.apache.dolphinscheduler.spi.task.request.ProcedureTaskExecutionContext;
import org.apache.dolphinscheduler.server.entity.TaskExecutionContext;
import org.apache.dolphinscheduler.spi.task.request.DataxTaskExecutionContext;
import org.apache.dolphinscheduler.spi.task.request.SQLTaskExecutionContext;
import org.apache.dolphinscheduler.spi.task.request.SqoopTaskExecutionContext;
=======
import org.apache.dolphinscheduler.dao.entity.*;
import org.apache.dolphinscheduler.server.entity.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

/**
 *  TaskExecutionContext builder
 */
public class TaskExecutionContextBuilder {

<<<<<<< HEAD
    public static TaskExecutionContextBuilder get() {
=======
    public static TaskExecutionContextBuilder get(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return new TaskExecutionContextBuilder();
    }

    private TaskExecutionContext taskExecutionContext =  new TaskExecutionContext();

    /**
     * build taskInstance related info
     *
     * @param taskInstance taskInstance
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildTaskInstanceRelatedInfo(TaskInstance taskInstance) {
        taskExecutionContext.setTaskInstanceId(taskInstance.getId());
        taskExecutionContext.setTaskName(taskInstance.getName());
        taskExecutionContext.setFirstSubmitTime(taskInstance.getFirstSubmitTime());
        taskExecutionContext.setStartTime(taskInstance.getStartTime());
        taskExecutionContext.setTaskType(taskInstance.getTaskType());
        taskExecutionContext.setLogPath(taskInstance.getLogPath());
        taskExecutionContext.setWorkerGroup(taskInstance.getWorkerGroup());
        taskExecutionContext.setEnvironmentConfig(taskInstance.getEnvironmentConfig());
        taskExecutionContext.setHost(taskInstance.getHost());
        taskExecutionContext.setResources(taskInstance.getResources());
        taskExecutionContext.setDelayTime(taskInstance.getDelayTime());
        taskExecutionContext.setVarPool(taskInstance.getVarPool());
        return this;
    }

    public TaskExecutionContextBuilder buildTaskDefinitionRelatedInfo(TaskDefinition taskDefinition) {
        taskExecutionContext.setTaskTimeout(Integer.MAX_VALUE);
        if (taskDefinition.getTimeoutFlag() == TimeoutFlag.OPEN) {
            taskExecutionContext.setTaskTimeoutStrategy(taskDefinition.getTimeoutNotifyStrategy());
            if (taskDefinition.getTimeoutNotifyStrategy() == TaskTimeoutStrategy.FAILED
                    || taskDefinition.getTimeoutNotifyStrategy() == TaskTimeoutStrategy.WARNFAILED) {
                taskExecutionContext.setTaskTimeout(Math.min(taskDefinition.getTimeout() * SEC_2_MINUTES_TIME_UNIT, Integer.MAX_VALUE));
            }
        }
        taskExecutionContext.setTaskParams(taskDefinition.getTaskParams());
        return this;
    }
=======
    public TaskExecutionContextBuilder buildTaskInstanceRelatedInfo(TaskInstance taskInstance){
        taskExecutionContext.setTaskInstanceId(taskInstance.getId());
        taskExecutionContext.setTaskName(taskInstance.getName());
        taskExecutionContext.setStartTime(taskInstance.getStartTime());
        taskExecutionContext.setTaskType(taskInstance.getTaskType());
        taskExecutionContext.setLogPath(taskInstance.getLogPath());
        taskExecutionContext.setTaskJson(taskInstance.getTaskJson());
        taskExecutionContext.setWorkerGroup(taskInstance.getWorkerGroup());
        taskExecutionContext.setHost(taskInstance.getHost());
        taskExecutionContext.setResources(taskInstance.getResources());
        return this;
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * build processInstance related info
     *
     * @param processInstance processInstance
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildProcessInstanceRelatedInfo(ProcessInstance processInstance) {
=======
    public TaskExecutionContextBuilder buildProcessInstanceRelatedInfo(ProcessInstance processInstance){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskExecutionContext.setProcessInstanceId(processInstance.getId());
        taskExecutionContext.setScheduleTime(processInstance.getScheduleTime());
        taskExecutionContext.setGlobalParams(processInstance.getGlobalParams());
        taskExecutionContext.setExecutorId(processInstance.getExecutorId());
        taskExecutionContext.setCmdTypeIfComplement(processInstance.getCmdTypeIfComplement().getCode());
        taskExecutionContext.setTenantCode(processInstance.getTenantCode());
        taskExecutionContext.setQueue(processInstance.getQueue());
        return this;
    }

    /**
     * build processDefinition related info
     *
     * @param processDefinition processDefinition
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildProcessDefinitionRelatedInfo(ProcessDefinition processDefinition) {
        taskExecutionContext.setProcessDefineCode(processDefinition.getCode());
        taskExecutionContext.setProcessDefineVersion(processDefinition.getVersion());
        taskExecutionContext.setProjectCode(processDefinition.getProjectCode());
        return this;
    }

=======
    public TaskExecutionContextBuilder buildProcessDefinitionRelatedInfo(ProcessDefinition processDefinition){
        taskExecutionContext.setProcessDefineId(processDefinition.getId());
        taskExecutionContext.setProjectId(processDefinition.getProjectId());
        return this;
    }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * build SQLTask related info
     *
     * @param sqlTaskExecutionContext sqlTaskExecutionContext
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildSQLTaskRelatedInfo(SQLTaskExecutionContext sqlTaskExecutionContext) {
=======
    public TaskExecutionContextBuilder buildSQLTaskRelatedInfo(SQLTaskExecutionContext sqlTaskExecutionContext){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskExecutionContext.setSqlTaskExecutionContext(sqlTaskExecutionContext);
        return this;
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * build DataxTask related info
     *
     * @param dataxTaskExecutionContext dataxTaskExecutionContext
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildDataxTaskRelatedInfo(DataxTaskExecutionContext dataxTaskExecutionContext) {
=======
    public TaskExecutionContextBuilder buildDataxTaskRelatedInfo(DataxTaskExecutionContext dataxTaskExecutionContext){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskExecutionContext.setDataxTaskExecutionContext(dataxTaskExecutionContext);
        return this;
    }

    /**
     * build procedureTask related info
     *
     * @param procedureTaskExecutionContext procedureTaskExecutionContext
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildProcedureTaskRelatedInfo(ProcedureTaskExecutionContext procedureTaskExecutionContext) {
=======
    public TaskExecutionContextBuilder buildProcedureTaskRelatedInfo(ProcedureTaskExecutionContext procedureTaskExecutionContext){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskExecutionContext.setProcedureTaskExecutionContext(procedureTaskExecutionContext);
        return this;
    }

    /**
     * build sqoopTask related info
     *
     * @param sqoopTaskExecutionContext sqoopTaskExecutionContext
     * @return TaskExecutionContextBuilder
     */
<<<<<<< HEAD
    public TaskExecutionContextBuilder buildSqoopTaskRelatedInfo(SqoopTaskExecutionContext sqoopTaskExecutionContext) {
=======
    public TaskExecutionContextBuilder buildSqoopTaskRelatedInfo(SqoopTaskExecutionContext sqoopTaskExecutionContext){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        taskExecutionContext.setSqoopTaskExecutionContext(sqoopTaskExecutionContext);
        return this;
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * create
     *
     * @return taskExecutionContext
     */
<<<<<<< HEAD
    public TaskExecutionContext create() {
=======
    public TaskExecutionContext create(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return taskExecutionContext;
    }

}
