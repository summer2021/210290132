/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.server.log;

import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.apache.dolphinscheduler.common.utils.LoggerUtils;
import org.apache.dolphinscheduler.remote.command.Command;
import org.apache.dolphinscheduler.remote.command.CommandType;
import org.apache.dolphinscheduler.remote.command.log.GetLogBytesRequestCommand;
import org.apache.dolphinscheduler.remote.command.log.GetLogBytesResponseCommand;
import org.apache.dolphinscheduler.remote.command.log.RemoveTaskLogRequestCommand;
import org.apache.dolphinscheduler.remote.command.log.RemoveTaskLogResponseCommand;
import org.apache.dolphinscheduler.remote.command.log.RollViewLogRequestCommand;
import org.apache.dolphinscheduler.remote.command.log.RollViewLogResponseCommand;
import org.apache.dolphinscheduler.remote.command.log.ViewLogRequestCommand;
import org.apache.dolphinscheduler.remote.command.log.ViewLogResponseCommand;
import org.apache.dolphinscheduler.remote.processor.NettyRequestProcessor;
import org.apache.dolphinscheduler.remote.utils.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
=======
package org.apache.dolphinscheduler.server.log;

import io.netty.channel.Channel;
import org.apache.dolphinscheduler.common.utils.IOUtils;
import org.apache.dolphinscheduler.remote.command.Command;
import org.apache.dolphinscheduler.remote.command.CommandType;
import org.apache.dolphinscheduler.remote.command.log.*;
import org.apache.dolphinscheduler.remote.processor.NettyRequestProcessor;
import org.apache.dolphinscheduler.remote.utils.FastJsonSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
<<<<<<< HEAD
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.Channel;

/**
 * logger request process logic
=======
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *  logger request process logic
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
 */
public class LoggerRequestProcessor implements NettyRequestProcessor {

    private final Logger logger = LoggerFactory.getLogger(LoggerRequestProcessor.class);

<<<<<<< HEAD
    private final ExecutorService executor;

    public LoggerRequestProcessor() {
        this.executor = Executors.newFixedThreadPool(Constants.CPUS * 2 + 1);
=======
    private final ThreadPoolExecutor executor;

    public LoggerRequestProcessor(){
        this.executor = new ThreadPoolExecutor(4, 4, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Override
    public void process(Channel channel, Command command) {
        logger.info("received command : {}", command);

        /**
         * reuqest task log command type
         */
        final CommandType commandType = command.getType();
<<<<<<< HEAD
        switch (commandType) {
            case GET_LOG_BYTES_REQUEST:
                GetLogBytesRequestCommand getLogRequest = JSONUtils.parseObject(
=======
        switch (commandType){
            case GET_LOG_BYTES_REQUEST:
                GetLogBytesRequestCommand getLogRequest = FastJsonSerializer.deserialize(
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                        command.getBody(), GetLogBytesRequestCommand.class);
                byte[] bytes = getFileContentBytes(getLogRequest.getPath());
                GetLogBytesResponseCommand getLogResponse = new GetLogBytesResponseCommand(bytes);
                channel.writeAndFlush(getLogResponse.convert2Command(command.getOpaque()));
                break;
            case VIEW_WHOLE_LOG_REQUEST:
<<<<<<< HEAD
                ViewLogRequestCommand viewLogRequest = JSONUtils.parseObject(
                        command.getBody(), ViewLogRequestCommand.class);
                String msg = LoggerUtils.readWholeFileContent(viewLogRequest.getPath());
=======
                ViewLogRequestCommand viewLogRequest = FastJsonSerializer.deserialize(
                        command.getBody(), ViewLogRequestCommand.class);
                String msg = readWholeFileContent(viewLogRequest.getPath());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                ViewLogResponseCommand viewLogResponse = new ViewLogResponseCommand(msg);
                channel.writeAndFlush(viewLogResponse.convert2Command(command.getOpaque()));
                break;
            case ROLL_VIEW_LOG_REQUEST:
<<<<<<< HEAD
                RollViewLogRequestCommand rollViewLogRequest = JSONUtils.parseObject(
=======
                RollViewLogRequestCommand rollViewLogRequest = FastJsonSerializer.deserialize(
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                        command.getBody(), RollViewLogRequestCommand.class);
                List<String> lines = readPartFileContent(rollViewLogRequest.getPath(),
                        rollViewLogRequest.getSkipLineNum(), rollViewLogRequest.getLimit());
                StringBuilder builder = new StringBuilder();
<<<<<<< HEAD
                for (String line : lines) {
=======
                for (String line : lines){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                    builder.append(line + "\r\n");
                }
                RollViewLogResponseCommand rollViewLogRequestResponse = new RollViewLogResponseCommand(builder.toString());
                channel.writeAndFlush(rollViewLogRequestResponse.convert2Command(command.getOpaque()));
                break;
            case REMOVE_TAK_LOG_REQUEST:
<<<<<<< HEAD
                RemoveTaskLogRequestCommand removeTaskLogRequest = JSONUtils.parseObject(
=======
                RemoveTaskLogRequestCommand removeTaskLogRequest = FastJsonSerializer.deserialize(
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                        command.getBody(), RemoveTaskLogRequestCommand.class);

                String taskLogPath = removeTaskLogRequest.getPath();

                File taskLogFile = new File(taskLogPath);
                Boolean status = true;
                try {
<<<<<<< HEAD
                    if (taskLogFile.exists()) {
                        status = taskLogFile.delete();
                    }
                } catch (Exception e) {
=======
                    if (taskLogFile.exists()){
                        taskLogFile.delete();
                    }
                }catch (Exception e){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                    status = false;
                }

                RemoveTaskLogResponseCommand removeTaskLogResponse = new RemoveTaskLogResponseCommand(status);
                channel.writeAndFlush(removeTaskLogResponse.convert2Command(command.getOpaque()));
                break;
            default:
                throw new IllegalArgumentException("unknown commandType");
        }
    }

<<<<<<< HEAD
    public ExecutorService getExecutor() {
=======
    public ExecutorService getExecutor(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return this.executor;
    }

    /**
     * get files content bytes，for down load file
     *
     * @param filePath file path
     * @return byte array of file
     * @throws Exception exception
     */
<<<<<<< HEAD
    private byte[] getFileContentBytes(String filePath) {
        try (InputStream in = new FileInputStream(filePath);
             ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
=======
    private byte[] getFileContentBytes(String filePath){
        InputStream in = null;
        ByteArrayOutputStream bos = null;
        try {
            in = new FileInputStream(filePath);
            bos  = new ByteArrayOutputStream();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) != -1) {
                bos.write(buf, 0, len);
            }
            return bos.toByteArray();
<<<<<<< HEAD
        } catch (IOException e) {
            logger.error("get file bytes error", e);
=======
        }catch (IOException e){
            logger.error("get file bytes error",e);
        }finally {
            IOUtils.closeQuietly(bos);
            IOUtils.closeQuietly(in);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
        return new byte[0];
    }

    /**
     * read part file content，can skip any line and read some lines
     *
     * @param filePath file path
     * @param skipLine skip line
     * @param limit read lines limit
     * @return part file content
     */
    private List<String> readPartFileContent(String filePath,
<<<<<<< HEAD
                                             int skipLine,
                                             int limit) {
=======
                                            int skipLine,
                                            int limit){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        File file = new File(filePath);
        if (file.exists() && file.isFile()) {
            try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
                return stream.skip(skipLine).limit(limit).collect(Collectors.toList());
            } catch (IOException e) {
<<<<<<< HEAD
                logger.error("read file error", e);
=======
                logger.error("read file error",e);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            }
        } else {
            logger.info("file path: {} not exists", filePath);
        }
        return Collections.emptyList();
    }

<<<<<<< HEAD
=======
    /**
     * read whole file content
     *
     * @param filePath file path
     * @return whole file content
     */
    private String readWholeFileContent(String filePath){
        BufferedReader br = null;
        String line;
        StringBuilder sb = new StringBuilder();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
            while ((line = br.readLine()) != null){
                sb.append(line + "\r\n");
            }
            return sb.toString();
        }catch (IOException e){
            logger.error("read file error",e);
        }finally {
            IOUtils.closeQuietly(br);
        }
        return "";
    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
