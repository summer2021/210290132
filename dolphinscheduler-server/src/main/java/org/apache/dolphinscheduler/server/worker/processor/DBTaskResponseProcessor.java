/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.server.worker.processor;

import io.netty.channel.Channel;
import org.apache.dolphinscheduler.common.enums.ExecutionStatus;
<<<<<<< HEAD
import org.apache.dolphinscheduler.common.utils.JSONUtils;
=======
import org.apache.dolphinscheduler.common.utils.Preconditions;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.remote.command.Command;
import org.apache.dolphinscheduler.remote.command.CommandType;
import org.apache.dolphinscheduler.remote.command.DBTaskResponseCommand;
import org.apache.dolphinscheduler.remote.processor.NettyRequestProcessor;
<<<<<<< HEAD
=======
import org.apache.dolphinscheduler.remote.utils.FastJsonSerializer;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.server.worker.cache.ResponceCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
import com.google.common.base.Preconditions;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
/**
 *  db task response processor
 */
public class DBTaskResponseProcessor implements NettyRequestProcessor {

    private final Logger logger = LoggerFactory.getLogger(DBTaskResponseProcessor.class);

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Override
    public void process(Channel channel, Command command) {
        Preconditions.checkArgument(CommandType.DB_TASK_RESPONSE == command.getType(),
                String.format("invalid command type : %s", command.getType()));

<<<<<<< HEAD
        DBTaskResponseCommand taskResponseCommand = JSONUtils.parseObject(
=======
        DBTaskResponseCommand taskResponseCommand = FastJsonSerializer.deserialize(
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                command.getBody(), DBTaskResponseCommand.class);

        if (taskResponseCommand == null){
            return;
        }

        if (taskResponseCommand.getStatus() == ExecutionStatus.SUCCESS.getCode()){
            ResponceCache.get().removeResponseCache(taskResponseCommand.getTaskInstanceId());
        }
    }


}
