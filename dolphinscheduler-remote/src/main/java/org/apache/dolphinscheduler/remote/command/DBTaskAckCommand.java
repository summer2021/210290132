/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.remote.command;

import org.apache.dolphinscheduler.common.utils.JSONUtils;
=======
package org.apache.dolphinscheduler.remote.command;

import org.apache.dolphinscheduler.remote.utils.FastJsonSerializer;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

import java.io.Serializable;

/**
<<<<<<< HEAD
 * db task ack request command
=======
 *  db task ack request command
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
 */
public class DBTaskAckCommand implements Serializable {

    private int taskInstanceId;
    private int status;

<<<<<<< HEAD
    public DBTaskAckCommand() {
        super();
    }

    public DBTaskAckCommand(int status, int taskInstanceId) {
=======
    public DBTaskAckCommand(int status,int taskInstanceId) {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        this.status = status;
        this.taskInstanceId = taskInstanceId;
    }

    public int getTaskInstanceId() {
        return taskInstanceId;
    }

    public void setTaskInstanceId(int taskInstanceId) {
        this.taskInstanceId = taskInstanceId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * package response command
<<<<<<< HEAD
     *
     * @return command
     */
    public Command convert2Command() {
        Command command = new Command();
        command.setType(CommandType.DB_TASK_ACK);
        byte[] body = JSONUtils.toJsonByteArray(this);
=======
     * @return command
     */
    public Command convert2Command(){
        Command command = new Command();
        command.setType(CommandType.DB_TASK_ACK);
        byte[] body = FastJsonSerializer.serialize(this);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        command.setBody(body);
        return command;
    }

<<<<<<< HEAD
    @Override
    public String toString() {
        return "DBTaskAckCommand{" + "taskInstanceId=" + taskInstanceId + ", status=" + status + '}';
=======

    @Override
    public String toString() {
        return "DBTaskAckCommand{" +
                "taskInstanceId=" + taskInstanceId +
                ", status=" + status +
                '}';
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }
}
