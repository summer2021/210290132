/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
package org.apache.dolphinscheduler.remote.utils;

import org.apache.dolphinscheduler.common.utils.NetUtils;

import java.net.InetSocketAddress;

import io.netty.channel.Channel;

/**
<<<<<<< HEAD
 * channel utils
 */
public class ChannelUtils {

    private ChannelUtils() {
        throw new IllegalStateException(ChannelUtils.class.getName());
    }

    /**
     * get local address
=======
 *  channel utils
 */
public class ChannelUtils {

    /**
     *  get local address
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     *
     * @param channel channel
     * @return local address
     */
<<<<<<< HEAD
    public static String getLocalAddress(Channel channel) {
        return NetUtils.getHost(((InetSocketAddress) channel.localAddress()).getAddress());
    }

    /**
     * get remote address
     *
     * @param channel channel
     * @return remote address
     */
    public static String getRemoteAddress(Channel channel) {
        return NetUtils.getHost(((InetSocketAddress) channel.remoteAddress()).getAddress());
    }

    /**
     * channel to address
     *
     * @param channel channel
     * @return address
     */
    public static Host toAddress(Channel channel) {
        InetSocketAddress socketAddress = ((InetSocketAddress) channel.remoteAddress());
=======
    public static String getLocalAddress(Channel channel){
        return NetUtils.getHost(((InetSocketAddress)channel.localAddress()).getAddress());
    }

    /**
     *  get remote address
     * @param channel channel
     * @return remote address
     */
    public static String getRemoteAddress(Channel channel){
        return NetUtils.getHost(((InetSocketAddress)channel.remoteAddress()).getAddress());
    }

    /**
     *  channel to address
     * @param channel channel
     * @return address
     */
    public static Host toAddress(Channel channel){
        InetSocketAddress socketAddress = ((InetSocketAddress)channel.remoteAddress());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return new Host(NetUtils.getHost(socketAddress.getAddress()), socketAddress.getPort());
    }

}
