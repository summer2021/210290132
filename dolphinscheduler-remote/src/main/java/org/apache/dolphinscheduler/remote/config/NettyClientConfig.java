/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
package org.apache.dolphinscheduler.remote.config;

import org.apache.dolphinscheduler.remote.utils.Constants;

/**
<<<<<<< HEAD
 * netty client config
=======
 *  netty client config
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
 */
public class NettyClientConfig {

    /**
<<<<<<< HEAD
     * worker threads，default get machine cpus
=======
     *  worker threads，default get machine cpus
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     */
    private int workerThreads = Constants.CPUS;

    /**
<<<<<<< HEAD
     * whether tpc delay
=======
     *  whether tpc delay
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     */
    private boolean tcpNoDelay = true;

    /**
     * whether keep alive
     */
    private boolean soKeepalive = true;

    /**
<<<<<<< HEAD
     * send buffer size
=======
     *  send buffer size
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     */
    private int sendBufferSize = 65535;

    /**
<<<<<<< HEAD
     * receive buffer size
=======
     *  receive buffer size
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     */
    private int receiveBufferSize = 65535;

    /**
     * connect timeout millis
     */
    private int connectTimeoutMillis = 3000;

    public int getWorkerThreads() {
        return workerThreads;
    }

    public void setWorkerThreads(int workerThreads) {
        this.workerThreads = workerThreads;
    }

    public boolean isTcpNoDelay() {
        return tcpNoDelay;
    }

    public void setTcpNoDelay(boolean tcpNoDelay) {
        this.tcpNoDelay = tcpNoDelay;
    }

    public boolean isSoKeepalive() {
        return soKeepalive;
    }

    public void setSoKeepalive(boolean soKeepalive) {
        this.soKeepalive = soKeepalive;
    }

    public int getSendBufferSize() {
        return sendBufferSize;
    }

    public void setSendBufferSize(int sendBufferSize) {
        this.sendBufferSize = sendBufferSize;
    }

    public int getReceiveBufferSize() {
        return receiveBufferSize;
    }

    public void setReceiveBufferSize(int receiveBufferSize) {
        this.receiveBufferSize = receiveBufferSize;
    }

    public int getConnectTimeoutMillis() {
        return connectTimeoutMillis;
    }

    public void setConnectTimeoutMillis(int connectTimeoutMillis) {
        this.connectTimeoutMillis = connectTimeoutMillis;
    }
}
