/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import i18n from '@/module/i18n'
import config from '~/external/config'
import Permissions from '@/module/permissions'

const menu = {
  projects: [
    {
      name: `${i18n.$t('Project Home')}`,
      id: 0,
      path: 'projects-index',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'ri-home-4-line',
      children: []
    },
    {
      name: `${i18n.$t('Kinship')}`,
      id: 1,
      path: 'projects-kinship',
      isOpen: true,
      enabled: true,
      icon: 'ri-node-tree',
=======
      disabled: true,
      icon: 'ans-icon-home-solid',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: []
    },
    {
      name: `${i18n.$t('Process')}`,
<<<<<<< HEAD
      id: 2,
      path: '',
      isOpen: true,
      enabled: true,
      icon: 'el-icon-s-tools',
=======
      id: 1,
      path: '',
      isOpen: true,
      disabled: true,
      icon: 'ans-icon-gear',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: [
        {
          name: `${i18n.$t('Process definition')}`,
          path: 'definition',
          id: 0,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: `${i18n.$t('Process Instance')}`,
          path: 'instance',
          id: 1,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: `${i18n.$t('Task Instance')}`,
          path: 'task-instance',
          id: 2,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: `${i18n.$t('Task record')}`,
          path: 'task-record',
          id: 3,
<<<<<<< HEAD
          enabled: config.recordSwitch
=======
          disabled: config.recordSwitch
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: `${i18n.$t('History task record')}`,
          path: 'history-task-record',
          id: 4,
<<<<<<< HEAD
          enabled: config.recordSwitch
=======
          disabled: config.recordSwitch
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
      ]
    }
  ],

  security: [
    {
      name: `${i18n.$t('Tenant Manage')}`,
      id: 0,
      path: 'tenement-manage',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-user-solid',
=======
      disabled: true,
      icon: 'ans-icon-user-solid',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: []
    },
    {
      name: `${i18n.$t('User Manage')}`,
      id: 1,
      path: 'users-manage',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-user-solid',
=======
      disabled: true,
      icon: 'ans-icon-user-circle-solid',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: []
    },
    {
      name: `${i18n.$t('Warning group manage')}`,
      id: 2,
      path: 'warning-groups-manage',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-warning',
      children: []
    },
    {
      name: `${i18n.$t('Warning instance manage')}`,
      id: 2,
      path: 'warning-instance-manage',
      isOpen: true,
      enabled: true,
      icon: 'el-icon-warning-outline',
=======
      disabled: true,
      icon: 'ans-icon-danger-solid',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: []
    },
    {
      name: `${i18n.$t('Worker group manage')}`,
      id: 4,
      path: 'worker-groups-manage',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-s-custom',
=======
      disabled: true,
      icon: 'ans-icon-diary',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: []
    },
    {
      name: `${i18n.$t('Queue manage')}`,
      id: 3,
      path: 'queue-manage',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-s-grid',
      children: []
    },
    {
      name: `${i18n.$t('Environment manage')}`,
      id: 3,
      path: 'environment-manage',
      isOpen: true,
      enabled: true,
      icon: 'el-icon-setting',
=======
      disabled: true,
      icon: 'ans-icon-recycle',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: []
    },
    {
      name: `${i18n.$t('Token manage')}`,
      id: 2,
      path: 'token-manage',
      isOpen: true,
<<<<<<< HEAD
      icon: 'el-icon-document',
      children: [],
      enabled: true
=======
      icon: 'ans-icon-document',
      children: [],
      disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }
  ],
  resource: [
    {
      name: `${i18n.$t('File Manage')}`,
      id: 0,
      path: 'file',
      isOpen: true,
<<<<<<< HEAD
      icon: 'el-icon-document-copy',
      children: [],
      enabled: true
=======
      icon: 'ans-icon-documents',
      children: [],
      disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    },
    {
      name: `${i18n.$t('UDF manage')}`,
      id: 1,
      path: '',
      isOpen: true,
<<<<<<< HEAD
      icon: 'el-icon-document',
      enabled: true,
=======
      icon: 'ans-icon-document',
      disabled: true,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: [
        {
          name: `${i18n.$t('Resource manage')}`,
          path: 'resource-udf',
          id: 0,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: `${i18n.$t('Function manage')}`,
          path: 'resource-func',
          id: 1,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
      ]
    }
  ],
  user: [
    {
      name: `${i18n.$t('User Information')}`,
      id: 0,
      path: 'account',
      isOpen: true,
<<<<<<< HEAD
      icon: 'el-icon-user-solid',
      children: [],
      enabled: true
=======
      icon: 'ans-icon-user-solid',
      children: [],
      disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    },
    {
      name: `${i18n.$t('Edit password')}`,
      id: 1,
      path: 'password',
      isOpen: true,
<<<<<<< HEAD
      icon: 'el-icon-key',
      children: [],
      enabled: true
=======
      icon: 'ans-icon-key',
      children: [],
      disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    },
    {
      name: `${i18n.$t('Token manage')}`,
      id: 2,
      path: 'token',
      isOpen: true,
<<<<<<< HEAD
      icon: 'el-icon-s-custom',
      children: [],
      enabled: Permissions.getAuth()
=======
      icon: 'ans-icon-diary',
      children: [],
      disabled: Permissions.getAuth()
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }
  ],
  monitor: [
    {
      name: `${i18n.$t('Servers manage')}`,
      id: 1,
      path: '',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-menu',
=======
      disabled: true,
      icon: 'ans-icon-menu',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: [
        {
          name: 'Master',
          path: 'servers-master',
          id: 0,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: 'Worker',
          path: 'servers-worker',
          id: 1,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: 'Zookeeper',
          path: 'servers-zookeeper',
          id: 4,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        },
        {
          name: 'DB',
          path: 'servers-db',
          id: 6,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
      ]
    },
    {
      name: `${i18n.$t('Statistics manage')}`,
      id: 0,
      path: '',
      isOpen: true,
<<<<<<< HEAD
      enabled: true,
      icon: 'el-icon-menu',
=======
      disabled: true,
      icon: 'ans-icon-menu',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      children: [
        {
          name: 'Statistics',
          path: 'statistics',
          id: 0,
<<<<<<< HEAD
          enabled: true
=======
          disabled: true
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
      ]
    }
  ]
}

export default type => menu[type]
