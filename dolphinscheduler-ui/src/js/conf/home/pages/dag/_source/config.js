/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import i18n from '@/module/i18n'

/**
 * Operation bar config
 * @code code
 * @icon icon
 * @disable disable
 * @desc tooltip
 */
const toolOper = (dagThis) => {
  const disabled = !!dagThis.$store.state.dag.isDetails// Permissions.getAuth() === false ? false : !dagThis.$store.state.dag.isDetails
  return [
    {
      code: 'pointer',
<<<<<<< HEAD
      icon: 'el-icon-thumb',
=======
      icon: 'ans-icon-pointer',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      disable: disabled,
      desc: `${i18n.$t('Drag Nodes and Selected Items')}`
    },
    {
      code: 'line',
<<<<<<< HEAD
      icon: 'el-icon-top-right',
=======
      icon: 'ans-icon-slash',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      disable: disabled,
      desc: `${i18n.$t('Select Line Connection')}`
    },
    {
      code: 'remove',
<<<<<<< HEAD
      icon: 'el-icon-delete',
=======
      icon: 'ans-icon-trash',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      disable: disabled,
      desc: `${i18n.$t('Delete selected lines or nodes')}`
    },
    {
      code: 'download',
<<<<<<< HEAD
      icon: 'el-icon-download',
=======
      icon: 'ans-icon-download',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      disable: !dagThis.type,
      desc: `${i18n.$t('Download')}`
    },
    {
      code: 'screen',
<<<<<<< HEAD
      icon: 'el-icon-full-screen',
=======
      icon: 'ans-icon-max',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      disable: false,
      desc: `${i18n.$t('Full Screen')}`
    }
  ]
}

/**
 * Post status
 * @id Front end definition id
 * @desc tooltip
 * @code Backend definition identifier
 */
const publishStatus = [
  {
    id: 0,
    desc: `${i18n.$t('Unpublished')}`,
    code: 'NOT_RELEASE'
  },
  {
    id: 1,
    desc: `${i18n.$t('online')}`,
    code: 'ONLINE'
  },
  {
    id: 2,
    desc: `${i18n.$t('offline')}`,
    code: 'OFFLINE'
  }
]

/**
 * Operation type
 * @desc tooltip
 * @code identifier
 */
const runningType = [
  {
    desc: `${i18n.$t('Start Process')}`,
    code: 'START_PROCESS'
  },
  {
    desc: `${i18n.$t('Execute from the current node')}`,
    code: 'START_CURRENT_TASK_PROCESS'
  },
  {
    desc: `${i18n.$t('Recover tolerance fault process')}`,
    code: 'RECOVER_TOLERANCE_FAULT_PROCESS'
  },
  {
    desc: `${i18n.$t('Resume the suspension process')}`,
    code: 'RECOVER_SUSPENDED_PROCESS'
  },
  {
    desc: `${i18n.$t('Execute from the failed nodes')}`,
    code: 'START_FAILURE_TASK_PROCESS'
  },
  {
    desc: `${i18n.$t('Complement Data')}`,
    code: 'COMPLEMENT_DATA'
  },
  {
    desc: `${i18n.$t('Scheduling execution')}`,
    code: 'SCHEDULER'
  },
  {
    desc: `${i18n.$t('Rerun')}`,
    code: 'REPEAT_RUNNING'
  },
  {
    desc: `${i18n.$t('Pause')}`,
    code: 'PAUSE'
  },
  {
    desc: `${i18n.$t('Stop')}`,
    code: 'STOP'
  },
  {
    desc: `${i18n.$t('Recovery waiting thread')}`,
<<<<<<< HEAD
    code: 'RECOVER_WAITING_THREAD'
=======
    code: 'RECOVER_WAITTING_THREAD'
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
  }
]

/**
 * Task status
 * @key key
 * @id id
 * @desc tooltip
 * @color color
 * @icoUnicode iconfont
 * @isSpin is loading (Need to execute the code block to write if judgment)
 */
const tasksState = {
  SUBMITTED_SUCCESS: {
    id: 0,
    desc: `${i18n.$t('Submitted successfully')}`,
    color: '#A9A9A9',
<<<<<<< HEAD
    icoUnicode: 'ri-record-circle-fill',
=======
    icoUnicode: 'ans-icon-dot-circle',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  RUNNING_EXECUTION: {
    id: 1,
    desc: `${i18n.$t('Executing')}`,
    color: '#0097e0',
<<<<<<< HEAD
    icoUnicode: 'el-icon-s-tools',
=======
    icoUnicode: 'ans-icon-gear',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: true
  },
  READY_PAUSE: {
    id: 2,
    desc: `${i18n.$t('Ready to pause')}`,
    color: '#07b1a3',
<<<<<<< HEAD
    icoUnicode: 'ri-settings-3-line',
=======
    icoUnicode: 'ans-icon-pause-solid',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  PAUSE: {
    id: 3,
    desc: `${i18n.$t('Pause')}`,
    color: '#057c72',
<<<<<<< HEAD
    icoUnicode: 'el-icon-video-pause',
=======
    icoUnicode: 'ans-icon-pause',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  READY_STOP: {
    id: 4,
    desc: `${i18n.$t('Ready to stop')}`,
    color: '#FE0402',
<<<<<<< HEAD
    icoUnicode: 'ri-stop-circle-fill',
=======
    icoUnicode: 'ans-icon-coin',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  STOP: {
    id: 5,
    desc: `${i18n.$t('Stop')}`,
    color: '#e90101',
<<<<<<< HEAD
    icoUnicode: 'ri-stop-circle-line',
=======
    icoUnicode: 'ans-icon-stop',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  FAILURE: {
    id: 6,
<<<<<<< HEAD
    desc: `${i18n.$t('Failed')}`,
    color: '#000000',
    icoUnicode: 'el-icon-circle-close',
=======
    desc: `${i18n.$t('failed')}`,
    color: '#000000',
    icoUnicode: 'ans-icon-fail-empty',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  SUCCESS: {
    id: 7,
<<<<<<< HEAD
    desc: `${i18n.$t('Success')}`,
    color: '#33cc00',
    icoUnicode: 'el-icon-circle-check',
=======
    desc: `${i18n.$t('success')}`,
    color: '#33cc00',
    icoUnicode: 'ans-icon-success-empty',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  NEED_FAULT_TOLERANCE: {
    id: 8,
    desc: `${i18n.$t('Need fault tolerance')}`,
    color: '#FF8C00',
<<<<<<< HEAD
    icoUnicode: 'el-icon-edit',
=======
    icoUnicode: 'ans-icon-pen',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  },
  KILL: {
    id: 9,
<<<<<<< HEAD
    desc: `${i18n.$t('Kill')}`,
    color: '#a70202',
    icoUnicode: 'el-icon-remove-outline',
    isSpin: false
  },
  WAITING_THREAD: {
    id: 10,
    desc: `${i18n.$t('Waiting for thread')}`,
    color: '#912eed',
    icoUnicode: 'ri-time-line',
    isSpin: false
  },
  WAITING_DEPEND: {
    id: 11,
    desc: `${i18n.$t('Waiting for dependence')}`,
    color: '#5101be',
    icoUnicode: 'ri-send-to-back',
    isSpin: false
  },
  DELAY_EXECUTION: {
    id: 12,
    desc: `${i18n.$t('Delay execution')}`,
    color: '#5102ce',
    icoUnicode: 'ri-pause-circle-fill',
    isSpin: false
  },
  FORCED_SUCCESS: {
    id: 13,
    desc: `${i18n.$t('Forced success')}`,
    color: '#5102ce',
    icoUnicode: 'el-icon-success',
=======
    desc: `${i18n.$t('kill')}`,
    color: '#a70202',
    icoUnicode: 'ans-icon-minus-circle-empty',
    isSpin: false
  },
  WAITTING_THREAD: {
    id: 10,
    desc: `${i18n.$t('Waiting for thread')}`,
    color: '#912eed',
    icoUnicode: 'ans-icon-sand-clock',
    isSpin: false
  },
  WAITTING_DEPEND: {
    id: 11,
    desc: `${i18n.$t('Waiting for dependence')}`,
    color: '#5101be',
    icoUnicode: 'ans-icon-dependence',
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    isSpin: false
  }
}

/**
 * Node type
 * @key key
 * @desc tooltip
 * @color color (tree and gantt)
 */
const tasksType = {
  SHELL: {
    desc: 'SHELL',
    color: '#646464'
  },
<<<<<<< HEAD
  WATERDROP: {
    desc: 'WATERDROP',
    color: '#646465'
  },
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
  SUB_PROCESS: {
    desc: 'SUB_PROCESS',
    color: '#0097e0'
  },
  PROCEDURE: {
    desc: 'PROCEDURE',
    color: '#525CCD'
  },
  SQL: {
    desc: 'SQL',
    color: '#7A98A1'
  },
  SPARK: {
    desc: 'SPARK',
    color: '#E46F13'
  },
  FLINK: {
    desc: 'FLINK',
    color: '#E46F13'
  },
  MR: {
    desc: 'MapReduce',
    color: '#A0A5CC'
  },
  PYTHON: {
    desc: 'PYTHON',
    color: '#FED52D'
  },
  DEPENDENT: {
    desc: 'DEPENDENT',
    color: '#2FBFD8'
  },
  HTTP: {
    desc: 'HTTP',
    color: '#E46F13'
  },
  DATAX: {
    desc: 'DataX',
    color: '#1fc747'
  },
<<<<<<< HEAD
  TIS: {
    desc: 'TIS',
    color: '#1fc747'
  },
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
  SQOOP: {
    desc: 'SQOOP',
    color: '#E46F13'
  },
  CONDITIONS: {
    desc: 'CONDITIONS',
    color: '#E46F13'
<<<<<<< HEAD
  },
  SWITCH: {
    desc: 'SWITCH',
    color: '#E46F13'
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
  }
}

export {
  toolOper,
  publishStatus,
  runningType,
  tasksState,
  tasksType
}
