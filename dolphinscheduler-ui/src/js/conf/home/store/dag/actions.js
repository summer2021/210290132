/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import _ from 'lodash'
import io from '@/module/io'
<<<<<<< HEAD
=======
import { tasksState } from '@/conf/home/pages/dag/_source/config'

// delete 'definitionList' from tasks
const deleteDefinitionList = (tasks) => {
  const newTasks = [];
  tasks.forEach(item => {
    const newItem = Object.assign({}, item);
    if(newItem.dependence && newItem.dependence.dependTaskList) {
      newItem.dependence.dependTaskList.forEach(dependTaskItem => {
        if (dependTaskItem.dependItemList) {
          dependTaskItem.dependItemList.forEach(dependItem => {
            Reflect.deleteProperty(dependItem, 'definitionList');
          })
        }
      })
    }
    newTasks.push(newItem);
  });
  return newTasks;
}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

export default {
  /**
   *  Task status acquisition
   */
  getTaskState ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-instances/${payload}/tasks`, {
        processInstanceId: payload
      }, res => {
        state.taskInstances = res.data.taskList
        resolve(res)
=======
      io.get(`projects/${state.projectName}/instance/task-list-by-process-id`, {
        processInstanceId: payload
      }, res => {
        const arr = _.map(res.data.taskList, v => {
          return _.cloneDeep(_.assign(tasksState[v.state], {
            name: v.name,
            stateId: v.id,
            dependentResult: v.dependentResult
          }))
        })
        resolve({
          list: arr,
          processInstanceState: res.data.processInstanceState,
          taskList: res.data.taskList
        })
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Update process definition status
   */
  editProcessState ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/process-definition/${payload.code}/release`, {
        name: payload.name,
=======
      io.post(`projects/${state.projectName}/process/release`, {
        processId: payload.processId,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        releaseState: payload.releaseState
      }, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
<<<<<<< HEAD

  /**
   * get process definition versions pagination info
   */
  getProcessDefinitionVersionsPage ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectCode}/process-definition/${payload.code}/versions`, payload, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

  /**
   * switch process definition version
   */
  switchProcessDefinitionVersion ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectCode}/process-definition/${payload.code}/versions/${payload.version}`, {}, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

  /**
   * delete process definition version
   */
  deleteProcessDefinitionVersion ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.delete(`projects/${state.projectCode}/process-definition/${payload.code}/versions/${payload.version}`, {}, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
  /**
   * Update process instance status
   */
  editExecutorsState ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/executors/execute`, {
=======
      io.post(`projects/${state.projectName}/executors/execute`, {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        processInstanceId: payload.processInstanceId,
        executeType: payload.executeType
      }, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Verify that the DGA map name exists
   */
  verifDAGName ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-definition/verify-name`, {
=======
      io.get(`projects/${state.projectName}/process/verify-name`, {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        name: payload
      }, res => {
        state.name = payload
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

  /**
   * Get process definition DAG diagram details
   */
  getProcessDetails ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-definition/${payload}`, {
      }, res => {
        // process definition code
        state.code = res.data.processDefinition.code
        // version
        state.version = res.data.processDefinition.version
        // name
        state.name = res.data.processDefinition.name
        // description
        state.description = res.data.processDefinition.description
        // taskRelationJson
        state.connects = res.data.processTaskRelationList
        // locations
        state.locations = JSON.parse(res.data.processDefinition.locations)
        // global params
        state.globalParams = res.data.processDefinition.globalParamList
        // timeout
        state.timeout = res.data.processDefinition.timeout
        // tenantCode
        state.tenantCode = res.data.processDefinition.tenantCode || 'default'
        // tasks info
        state.tasks = res.data.taskDefinitionList.map(task => _.pick(task, [
          'code',
          'name',
          'version',
          'description',
          'delayTime',
          'taskType',
          'taskParams',
          'flag',
          'taskPriority',
          'workerGroup',
          'failRetryTimes',
          'failRetryInterval',
          'timeoutFlag',
          'timeoutNotifyStrategy',
          'timeout',
          'environmentCode'
        ]))
=======
      io.get(`projects/${state.projectName}/process/select-by-id`, {
        processId: payload
      }, res => {
        // name
        state.name = res.data.name
        // description
        state.description = res.data.description
        // connects
        state.connects = JSON.parse(res.data.connects)
        // locations
        state.locations = JSON.parse(res.data.locations)
        // Process definition
        const processDefinitionJson = JSON.parse(res.data.processDefinitionJson)
        // tasks info
        state.tasks = processDefinitionJson.tasks
        // tasks cache
        state.cacheTasks = {}
        processDefinitionJson.tasks.forEach(v => {
          state.cacheTasks[v.id] = v
        })
        // global params
        state.globalParams = processDefinitionJson.globalParams
        // timeout
        state.timeout = processDefinitionJson.timeout

        state.tenantId = processDefinitionJson.tenantId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },

  /**
   * Get process definition DAG diagram details
   */
  copyProcess ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/process-definition/batch-copy`, {
        codes: payload.codes,
        targetProjectCode: payload.targetProjectCode
=======
      io.post(`projects/${state.projectName}/process/copy`, {
        processId: payload.processId
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      }, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

  /**
<<<<<<< HEAD
   * Get process definition DAG diagram details
   */
  moveProcess ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.post(`projects/${state.projectCode}/process-definition/batch-move`, {
        codes: payload.codes,
        targetProjectCode: payload.targetProjectCode
      }, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

  /**
   * Get all the items created by the logged in user
   */
  getAllItems ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('projects/created-and-authed', {}, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },

  /**
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
   * Get the process instance DAG diagram details
   */
  getInstancedetail ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-instances/${payload}`, {
      }, res => {
        const { processDefinition, processTaskRelationList, taskDefinitionList } = res.data.dagData
        // code
        state.code = processDefinition.code
        // version
        state.version = processDefinition.version
        // name
        state.name = res.data.name
        // desc
        state.description = processDefinition.description
        // connects
        state.connects = processTaskRelationList
        // locations
        state.locations = JSON.parse(processDefinition.locations)
        // global params
        state.globalParams = processDefinition.globalParamList
        // timeout
        state.timeout = processDefinition.timeout
        // tenantCode
        state.tenantCode = res.data.tenantCode || 'default'
        // tasks info
        state.tasks = taskDefinitionList.map(task => _.pick(task, [
          'code',
          'name',
          'version',
          'description',
          'delayTime',
          'taskType',
          'taskParams',
          'flag',
          'taskPriority',
          'workerGroup',
          'failRetryTimes',
          'failRetryInterval',
          'timeoutFlag',
          'timeoutNotifyStrategy',
          'timeout',
          'environmentCode'
        ]))
=======
      io.get(`projects/${state.projectName}/instance/select-by-id`, {
        processInstanceId: payload
      }, res => {
        // name
        state.name = res.data.name
        // desc
        state.description = res.data.description
        // connects
        state.connects = JSON.parse(res.data.connects)
        // locations
        state.locations = JSON.parse(res.data.locations)
        // process instance
        const processInstanceJson = JSON.parse(res.data.processInstanceJson)
        // tasks info
        state.tasks = processInstanceJson.tasks
        // tasks cache
        state.cacheTasks = {}
        processInstanceJson.tasks.forEach(v => {
          state.cacheTasks[v.id] = v
        })
        // global params
        state.globalParams = processInstanceJson.globalParams
        // timeout
        state.timeout = processInstanceJson.timeout

        state.tenantId = processInstanceJson.tenantId

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        // startup parameters
        state.startup = _.assign(state.startup, _.pick(res.data, ['commandType', 'failureStrategy', 'processInstancePriority', 'workerGroup', 'warningType', 'warningGroupId', 'receivers', 'receiversCc']))
        state.startup.commandParam = JSON.parse(res.data.commandParam)

        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * Create process definition
   */
  saveDAGchart ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/process-definition`, {
        locations: JSON.stringify(state.locations),
        name: _.trim(state.name),
        taskDefinitionJson: JSON.stringify(state.tasks),
        taskRelationJson: JSON.stringify(state.connects),
        tenantCode: state.tenantCode,
        description: _.trim(state.description),
        globalParams: JSON.stringify(state.globalParams),
        timeout: state.timeout
=======
      const data = {
        globalParams: state.globalParams,
        tasks: deleteDefinitionList(state.tasks),
        tenantId: state.tenantId,
        timeout: state.timeout
      }
      io.post(`projects/${state.projectName}/process/save`, {
        processDefinitionJson: JSON.stringify(data),
        name: _.trim(state.name),
        description: _.trim(state.description),
        locations: JSON.stringify(state.locations),
        connects: JSON.stringify(state.connects)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      }, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Process definition update
   */
  updateDefinition ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.put(`projects/${state.projectCode}/process-definition/${payload}`, {
        locations: JSON.stringify(state.locations),
        name: _.trim(state.name),
        taskDefinitionJson: JSON.stringify(state.tasks),
        taskRelationJson: JSON.stringify(state.connects),
        tenantCode: state.tenantCode,
        description: _.trim(state.description),
        globalParams: JSON.stringify(state.globalParams),
        timeout: state.timeout,
        releaseState: state.releaseState
=======
      const data = {
        globalParams: state.globalParams,
        tasks: deleteDefinitionList(state.tasks),
        tenantId: state.tenantId,
        timeout: state.timeout
      }
      io.post(`projects/${state.projectName}/process/update`, {
        processDefinitionJson: JSON.stringify(data),
        locations: JSON.stringify(state.locations),
        connects: JSON.stringify(state.connects),
        name: _.trim(state.name),
        description: _.trim(state.description),
        id: payload
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      }, res => {
        resolve(res)
        state.isEditDag = false
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Process instance update
   */
  updateInstance ({ state }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        globalParams: state.globalParams,
        tasks: state.tasks,
        tenantId: state.tenantId,
        timeout: state.timeout
      }
<<<<<<< HEAD
      io.put(`projects/${state.projectCode}/process-instances/${payload}`, {
        processInstanceJson: JSON.stringify(data),
        locations: JSON.stringify(state.locations),
        connects: JSON.stringify(state.connects),
=======
      io.post(`projects/${state.projectName}/instance/update`, {
        processInstanceJson: JSON.stringify(data),
        locations: JSON.stringify(state.locations),
        connects: JSON.stringify(state.connects),
        processInstanceId: payload,
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        syncDefine: state.syncDefine
      }, res => {
        resolve(res)
        state.isEditDag = false
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Get a list of process definitions (sub-workflow usage is not paged)
   */
  getProcessList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      if (state.processListS.length) {
        resolve()
        return
      }
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-definition/list`, payload, res => {
=======
      io.get(`projects/${state.projectName}/process/list`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        state.processListS = res.data
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * Get a list of process definitions (list page usage with pagination)
   */
  getProcessListP ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-definition`, payload, res => {
=======
      io.get(`projects/${state.projectName}/process/list-paging`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * Get a list of project
   */
  getProjectList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      if (state.projectListS.length) {
        resolve()
        return
      }
<<<<<<< HEAD
      io.get('projects/created-and-authed', payload, res => {
=======
      io.get('projects/query-project-list', payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        state.projectListS = res.data
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
<<<<<<< HEAD
   * Get a list of process definitions by project code
   */
  getProcessByProjectCode ({ state }, code) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${code}/process-definition/all`, res => {
=======
   * Get a list of process definitions by project id
   */
  getProcessByProjectId ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectName}/process/queryProcessDefinitionAllByProjectId`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * get datasource
   */
  getDatasourceList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('datasources/list', {
        type: payload
      }, res => {
        resolve(res)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * get resources
   */
  getResourcesList ({ state }) {
    return new Promise((resolve, reject) => {
      if (state.resourcesListS.length) {
        resolve()
        return
      }
      io.get('resources/list', {
        type: 'FILE'
      }, res => {
        state.resourcesListS = res.data
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * get jar
   */
<<<<<<< HEAD
  getResourcesListJar ({ state }) {
=======
  getResourcesListJar ({ state }, payload) {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    return new Promise((resolve, reject) => {
      if (state.resourcesListJar.length) {
        resolve()
        return
      }
<<<<<<< HEAD
      io.get('resources/query-by-type', {
        type: 'FILE'
      }, res => {
        state.resourcesListJar = res.data
=======
      io.get('resources/list/jar', {
        type: 'FILE',
        programType: payload
      }, res => {
        if(payload) {
          state.resourcesListPy = res.data
        } else {
          state.resourcesListJar = res.data
        }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * Get process instance
   */
  getProcessInstance ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-instances`, payload, res => {
=======
      io.get(`projects/${state.projectName}/instance/list-paging`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        state.instanceListS = res.data.totalList
        resolve(res.data)
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * Get alarm list
   */
  getNotifyGroupList ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get('alert-groups/list', res => {
=======
      io.get('alert-group/list', res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        state.notifyGroupListS = _.map(res.data, v => {
          return {
            id: v.id,
            code: v.groupName,
            disabled: false
          }
        })
        resolve(_.cloneDeep(state.notifyGroupListS))
      }).catch(res => {
        reject(res)
      })
    })
  },
  /**
   * Process definition startup interface
   */
  processStart ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/executors/start-process-instance`, payload, res => {
=======
      io.post(`projects/${state.projectName}/executors/start-process-instance`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * View log
   */
  getLog ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('log/detail', payload, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Get the process instance id according to the process definition id
   * @param taskId
   */
  getSubProcessId ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-instances/query-sub-by-parent`, payload, res => {
=======
      io.get(`projects/${state.projectName}/instance/select-sub-process`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Called before the process definition starts
   */
  getStartCheck ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/executors/start-check`, payload, res => {
=======
      io.post(`projects/${state.projectName}/executors/start-check`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Create timing
   */
  createSchedule ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/schedules`, payload, res => {
=======
      io.post(`projects/${state.projectName}/schedule/create`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Preview timing
   */
  previewSchedule ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/schedules/preview`, payload, res => {
=======
      io.post(`projects/${state.projectName}/schedule/preview`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
        // alert(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Timing list paging
   */
  getScheduleList ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/schedules`, payload, res => {
=======
      io.get(`projects/${state.projectName}/schedule/list-paging`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Timing online
   */
  scheduleOffline ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/schedules/${payload.id}/offline`, payload, res => {
=======
      io.post(`projects/${state.projectName}/schedule/offline`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Timed offline
   */
  scheduleOnline ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/schedules/${payload.id}/online`, payload, res => {
=======
      io.post(`projects/${state.projectName}/schedule/online`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Edit timing
   */
  updateSchedule ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.put(`projects/${state.projectCode}/schedules/${payload.id}`, payload, res => {
=======
      io.post(`projects/${state.projectName}/schedule/update`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Delete process instance
   */
  deleteInstance ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.delete(`projects/${state.projectCode}/process-instances/${payload.processInstanceId}`, {}, res => {
=======
      io.get(`projects/${state.projectName}/instance/delete`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Batch delete process instance
   */
  batchDeleteInstance ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/process-instances/batch-delete`, payload, res => {
=======
      io.get(`projects/${state.projectName}/instance/batch-delete`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Delete definition
   */
  deleteDefinition ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.delete(`projects/${state.projectCode}/process-definition/${payload.code}`, {}, res => {
=======
      io.get(`projects/${state.projectName}/process/delete`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Batch delete definition
   */
  batchDeleteDefinition ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.post(`projects/${state.projectCode}/process-definition/batch-delete`, payload, res => {
=======
      io.get(`projects/${state.projectName}/process/batch-delete`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * export definition
   */
  exportDefinition ({ state }, payload) {
    const downloadBlob = (data, fileNameS = 'json') => {
      if (!data) {
        return
      }
      const blob = new Blob([data])
      const fileName = `${fileNameS}.json`
      if ('download' in document.createElement('a')) { // 不是IE浏览器
        const url = window.URL.createObjectURL(blob)
        const link = document.createElement('a')
        link.style.display = 'none'
        link.href = url
        link.setAttribute('download', fileName)
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link) // 下载完成移除元素
        window.URL.revokeObjectURL(url) // 释放掉blob对象
      } else { // IE 10+
        window.navigator.msSaveBlob(blob, fileName)
      }
    }

<<<<<<< HEAD
    io.post(`projects/${state.projectCode}/process-definition/batch-export`, { codes: payload.codes }, res => {
=======
    io.get(`projects/${state.projectName}/process/export`, {processDefinitionIds: payload.processDefinitionIds}, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      downloadBlob(res, payload.fileName)
    }, e => {

    }, {
      responseType: 'blob'
    })
  },

  /**
   * Process instance get variable
   */
  getViewvariables ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-instances/${payload.processInstanceId}/view-variables`, res => {
=======
      io.get(`projects/${state.projectName}/instance/view-variables`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Get udfs function based on data source
   */
  getUdfList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('resources/udf-func/list', payload, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Query task instance list
   */
  getTaskInstanceList ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/task-instances`, payload, res => {
=======
      io.get(`projects/${state.projectName}/task-instance/list-paging`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
<<<<<<< HEAD
   * Force fail/kill/need_fault_tolerance task success
   */
  forceTaskSuccess ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.post(`projects/${state.projectCode}/task-instances/${payload.taskInstanceId}/force-success`, payload, res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
   * Query task record list
   */
  getTaskRecordList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('projects/task-record/list-paging', payload, res => {
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Query history task record list
   */
  getHistoryTaskRecordList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('projects/task-record/history-list-paging', payload, res => {
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * tree chart
   */
  getViewTree ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-definition/${payload.code}/view-tree`, { limit: payload.limit }, res => {
=======
      io.get(`projects/${state.projectName}/process/view-tree`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * gantt chart
   */
  getViewGantt ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-instances/${payload.processInstanceId}/view-gantt`, payload, res => {
=======
      io.get(`projects/${state.projectName}/instance/view-gantt`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * Query task node list
   */
  getProcessTasksList ({ state }, payload) {
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      io.get(`projects/${state.projectCode}/process-definition/${payload.code}/tasks`, payload, res => {
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  getTaskListDefIdAll ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectCode}/process-definition/batch-query-tasks`, payload, res => {
=======
      io.get(`projects/${state.projectName}/process/gen-task-list`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
<<<<<<< HEAD
   * remove timing
   */
  deleteTiming ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.delete(`projects/${state.projectCode}/schedules/${payload.scheduleId}`, payload, res => {
        resolve(res)
=======
   * Get the mailbox list interface
   */
  getReceiver ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectName}/executors/get-receiver-cc`, payload, res => {
        resolve(res.data)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      }).catch(e => {
        reject(e)
      })
    })
  },
<<<<<<< HEAD
  getResourceId ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`resources/${payload.id}`, payload, res => {
=======
  getTaskListDefIdAll ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectName}/process/get-task-list`, payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  },
<<<<<<< HEAD
  genTaskCodeList ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectCode}/task-definition/gen-task-codes`, payload, res => {
        resolve(res.data)
=======
  /**
   * remove timing
   */
  deleteTiming ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectName}/schedule/delete`, payload, res => {
        resolve(res)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
      }).catch(e => {
        reject(e)
      })
    })
  },
<<<<<<< HEAD
  getTaskDefinitions ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get(`projects/${state.projectCode}/task-definition`, payload, res => {
=======
  getResourceId ({ state }, payload) {
    return new Promise((resolve, reject) => {
      io.get('resources/queryResource', payload, res => {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resolve(res.data)
      }).catch(e => {
        reject(e)
      })
    })
  }
}
