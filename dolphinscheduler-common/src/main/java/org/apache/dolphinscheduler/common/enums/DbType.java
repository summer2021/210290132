/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.common.enums;

import static java.util.stream.Collectors.toMap;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.google.common.base.Functions;

public enum DbType {
    MYSQL(0),
    POSTGRESQL(1),
    HIVE(2),
    SPARK(3),
    CLICKHOUSE(4),
    ORACLE(5),
    SQLSERVER(6),
    DB2(7),
    PRESTO(8),
    H2(9);

    DbType(int code) {
        this.code = code;
=======
package org.apache.dolphinscheduler.common.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * data base types
 */
public enum DbType {
    /**
     * 0 mysql
     * 1 postgresql
     * 2 hive
     * 3 spark
     * 4 clickhouse
     * 5 oracle
     * 6 sqlserver
     * 7 db2
     */
    MYSQL(0, "mysql"),
    POSTGRESQL(1, "postgresql"),
    HIVE(2, "hive"),
    SPARK(3, "spark"),
    CLICKHOUSE(4, "clickhouse"),
    ORACLE(5, "oracle"),
    SQLSERVER(6, "sqlserver"),
    DB2(7, "db2");

    DbType(int code, String descp) {
        this.code = code;
        this.descp = descp;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @EnumValue
    private final int code;
<<<<<<< HEAD
=======
    private final String descp;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    public int getCode() {
        return code;
    }

<<<<<<< HEAD
    private static final Map<Integer, DbType> DB_TYPE_MAP =
            Arrays.stream(DbType.values()).collect(toMap(DbType::getCode, Functions.identity()));

    public static DbType of(int type) {
        if (DB_TYPE_MAP.containsKey(type)) {
            return DB_TYPE_MAP.get(type);
=======
    public String getDescp() {
        return descp;
    }


    public static DbType of(int type){
        for(DbType ty : values()){
            if(ty.getCode() == type){
                return ty;
            }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
        return null;
    }
}
