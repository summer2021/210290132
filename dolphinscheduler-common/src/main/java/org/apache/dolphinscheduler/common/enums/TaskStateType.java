/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.dolphinscheduler.common.enums;

/**
 * type of task state
 */
public enum TaskStateType {
    /**
     * 0 waiting running
     * 1 running
     * 2 finish
     * 3 failed
     * 4 success
     */
    WAITTING, RUNNING, FINISH, FAILED, SUCCESS;

    /**
     * convert task state to execute status integer array ;
<<<<<<< HEAD
     *
     * @param taskStateType task state type
     * @return result of execution status
     */
    public static int[] convert2ExecutStatusIntArray(TaskStateType taskStateType) {

        switch (taskStateType) {
=======
     * @param taskStateType task state type
     * @return result of execution status
     */
    public static int[] convert2ExecutStatusIntArray(TaskStateType taskStateType){

        switch (taskStateType){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            case SUCCESS:
                return new int[]{ExecutionStatus.SUCCESS.ordinal()};
            case FAILED:
                return new int[]{
                        ExecutionStatus.FAILURE.ordinal(),
                        ExecutionStatus.NEED_FAULT_TOLERANCE.ordinal()};
            case FINISH:
                return new int[]{
                        ExecutionStatus.PAUSE.ordinal(),
                        ExecutionStatus.STOP.ordinal()
                };
            case RUNNING:
                return new int[]{ExecutionStatus.SUBMITTED_SUCCESS.ordinal(),
                        ExecutionStatus.RUNNING_EXECUTION.ordinal(),
<<<<<<< HEAD
                        ExecutionStatus.DELAY_EXECUTION.ordinal(),
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                        ExecutionStatus.READY_PAUSE.ordinal(),
                        ExecutionStatus.READY_STOP.ordinal()};
            case WAITTING:
                return new int[]{
                        ExecutionStatus.SUBMITTED_SUCCESS.ordinal()
                };
<<<<<<< HEAD
            default:
                break;
=======
                default:
                    break;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }
        return new int[0];
    }

}
