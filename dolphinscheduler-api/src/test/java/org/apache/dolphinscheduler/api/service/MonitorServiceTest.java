/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.service.impl.MonitorServiceImpl;
=======
package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.enums.Status;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.DbType;
import org.apache.dolphinscheduler.common.model.Server;
import org.apache.dolphinscheduler.common.utils.CollectionUtils;
import org.apache.dolphinscheduler.dao.MonitorDBDao;
import org.apache.dolphinscheduler.dao.entity.MonitorRecord;
<<<<<<< HEAD

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * monitor service test
 */
=======
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RunWith(MockitoJUnitRunner.class)
public class MonitorServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(MonitorServiceTest.class);

    @InjectMocks
<<<<<<< HEAD
    private MonitorServiceImpl monitorService;

    @Mock
    private MonitorDBDao monitorDBDao;

    @Test
    public  void testQueryDatabaseState() {
=======
    private MonitorService monitorService;
    @Mock
    private MonitorDBDao monitorDBDao;


    @Test
    public  void testQueryDatabaseState(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Mockito.when(monitorDBDao.queryDatabaseState()).thenReturn(getList());
        Map<String,Object> result = monitorService.queryDatabaseState(null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
        List<MonitorRecord> monitorRecordList = (List<MonitorRecord>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(monitorRecordList));
    }
<<<<<<< HEAD

    @Test
    public  void testQueryMaster() {
        //TODO need zk
        /*Map<String,Object> result = monitorService.queryMaster(null);*/
        /*logger.info(result.toString());*/
        /*Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));*/
    }

    @Test
    public  void testQueryZookeeperState() {
        //TODO need zk
        /*Map<String,Object> result = monitorService.queryZookeeperState(null);*/
        /*logger.info(result.toString());*/
        /*Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));*/
    }

    @Test
    public  void testGetServerListFromZK() {
        //TODO need zk
        /*List<Server> serverList = monitorService.getServerListFromZK(true);*/
        /*logger.info(serverList.toString());*/
    }

    private List<MonitorRecord> getList() {
=======
    @Test
    public  void testQueryMaster(){
        //TODO need zk
//        Map<String,Object> result = monitorService.queryMaster(null);
//        logger.info(result.toString());
//        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
    }
    @Test
    public  void testQueryZookeeperState(){
        //TODO need zk
//        Map<String,Object> result = monitorService.queryZookeeperState(null);
//        logger.info(result.toString());
//        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
    }

    @Test
    public  void testGetServerListFromZK(){
        //TODO need zk
//        List<Server> serverList = monitorService.getServerListFromZK(true);
//        logger.info(serverList.toString());
    }

    private List<MonitorRecord> getList(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<MonitorRecord> monitorRecordList = new ArrayList<>();
        monitorRecordList.add(getEntity());
        return monitorRecordList;
    }

<<<<<<< HEAD
    private MonitorRecord getEntity() {
=======
    private MonitorRecord getEntity(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        MonitorRecord monitorRecord = new  MonitorRecord();
        monitorRecord.setDbType(DbType.MYSQL);
        return monitorRecord;
    }

<<<<<<< HEAD
    private List<Server> getServerList() {
=======
    private List<Server> getServerList(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<Server> servers = new ArrayList<>();
        servers.add(new Server());
        return servers;
    }

}
