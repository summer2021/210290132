/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.service.impl.ResourcesServiceImpl;
=======
package org.apache.dolphinscheduler.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dolphinscheduler.api.enums.Status;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.utils.PageInfo;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.ResourceType;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.CollectionUtils;
import org.apache.dolphinscheduler.common.utils.FileUtils;
import org.apache.dolphinscheduler.common.utils.HadoopUtils;
import org.apache.dolphinscheduler.common.utils.PropertyUtils;
import org.apache.dolphinscheduler.dao.entity.Resource;
import org.apache.dolphinscheduler.dao.entity.Tenant;
import org.apache.dolphinscheduler.dao.entity.UdfFunc;
import org.apache.dolphinscheduler.dao.entity.User;
<<<<<<< HEAD
import org.apache.dolphinscheduler.dao.mapper.ProcessDefinitionMapper;
import org.apache.dolphinscheduler.dao.mapper.ResourceMapper;
import org.apache.dolphinscheduler.dao.mapper.ResourceUserMapper;
import org.apache.dolphinscheduler.dao.mapper.TenantMapper;
import org.apache.dolphinscheduler.dao.mapper.UdfFuncMapper;
import org.apache.dolphinscheduler.dao.mapper.UserMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

=======
import org.apache.dolphinscheduler.dao.mapper.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockMultipartFile;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * resources service test
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"sun.security.*", "javax.net.*"})
@PrepareForTest({HadoopUtils.class, PropertyUtils.class, FileUtils.class, org.apache.dolphinscheduler.api.utils.FileUtils.class})
public class ResourcesServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(ResourcesServiceTest.class);

    @InjectMocks
    private ResourcesServiceImpl resourcesService;

    @Mock
    private ResourceMapper resourcesMapper;

    @Mock
    private TenantMapper tenantMapper;

    @Mock
    private HadoopUtils hadoopUtils;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UdfFuncMapper udfFunctionMapper;

    @Mock
    private ProcessDefinitionMapper processDefinitionMapper;

    @Mock
    private ResourceUserMapper resourceUserMapper;

=======
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"sun.security.*", "javax.net.*"})
@PrepareForTest({HadoopUtils.class,PropertyUtils.class, FileUtils.class,org.apache.dolphinscheduler.api.utils.FileUtils.class})
public class ResourcesServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(ResourcesServiceTest.class);

    @InjectMocks
    private ResourcesService resourcesService;
    @Mock
    private ResourceMapper resourcesMapper;
    @Mock
    private TenantMapper tenantMapper;
    @Mock
    private ResourceUserMapper resourceUserMapper;
    @Mock
    private HadoopUtils hadoopUtils;
    @Mock
    private UserMapper userMapper;
    @Mock
    private UdfFuncMapper udfFunctionMapper;
    @Mock
    private ProcessDefinitionMapper processDefinitionMapper;

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Before
    public void setUp() {

        PowerMockito.mockStatic(HadoopUtils.class);
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.mockStatic(org.apache.dolphinscheduler.api.utils.FileUtils.class);
        try {
            // new HadoopUtils
            PowerMockito.whenNew(HadoopUtils.class).withNoArguments().thenReturn(hadoopUtils);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PowerMockito.when(HadoopUtils.getInstance()).thenReturn(hadoopUtils);
        PowerMockito.mockStatic(PropertyUtils.class);
    }

    @Test
<<<<<<< HEAD
    public void testCreateResource() {
=======
    public void testCreateResource(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);
        User user = new User();
        //HDFS_NOT_STARTUP
<<<<<<< HEAD
        Result result = resourcesService.createResource(user, "ResourcesServiceTest", "ResourcesServiceTest", ResourceType.FILE, null, -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());

        //RESOURCE_FILE_IS_EMPTY
        MockMultipartFile mockMultipartFile = new MockMultipartFile("test.pdf", "".getBytes());
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        result = resourcesService.createResource(user, "ResourcesServiceTest", "ResourcesServiceTest", ResourceType.FILE, mockMultipartFile, -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_FILE_IS_EMPTY.getMsg(), result.getMsg());

        //RESOURCE_SUFFIX_FORBID_CHANGE
        mockMultipartFile = new MockMultipartFile("test.pdf", "test.pdf", "pdf", "test".getBytes());
        PowerMockito.when(FileUtils.suffix("test.pdf")).thenReturn("pdf");
        PowerMockito.when(FileUtils.suffix("ResourcesServiceTest.jar")).thenReturn("jar");
        result = resourcesService.createResource(user, "ResourcesServiceTest.jar", "ResourcesServiceTest", ResourceType.FILE, mockMultipartFile, -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_FORBID_CHANGE.getMsg(), result.getMsg());

        //UDF_RESOURCE_SUFFIX_NOT_JAR
        mockMultipartFile = new MockMultipartFile("ResourcesServiceTest.pdf", "ResourcesServiceTest.pdf", "pdf", "test".getBytes());
        PowerMockito.when(FileUtils.suffix("ResourcesServiceTest.pdf")).thenReturn("pdf");
        result = resourcesService.createResource(user, "ResourcesServiceTest.pdf", "ResourcesServiceTest", ResourceType.UDF, mockMultipartFile, -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.UDF_RESOURCE_SUFFIX_NOT_JAR.getMsg(), result.getMsg());
=======
        Result result = resourcesService.createResource(user,"ResourcesServiceTest","ResourcesServiceTest",ResourceType.FILE,null,-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(),result.getMsg());

        //RESOURCE_FILE_IS_EMPTY
        MockMultipartFile mockMultipartFile = new MockMultipartFile("test.pdf",new String().getBytes());
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        result = resourcesService.createResource(user,"ResourcesServiceTest","ResourcesServiceTest",ResourceType.FILE,mockMultipartFile,-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_FILE_IS_EMPTY.getMsg(),result.getMsg());

        //RESOURCE_SUFFIX_FORBID_CHANGE
        mockMultipartFile = new MockMultipartFile("test.pdf","test.pdf","pdf",new String("test").getBytes());
        PowerMockito.when(FileUtils.suffix("test.pdf")).thenReturn("pdf");
        PowerMockito.when(FileUtils.suffix("ResourcesServiceTest.jar")).thenReturn("jar");
        result = resourcesService.createResource(user,"ResourcesServiceTest.jar","ResourcesServiceTest",ResourceType.FILE,mockMultipartFile,-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_FORBID_CHANGE.getMsg(),result.getMsg());

        //UDF_RESOURCE_SUFFIX_NOT_JAR
        mockMultipartFile = new MockMultipartFile("ResourcesServiceTest.pdf","ResourcesServiceTest.pdf","pdf",new String("test").getBytes());
        PowerMockito.when(FileUtils.suffix("ResourcesServiceTest.pdf")).thenReturn("pdf");
        result = resourcesService.createResource(user,"ResourcesServiceTest.pdf","ResourcesServiceTest",ResourceType.UDF,mockMultipartFile,-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.UDF_RESOURCE_SUFFIX_NOT_JAR.getMsg(),result.getMsg());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }

    @Test
<<<<<<< HEAD
    public void testCreateDirecotry() {
=======
    public void testCreateDirecotry(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);
        User user = new User();
        //HDFS_NOT_STARTUP
<<<<<<< HEAD
        Result result = resourcesService.createDirectory(user, "directoryTest", "directory test", ResourceType.FILE, -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());
=======
        Result result = resourcesService.createDirectory(user,"directoryTest","directory test",ResourceType.FILE,-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //PARENT_RESOURCE_NOT_EXIST
        user.setId(1);
        user.setTenantId(1);
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        Mockito.when(resourcesMapper.selectById(Mockito.anyInt())).thenReturn(null);
<<<<<<< HEAD
        result = resourcesService.createDirectory(user, "directoryTest", "directory test", ResourceType.FILE, 1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.PARENT_RESOURCE_NOT_EXIST.getMsg(), result.getMsg());
        //RESOURCE_EXIST
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        Mockito.when(resourcesMapper.existResource("/directoryTest", 0, 0)).thenReturn(true);
        result = resourcesService.createDirectory(user, "directoryTest", "directory test", ResourceType.FILE, -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_EXIST.getMsg(), result.getMsg());
=======
        result = resourcesService.createDirectory(user,"directoryTest","directory test",ResourceType.FILE,1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.PARENT_RESOURCE_NOT_EXIST.getMsg(),result.getMsg());
        //RESOURCE_EXIST
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        Mockito.when(resourcesMapper.queryResourceList("/directoryTest", 0, 0)).thenReturn(getResourceList());
        result = resourcesService.createDirectory(user,"directoryTest","directory test",ResourceType.FILE,-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_EXIST.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }

    @Test
<<<<<<< HEAD
    public void testUpdateResource() {
=======
    public void testUpdateResource(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);
        User user = new User();
        //HDFS_NOT_STARTUP
<<<<<<< HEAD
        Result result = resourcesService.updateResource(user, 1, "ResourcesServiceTest", "ResourcesServiceTest", ResourceType.FILE, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());
=======
        Result result = resourcesService.updateResource(user,1,"ResourcesServiceTest","ResourcesServiceTest",ResourceType.FILE,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RESOURCE_NOT_EXIST
        Mockito.when(resourcesMapper.selectById(1)).thenReturn(getResource());
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
<<<<<<< HEAD
        result = resourcesService.updateResource(user, 0, "ResourcesServiceTest", "ResourcesServiceTest", ResourceType.FILE, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), result.getMsg());

        //USER_NO_OPERATION_PERM
        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest", "ResourcesServiceTest", ResourceType.FILE, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM.getMsg(), result.getMsg());
=======
        result = resourcesService.updateResource(user,0,"ResourcesServiceTest","ResourcesServiceTest",ResourceType.FILE,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(),result.getMsg());

        //USER_NO_OPERATION_PERM
        result = resourcesService.updateResource(user,1,"ResourcesServiceTest","ResourcesServiceTest",ResourceType.FILE,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RESOURCE_NOT_EXIST
        user.setId(1);
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
<<<<<<< HEAD
        PowerMockito.when(HadoopUtils.getHdfsFileName(Mockito.any(), Mockito.any(), Mockito.anyString())).thenReturn("test1");
=======
        PowerMockito.when(HadoopUtils.getHdfsFileName(Mockito.any(), Mockito.any(),Mockito.anyString())).thenReturn("test1");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        try {
            Mockito.when(HadoopUtils.getInstance().exists(Mockito.any())).thenReturn(false);
        } catch (IOException e) {
<<<<<<< HEAD
            logger.error(e.getMessage(), e);
        }
        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest1.jar", "ResourcesServiceTest", ResourceType.UDF, null);
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), result.getMsg());
=======
            logger.error(e.getMessage(),e);
        }
        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest1.jar", "ResourcesServiceTest", ResourceType.UDF,null);
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //SUCCESS
        user.setId(1);
        Mockito.when(userMapper.queryDetailsById(1)).thenReturn(getUser());
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        try {
            Mockito.when(HadoopUtils.getInstance().exists(Mockito.any())).thenReturn(true);
        } catch (IOException e) {
<<<<<<< HEAD
            logger.error(e.getMessage(), e);
        }

        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest.jar", "ResourcesServiceTest", ResourceType.FILE, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());

        //RESOURCE_EXIST
        Mockito.when(resourcesMapper.existResource("/ResourcesServiceTest1.jar", 0, 0)).thenReturn(true);
        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest1.jar", "ResourcesServiceTest", ResourceType.FILE, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_EXIST.getMsg(), result.getMsg());
        //USER_NOT_EXIST
        Mockito.when(userMapper.selectById(Mockito.anyInt())).thenReturn(null);
        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest1.jar", "ResourcesServiceTest", ResourceType.UDF, null);
=======
            logger.error(e.getMessage(),e);
        }

        result = resourcesService.updateResource(user,1,"ResourcesServiceTest.jar","ResourcesServiceTest",ResourceType.FILE,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(),result.getMsg());

        //RESOURCE_EXIST
        Mockito.when(resourcesMapper.queryResourceList("/ResourcesServiceTest1.jar", 0, 0)).thenReturn(getResourceList());
        result = resourcesService.updateResource(user,1,"ResourcesServiceTest1.jar","ResourcesServiceTest",ResourceType.FILE,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_EXIST.getMsg(),result.getMsg());
        //USER_NOT_EXIST
        Mockito.when(userMapper.selectById(Mockito.anyInt())).thenReturn(null);
        result = resourcesService.updateResource(user,1,"ResourcesServiceTest1.jar","ResourcesServiceTest",ResourceType.UDF,null);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertTrue(Status.USER_NOT_EXIST.getCode() == result.getCode());

        //TENANT_NOT_EXIST
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        Mockito.when(tenantMapper.queryById(Mockito.anyInt())).thenReturn(null);
<<<<<<< HEAD
        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest1.jar", "ResourcesServiceTest", ResourceType.UDF, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(), result.getMsg());
=======
        result = resourcesService.updateResource(user,1,"ResourcesServiceTest1.jar","ResourcesServiceTest",ResourceType.UDF,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(),result.getMsg());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //SUCCESS
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        PowerMockito.when(HadoopUtils.getHdfsResourceFileName(Mockito.any(), Mockito.any())).thenReturn("test");
        try {
<<<<<<< HEAD
            PowerMockito.when(HadoopUtils.getInstance().copy(Mockito.anyString(), Mockito.anyString(), true, true)).thenReturn(true);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        result = resourcesService.updateResource(user, 1, "ResourcesServiceTest1.jar", "ResourcesServiceTest1.jar", ResourceType.UDF, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());
=======
            PowerMockito.when(HadoopUtils.getInstance().copy(Mockito.anyString(),Mockito.anyString(),true,true)).thenReturn(true);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }

        result = resourcesService.updateResource(user,1,"ResourcesServiceTest1.jar","ResourcesServiceTest1.jar",ResourceType.UDF,null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }

    @Test
<<<<<<< HEAD
    public void testQueryResourceListPaging() {
        User loginUser = new User();
        loginUser.setUserType(UserType.ADMIN_USER);
        IPage<Resource> resourcePage = new Page<>(1, 10);
        resourcePage.setTotal(1);
        resourcePage.setRecords(getResourceList());

        Mockito.when(resourcesMapper.queryResourcePaging(Mockito.any(Page.class),
                Mockito.eq(0), Mockito.eq(-1), Mockito.eq(0), Mockito.eq("test"), Mockito.any())).thenReturn(resourcePage);
        Result result = resourcesService.queryResourceListPaging(loginUser, -1, ResourceType.FILE, "test", 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getCode(), (int)result.getCode());
        PageInfo pageInfo = (PageInfo) result.getData();
        Assert.assertTrue(CollectionUtils.isNotEmpty(pageInfo.getTotalList()));
=======
    public void testQueryResourceListPaging(){
        User loginUser = new User();
        loginUser.setUserType(UserType.ADMIN_USER);
        IPage<Resource> resourcePage = new Page<>(1,10);
        resourcePage.setTotal(1);
        resourcePage.setRecords(getResourceList());
        Mockito.when(resourcesMapper.queryResourcePaging(Mockito.any(Page.class),
                Mockito.eq(0),Mockito.eq(-1), Mockito.eq(0), Mockito.eq("test"))).thenReturn(resourcePage);
        Map<String, Object> result = resourcesService.queryResourceListPaging(loginUser,-1,ResourceType.FILE,"test",1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        PageInfo pageInfo = (PageInfo) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(pageInfo.getLists()));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }

    @Test
<<<<<<< HEAD
    public void testQueryResourceList() {
        User loginUser = new User();
        loginUser.setId(0);
        loginUser.setUserType(UserType.ADMIN_USER);
        Mockito.when(resourcesMapper.queryResourceListAuthored(0, 0)).thenReturn(getResourceList());
=======
    public void testQueryResourceList(){
        User loginUser = new User();
        loginUser.setId(0);
        loginUser.setUserType(UserType.ADMIN_USER);
        Mockito.when(resourcesMapper.queryResourceListAuthored(0, 0,0)).thenReturn(getResourceList());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = resourcesService.queryResourceList(loginUser, ResourceType.FILE);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        List<Resource> resourceList = (List<Resource>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(resourceList));
    }

    @Test
<<<<<<< HEAD
    public void testDelete() {
=======
    public void testDelete(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        User loginUser = new User();
        loginUser.setId(0);
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());

        try {
            // HDFS_NOT_STARTUP
<<<<<<< HEAD
            Result result = resourcesService.delete(loginUser, 1);
=======
            Result result = resourcesService.delete(loginUser,1);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());

            //RESOURCE_NOT_EXIST
            PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
            Mockito.when(resourcesMapper.selectById(1)).thenReturn(getResource());
<<<<<<< HEAD
            result = resourcesService.delete(loginUser, 2);
=======
            result = resourcesService.delete(loginUser,2);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), result.getMsg());

            // USER_NO_OPERATION_PERM
<<<<<<< HEAD
            result = resourcesService.delete(loginUser, 2);
=======
            result = resourcesService.delete(loginUser,2);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), result.getMsg());

            //TENANT_NOT_EXIST
            loginUser.setUserType(UserType.ADMIN_USER);
            loginUser.setTenantId(2);
            Mockito.when(userMapper.selectById(Mockito.anyInt())).thenReturn(loginUser);
<<<<<<< HEAD
            result = resourcesService.delete(loginUser, 1);
=======
            result = resourcesService.delete(loginUser,1);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(), result.getMsg());

            //SUCCESS
            loginUser.setTenantId(1);
            Mockito.when(hadoopUtils.delete(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(true);
<<<<<<< HEAD
            Mockito.when(processDefinitionMapper.listResources()).thenReturn(getResources());
            Mockito.when(resourcesMapper.deleteIds(Mockito.any())).thenReturn(1);
            Mockito.when(resourceUserMapper.deleteResourceUserArray(Mockito.anyInt(), Mockito.any())).thenReturn(1);
            result = resourcesService.delete(loginUser, 1);
=======
            result = resourcesService.delete(loginUser,1);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());

        } catch (Exception e) {
<<<<<<< HEAD
            logger.error("delete error", e);
=======
            logger.error("delete error",e);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            Assert.assertTrue(false);
        }
    }

    @Test
<<<<<<< HEAD
    public void testVerifyResourceName() {

        User user = new User();
        user.setId(1);
        Mockito.when(resourcesMapper.existResource("/ResourcesServiceTest.jar", 0, 0)).thenReturn(true);
        Result result = resourcesService.verifyResourceName("/ResourcesServiceTest.jar", ResourceType.FILE, user);
=======
    public  void testVerifyResourceName(){

        User user = new User();
        user.setId(1);
        Mockito.when(resourcesMapper.queryResourceList("/ResourcesServiceTest.jar", 0, 0)).thenReturn(getResourceList());
        Result result = resourcesService.verifyResourceName("/ResourcesServiceTest.jar",ResourceType.FILE,user);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_EXIST.getMsg(), result.getMsg());

        //TENANT_NOT_EXIST
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        String unExistFullName = "/test.jar";
        try {
            Mockito.when(hadoopUtils.exists(unExistFullName)).thenReturn(false);
        } catch (IOException e) {
<<<<<<< HEAD
            logger.error("hadoop error", e);
        }
        result = resourcesService.verifyResourceName("/test.jar", ResourceType.FILE, user);
        logger.info(result.toString());
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(), result.getMsg());

=======
            logger.error("hadoop error",e);
        }
        result = resourcesService.verifyResourceName("/test.jar",ResourceType.FILE,user);
        logger.info(result.toString());
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(), result.getMsg());


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //RESOURCE_FILE_EXIST
        user.setTenantId(1);
        try {
            Mockito.when(hadoopUtils.exists("test")).thenReturn(true);
        } catch (IOException e) {
<<<<<<< HEAD
            logger.error("hadoop error", e);
        }
        PowerMockito.when(HadoopUtils.getHdfsResourceFileName("123", "test1")).thenReturn("test");
        result = resourcesService.verifyResourceName("/ResourcesServiceTest.jar", ResourceType.FILE, user);
        logger.info(result.toString());
        Assert.assertTrue(Status.RESOURCE_EXIST.getCode() == result.getCode());

        //SUCCESS
        result = resourcesService.verifyResourceName("test2", ResourceType.FILE, user);
=======
            logger.error("hadoop error",e);
        }
        PowerMockito.when(HadoopUtils.getHdfsResourceFileName("123", "test1")).thenReturn("test");
        result = resourcesService.verifyResourceName("/ResourcesServiceTest.jar",ResourceType.FILE,user);
        logger.info(result.toString());
        Assert.assertTrue(Status.RESOURCE_EXIST.getCode()==result.getCode());

        //SUCCESS
        result = resourcesService.verifyResourceName("test2",ResourceType.FILE,user);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());

    }

    @Test
<<<<<<< HEAD
    public void testReadResource() {
=======
    public void testReadResource(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);

        //HDFS_NOT_STARTUP
<<<<<<< HEAD
        Result result = resourcesService.readResource(1, 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());
=======
        Result result = resourcesService.readResource(1,1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RESOURCE_NOT_EXIST
        Mockito.when(resourcesMapper.selectById(1)).thenReturn(getResource());
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
<<<<<<< HEAD
        result = resourcesService.readResource(2, 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), result.getMsg());
=======
        result = resourcesService.readResource(2,1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(),result.getMsg());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RESOURCE_SUFFIX_NOT_SUPPORT_VIEW
        PowerMockito.when(FileUtils.getResourceViewSuffixs()).thenReturn("class");
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
<<<<<<< HEAD
        result = resourcesService.readResource(1, 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_NOT_SUPPORT_VIEW.getMsg(), result.getMsg());
=======
        result = resourcesService.readResource(1,1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_NOT_SUPPORT_VIEW.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //USER_NOT_EXIST
        PowerMockito.when(FileUtils.getResourceViewSuffixs()).thenReturn("jar");
        PowerMockito.when(FileUtils.suffix("ResourcesServiceTest.jar")).thenReturn("jar");
<<<<<<< HEAD
        result = resourcesService.readResource(1, 1, 10);
        logger.info(result.toString());
        Assert.assertTrue(Status.USER_NOT_EXIST.getCode() == result.getCode());

        //TENANT_NOT_EXIST
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        result = resourcesService.readResource(1, 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(), result.getMsg());
=======
        result = resourcesService.readResource(1,1,10);
        logger.info(result.toString());
        Assert.assertTrue(Status.USER_NOT_EXIST.getCode()==result.getCode());


        //TENANT_NOT_EXIST
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        result = resourcesService.readResource(1,1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getMsg(),result.getMsg());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RESOURCE_FILE_NOT_EXIST
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        try {
            Mockito.when(hadoopUtils.exists(Mockito.anyString())).thenReturn(false);
        } catch (IOException e) {
<<<<<<< HEAD
            logger.error("hadoop error", e);
        }
        result = resourcesService.readResource(1, 1, 10);
        logger.info(result.toString());
        Assert.assertTrue(Status.RESOURCE_FILE_NOT_EXIST.getCode() == result.getCode());
=======
            logger.error("hadoop error",e);
        }
        result = resourcesService.readResource(1,1,10);
        logger.info(result.toString());
        Assert.assertTrue(Status.RESOURCE_FILE_NOT_EXIST.getCode()==result.getCode());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //SUCCESS
        try {
            Mockito.when(hadoopUtils.exists(null)).thenReturn(true);
<<<<<<< HEAD
            Mockito.when(hadoopUtils.catFile(null, 1, 10)).thenReturn(getContent());
        } catch (IOException e) {
            logger.error("hadoop error", e);
        }
        result = resourcesService.readResource(1, 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());
=======
            Mockito.when(hadoopUtils.catFile(null,1,10)).thenReturn(getContent());
        } catch (IOException e) {
            logger.error("hadoop error",e);
        }
        result = resourcesService.readResource(1,1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(),result.getMsg());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }

    @Test
    public void testOnlineCreateResource() {

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);
        PowerMockito.when(HadoopUtils.getHdfsResDir("hdfsdDir")).thenReturn("hdfsDir");
        PowerMockito.when(HadoopUtils.getHdfsUdfDir("udfDir")).thenReturn("udfDir");
        User user = getUser();
        //HDFS_NOT_STARTUP
<<<<<<< HEAD
        Result result = resourcesService.onlineCreateResource(user, ResourceType.FILE, "test", "jar", "desc", "content", -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());
=======
        Result result = resourcesService.onlineCreateResource(user,ResourceType.FILE,"test","jar","desc","content",-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RESOURCE_SUFFIX_NOT_SUPPORT_VIEW
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        PowerMockito.when(FileUtils.getResourceViewSuffixs()).thenReturn("class");
<<<<<<< HEAD
        result = resourcesService.onlineCreateResource(user, ResourceType.FILE, "test", "jar", "desc", "content", -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_NOT_SUPPORT_VIEW.getMsg(), result.getMsg());
=======
        result =  resourcesService.onlineCreateResource(user,ResourceType.FILE,"test","jar","desc","content",-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_NOT_SUPPORT_VIEW.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //RuntimeException
        try {
            PowerMockito.when(FileUtils.getResourceViewSuffixs()).thenReturn("jar");
            Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
<<<<<<< HEAD
            result = resourcesService.onlineCreateResource(user, ResourceType.FILE, "test", "jar", "desc", "content", -1, "/");
        } catch (RuntimeException ex) {
=======
            result = resourcesService.onlineCreateResource(user, ResourceType.FILE, "test", "jar", "desc", "content",-1,"/");
        }catch (RuntimeException ex){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), ex.getMessage());
        }

        //SUCCESS
        Mockito.when(FileUtils.getUploadFilename(Mockito.anyString(), Mockito.anyString())).thenReturn("test");
        PowerMockito.when(FileUtils.writeContent2File(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
<<<<<<< HEAD
        result = resourcesService.onlineCreateResource(user, ResourceType.FILE, "test", "jar", "desc", "content", -1, "/");
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());
=======
        result =  resourcesService.onlineCreateResource(user,ResourceType.FILE,"test","jar","desc","content",-1,"/");
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(),result.getMsg());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    }

    @Test
<<<<<<< HEAD
    public void testUpdateResourceContent() {
=======
    public void testUpdateResourceContent(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        User loginUser = new User();
        loginUser.setId(0);
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);

        // HDFS_NOT_STARTUP
<<<<<<< HEAD
        Result result = resourcesService.updateResourceContent(1, "content");
=======
        Result result = resourcesService.updateResourceContent(1,"content");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertEquals(Status.HDFS_NOT_STARTUP.getMsg(), result.getMsg());

        //RESOURCE_NOT_EXIST
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        Mockito.when(resourcesMapper.selectById(1)).thenReturn(getResource());
<<<<<<< HEAD
        result = resourcesService.updateResourceContent(2, "content");
=======
        result = resourcesService.updateResourceContent(2,"content");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_NOT_EXIST.getMsg(), result.getMsg());

        //RESOURCE_SUFFIX_NOT_SUPPORT_VIEW
        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        PowerMockito.when(FileUtils.getResourceViewSuffixs()).thenReturn("class");
<<<<<<< HEAD
        result = resourcesService.updateResourceContent(1, "content");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_NOT_SUPPORT_VIEW.getMsg(), result.getMsg());
=======
        result = resourcesService.updateResourceContent(1,"content");
        logger.info(result.toString());
        Assert.assertEquals(Status.RESOURCE_SUFFIX_NOT_SUPPORT_VIEW.getMsg(),result.getMsg());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //USER_NOT_EXIST
        PowerMockito.when(FileUtils.getResourceViewSuffixs()).thenReturn("jar");
        PowerMockito.when(FileUtils.suffix("ResourcesServiceTest.jar")).thenReturn("jar");
<<<<<<< HEAD
        result = resourcesService.updateResourceContent(1, "content");
        logger.info(result.toString());
        Assert.assertTrue(Status.USER_NOT_EXIST.getCode() == result.getCode());

        //TENANT_NOT_EXIST
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        result = resourcesService.updateResourceContent(1, "content");
=======
        result = resourcesService.updateResourceContent(1,"content");
        logger.info(result.toString());
        Assert.assertTrue(Status.USER_NOT_EXIST.getCode() == result.getCode());


        //TENANT_NOT_EXIST
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        result = resourcesService.updateResourceContent(1,"content");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertTrue(Status.TENANT_NOT_EXIST.getCode() == result.getCode());

        //SUCCESS
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        Mockito.when(FileUtils.getUploadFilename(Mockito.anyString(), Mockito.anyString())).thenReturn("test");
        PowerMockito.when(FileUtils.writeContent2File(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
<<<<<<< HEAD
        result = resourcesService.updateResourceContent(1, "content");
=======
        result = resourcesService.updateResourceContent(1,"content");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());
    }

    @Test
<<<<<<< HEAD
    public void testDownloadResource() {
=======
    public void testDownloadResource(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(true);
        Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
        Mockito.when(userMapper.selectById(1)).thenReturn(getUser());
        org.springframework.core.io.Resource resourceMock = Mockito.mock(org.springframework.core.io.Resource.class);
        try {
            //resource null
            org.springframework.core.io.Resource resource = resourcesService.downloadResource(1);
            Assert.assertNull(resource);

            Mockito.when(resourcesMapper.selectById(1)).thenReturn(getResource());
            PowerMockito.when(org.apache.dolphinscheduler.api.utils.FileUtils.file2Resource(Mockito.any())).thenReturn(resourceMock);
            resource = resourcesService.downloadResource(1);
            Assert.assertNotNull(resource);
        } catch (Exception e) {
<<<<<<< HEAD
            logger.error("DownloadResource error", e);
=======
            logger.error("DownloadResource error",e);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            Assert.assertTrue(false);
        }

    }

    @Test
<<<<<<< HEAD
    public void testUnauthorizedFile() {
        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.unauthorizedFile(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
=======
    public void testUnauthorizedFile(){
        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.unauthorizedFile(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //SUCCESS
        user.setUserType(UserType.ADMIN_USER);
        Mockito.when(resourcesMapper.queryResourceExceptUserId(1)).thenReturn(getResourceList());
<<<<<<< HEAD
        result = resourcesService.unauthorizedFile(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
=======
        result = resourcesService.unauthorizedFile(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<Resource> resources = (List<Resource>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(resources));

    }

    @Test
<<<<<<< HEAD
    public void testUnauthorizedUDFFunction() {

        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.unauthorizedUDFFunction(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
=======
    public void testUnauthorizedUDFFunction(){

        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.unauthorizedUDFFunction(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //SUCCESS
        user.setUserType(UserType.ADMIN_USER);
        Mockito.when(udfFunctionMapper.queryUdfFuncExceptUserId(1)).thenReturn(getUdfFuncList());
<<<<<<< HEAD
        result = resourcesService.unauthorizedUDFFunction(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
=======
        result = resourcesService.unauthorizedUDFFunction(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<UdfFunc> udfFuncs = (List<UdfFunc>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(udfFuncs));
    }

<<<<<<< HEAD
    @Test
    public void testAuthorizedUDFFunction() {
        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.authorizedUDFFunction(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //SUCCESS
        user.setUserType(UserType.ADMIN_USER);
        Mockito.when(udfFunctionMapper.queryAuthedUdfFunc(1)).thenReturn(getUdfFuncList());
        result = resourcesService.authorizedUDFFunction(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
=======

    @Test
    public void testAuthorizedUDFFunction(){
        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.authorizedUDFFunction(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
        //SUCCESS
        user.setUserType(UserType.ADMIN_USER);
        Mockito.when(udfFunctionMapper.queryAuthedUdfFunc(1)).thenReturn(getUdfFuncList());
        result = resourcesService.authorizedUDFFunction(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<UdfFunc> udfFuncs = (List<UdfFunc>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(udfFuncs));
    }

    @Test
<<<<<<< HEAD
    public void testAuthorizedFile() {

        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.authorizedFile(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //SUCCESS
        user.setUserType(UserType.ADMIN_USER);

        List<Integer> resIds = new ArrayList<>();
        resIds.add(1);
        Mockito.when(resourceUserMapper.queryResourcesIdListByUserIdAndPerm(Mockito.anyInt(), Mockito.anyInt())).thenReturn(resIds);
        Mockito.when(resourcesMapper.queryResourceListById(Mockito.any())).thenReturn(getResourceList());
        result = resourcesService.authorizedFile(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
=======
    public void testAuthorizedFile(){

        User user = getUser();
        //USER_NO_OPERATION_PERM
        Map<String, Object> result = resourcesService.authorizedFile(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
        //SUCCESS
        user.setUserType(UserType.ADMIN_USER);
        Mockito.when(resourcesMapper.queryAuthorizedResourceList(1)).thenReturn(getResourceList());
        result = resourcesService.authorizedFile(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<Resource> resources = (List<Resource>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(resources));
    }

<<<<<<< HEAD
    @Test
    public void testCatFile() {

        PowerMockito.when(PropertyUtils.getResUploadStartupState()).thenReturn(false);

        //SUCCESS
        try {
            Mockito.when(hadoopUtils.exists(null)).thenReturn(true);
            Mockito.when(hadoopUtils.catFile(null, 1, 10)).thenReturn(getContent());

            List<String> list = hadoopUtils.catFile(null, 1, 10);
            Assert.assertNotNull(list);

        } catch (IOException e) {
            logger.error("hadoop error", e);
        }
    }

    private List<Resource> getResourceList() {

        List<Resource> resources = new ArrayList<>();
=======

    private List<Resource> getResourceList(){

        List<Resource> resources =  new ArrayList<>();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        resources.add(getResource());
        return resources;
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private Tenant getTenant() {
        Tenant tenant = new Tenant();
        tenant.setTenantCode("123");
        return tenant;
    }

<<<<<<< HEAD
    private Resource getResource() {
=======
    private Resource getResource(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Resource resource = new Resource();
        resource.setPid(-1);
        resource.setUserId(1);
        resource.setDescription("ResourcesServiceTest.jar");
        resource.setAlias("ResourcesServiceTest.jar");
        resource.setFullName("/ResourcesServiceTest.jar");
        resource.setType(ResourceType.FILE);
        return resource;
    }

<<<<<<< HEAD
    private Resource getUdfResource() {
=======
    private Resource getUdfResource(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Resource resource = new Resource();
        resource.setUserId(1);
        resource.setDescription("udfTest");
        resource.setAlias("udfTest.jar");
        resource.setFullName("/udfTest.jar");
        resource.setType(ResourceType.UDF);
        return resource;
    }

<<<<<<< HEAD
    private UdfFunc getUdfFunc() {
=======
    private UdfFunc getUdfFunc(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        UdfFunc udfFunc = new UdfFunc();
        udfFunc.setId(1);
        return udfFunc;
    }

<<<<<<< HEAD
    private List<UdfFunc> getUdfFuncList() {
=======
    private List<UdfFunc> getUdfFuncList(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        List<UdfFunc> udfFuncs = new ArrayList<>();
        udfFuncs.add(getUdfFunc());
        return udfFuncs;
    }

<<<<<<< HEAD
    private User getUser() {
=======
    private User getUser(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setId(1);
        user.setTenantId(1);
        return user;
    }
<<<<<<< HEAD

    private List<String> getContent() {
=======
    private List<String> getContent(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<String> contentList = new ArrayList<>();
        contentList.add("test");
        return contentList;
    }
<<<<<<< HEAD

    private List<Map<String, Object>> getResources() {
        List<Map<String, Object>> resources = new ArrayList<>();
        Map<String, Object> resource = new HashMap<>();
        resource.put("id", 1);
        resource.put("resource_ids", "1");
        resources.add(resource);
        return resources;
    }
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}