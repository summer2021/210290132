/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.api.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.ProgramType;
import org.apache.dolphinscheduler.common.enums.TaskType;
<<<<<<< HEAD
import org.apache.dolphinscheduler.common.model.TaskNode;
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.common.process.ResourceInfo;
import org.apache.dolphinscheduler.common.task.datax.DataxParameters;
import org.apache.dolphinscheduler.common.task.dependent.DependentParameters;
import org.apache.dolphinscheduler.common.task.flink.FlinkParameters;
import org.apache.dolphinscheduler.common.task.http.HttpParameters;
import org.apache.dolphinscheduler.common.task.mr.MapReduceParameters;
import org.apache.dolphinscheduler.common.task.procedure.ProcedureParameters;
import org.apache.dolphinscheduler.common.task.python.PythonParameters;
import org.apache.dolphinscheduler.common.task.shell.ShellParameters;
import org.apache.dolphinscheduler.common.task.spark.SparkParameters;
import org.apache.dolphinscheduler.common.task.sql.SqlParameters;
import org.apache.dolphinscheduler.common.task.subprocess.SubProcessParameters;
import org.apache.dolphinscheduler.common.utils.JSONUtils;

import java.util.Map;

<<<<<<< HEAD
import org.junit.Test;

public class CheckUtilsTest {

=======
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckUtilsTest {

    private static final Logger logger = LoggerFactory.getLogger(CheckUtilsTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * check username
     */
    @Test
    public void testCheckUserName() {
<<<<<<< HEAD
        assertTrue(CheckUtils.checkUserName("test01"));
        assertFalse(CheckUtils.checkUserName(null));
=======

        assertTrue(CheckUtils.checkUserName("test01"));

        assertFalse(CheckUtils.checkUserName(null));

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        assertFalse(CheckUtils.checkUserName("test01@abc"));
    }

    /**
     * check email
     */
    @Test
    public void testCheckEmail() {
<<<<<<< HEAD
        assertTrue(CheckUtils.checkEmail("test01@gmail.com"));
=======

        assertTrue(CheckUtils.checkEmail("test01@gmail.com"));

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        assertFalse(CheckUtils.checkEmail("test01@gmail"));
    }

    /**
     * check desc
     */
    @Test
    public void testCheckDesc() {
<<<<<<< HEAD
        Map<String, Object> objectMap = CheckUtils.checkDesc("I am desc");
        Status status = (Status) objectMap.get(Constants.STATUS);
        assertEquals(status.getCode(), Status.SUCCESS.getCode());
=======

        Map<String, Object> objectMap = CheckUtils.checkDesc("I am desc");
        Status status = (Status) objectMap.get(Constants.STATUS);

        assertEquals(status.getCode(),Status.SUCCESS.getCode());

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testCheckOtherParams() {
        assertFalse(CheckUtils.checkOtherParams(null));
        assertFalse(CheckUtils.checkOtherParams(""));
        assertTrue(CheckUtils.checkOtherParams("xxx"));
        assertFalse(CheckUtils.checkOtherParams("{}"));
        assertFalse(CheckUtils.checkOtherParams("{\"key1\":111}"));
    }
<<<<<<< HEAD

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * check passwd
     */
    @Test
    public void testCheckPassword() {
<<<<<<< HEAD
        assertFalse(CheckUtils.checkPassword(null));
        assertFalse(CheckUtils.checkPassword("a"));
        assertFalse(CheckUtils.checkPassword("1234567890abcderfasdf2"));
=======

        assertFalse(CheckUtils.checkPassword(null));

        assertFalse(CheckUtils.checkPassword("a"));

        assertFalse(CheckUtils.checkPassword("1234567890abcderfasdf2"));

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        assertTrue(CheckUtils.checkPassword("123456"));
    }

    /**
     * check phone
     */
    @Test
    public void testCheckPhone() {
<<<<<<< HEAD
        // phone can be null
        assertTrue(CheckUtils.checkPhone(null));
        assertFalse(CheckUtils.checkPhone("14567134578654"));
        assertTrue(CheckUtils.checkPhone("17362537263"));
    }

    @Test
    public void testCheckTaskNodeParameters() {
        TaskNode taskNode = new TaskNode();
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));

        taskNode.setType("unKnown");
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));

        taskNode.setParams("unKnown");
        taskNode.setType("unKnown");
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));

        taskNode.setParams("unKnown");
        taskNode.setType(null);
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));

        // sub SubProcessParameters
        SubProcessParameters subProcessParameters = new SubProcessParameters();
        taskNode.setParams(JSONUtils.toJsonString(subProcessParameters));
        taskNode.setType(TaskType.SUB_PROCESS.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));

        subProcessParameters.setProcessDefinitionId(1234);
        taskNode.setParams(JSONUtils.toJsonString(subProcessParameters));
        taskNode.setType(TaskType.SUB_PROCESS.getDesc());
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // ShellParameters
        ShellParameters shellParameters = new ShellParameters();
        taskNode.setParams(JSONUtils.toJsonString(shellParameters));
        taskNode.setType(TaskType.SHELL.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        shellParameters.setRawScript("");
        taskNode.setParams(JSONUtils.toJsonString(shellParameters));
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        shellParameters.setRawScript("sss");
        taskNode.setParams(JSONUtils.toJsonString(shellParameters));
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // ProcedureParameters
        ProcedureParameters procedureParameters = new ProcedureParameters();
        taskNode.setParams(JSONUtils.toJsonString(procedureParameters));
        taskNode.setType(TaskType.PROCEDURE.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        procedureParameters.setDatasource(1);
        procedureParameters.setType("xx");
        procedureParameters.setMethod("yy");
        taskNode.setParams(JSONUtils.toJsonString(procedureParameters));
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // SqlParameters
        SqlParameters sqlParameters = new SqlParameters();
        taskNode.setParams(JSONUtils.toJsonString(sqlParameters));
        taskNode.setType(TaskType.SQL.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        sqlParameters.setDatasource(1);
        sqlParameters.setType("xx");
        sqlParameters.setSql("yy");
        taskNode.setParams(JSONUtils.toJsonString(sqlParameters));
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // MapReduceParameters
        MapReduceParameters mapreduceParameters = new MapReduceParameters();
        taskNode.setParams(JSONUtils.toJsonString(mapreduceParameters));
        taskNode.setType(TaskType.MR.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
=======

        // phone can be null
        assertTrue(CheckUtils.checkPhone(null));

        assertFalse(CheckUtils.checkPhone("14567134578654"));

        assertTrue(CheckUtils.checkPhone("17362537263"));
    }
    @Test
    public void testCheckTaskNodeParameters() {

        assertFalse(CheckUtils.checkTaskNodeParameters(null,null));
        assertFalse(CheckUtils.checkTaskNodeParameters(null,"unKnown"));
        assertFalse(CheckUtils.checkTaskNodeParameters("unKnown","unKnown"));
        assertFalse(CheckUtils.checkTaskNodeParameters("unKnown",null));

        // sub SubProcessParameters
        SubProcessParameters subProcessParameters = new SubProcessParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(subProcessParameters), TaskType.SUB_PROCESS.toString()));
        subProcessParameters.setProcessDefinitionId(1234);
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(subProcessParameters), TaskType.SUB_PROCESS.toString()));

        // ShellParameters
        ShellParameters shellParameters = new ShellParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(shellParameters), TaskType.SHELL.toString()));
        shellParameters.setRawScript("");
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(shellParameters), TaskType.SHELL.toString()));
        shellParameters.setRawScript("sss");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(shellParameters), TaskType.SHELL.toString()));

        // ProcedureParameters
        ProcedureParameters procedureParameters = new ProcedureParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(procedureParameters), TaskType.PROCEDURE.toString()));
        procedureParameters.setDatasource(1);
        procedureParameters.setType("xx");
        procedureParameters.setMethod("yy");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(procedureParameters), TaskType.PROCEDURE.toString()));

        // SqlParameters
        SqlParameters sqlParameters = new SqlParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(sqlParameters), TaskType.SQL.toString()));
        sqlParameters.setDatasource(1);
        sqlParameters.setType("xx");
        sqlParameters.setSql("yy");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(sqlParameters), TaskType.SQL.toString()));

        // MapReduceParameters
        MapReduceParameters mapreduceParameters = new MapReduceParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(mapreduceParameters), TaskType.MR.toString()));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        ResourceInfo resourceInfoMapreduce = new ResourceInfo();
        resourceInfoMapreduce.setId(1);
        resourceInfoMapreduce.setRes("");
        mapreduceParameters.setMainJar(resourceInfoMapreduce);
        mapreduceParameters.setProgramType(ProgramType.JAVA);
<<<<<<< HEAD
        taskNode.setParams(JSONUtils.toJsonString(mapreduceParameters));
        taskNode.setType(TaskType.MR.getDesc());
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // SparkParameters
        SparkParameters sparkParameters = new SparkParameters();
        taskNode.setParams(JSONUtils.toJsonString(sparkParameters));
        taskNode.setType(TaskType.SPARK.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        sparkParameters.setMainJar(new ResourceInfo());
        sparkParameters.setProgramType(ProgramType.SCALA);
        sparkParameters.setSparkVersion("1.1.1");
        taskNode.setParams(JSONUtils.toJsonString(sparkParameters));
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // PythonParameters
        PythonParameters pythonParameters = new PythonParameters();
        taskNode.setParams(JSONUtils.toJsonString(pythonParameters));
        taskNode.setType(TaskType.PYTHON.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        pythonParameters.setRawScript("ss");
        taskNode.setParams(JSONUtils.toJsonString(pythonParameters));
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // DependentParameters
        DependentParameters dependentParameters = new DependentParameters();
        taskNode.setDependence(JSONUtils.toJsonString(dependentParameters));
        taskNode.setType(TaskType.DEPENDENT.getDesc());
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // FlinkParameters
        FlinkParameters flinkParameters = new FlinkParameters();
        taskNode.setParams(JSONUtils.toJsonString(flinkParameters));
        taskNode.setType(TaskType.FLINK.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        flinkParameters.setMainJar(new ResourceInfo());
        flinkParameters.setProgramType(ProgramType.JAVA);
        taskNode.setParams(JSONUtils.toJsonString(flinkParameters));
        taskNode.setType(TaskType.FLINK.getDesc());
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // HTTP
        HttpParameters httpParameters = new HttpParameters();
        taskNode.setParams(JSONUtils.toJsonString(httpParameters));
        taskNode.setType(TaskType.HTTP.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        httpParameters.setUrl("httpUrl");
        taskNode.setParams(JSONUtils.toJsonString(httpParameters));
        taskNode.setType(TaskType.HTTP.getDesc());
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));

        // DataxParameters
        DataxParameters dataxParameters = new DataxParameters();
        taskNode.setParams(JSONUtils.toJsonString(dataxParameters));
        taskNode.setType(TaskType.DATAX.getDesc());
        assertFalse(CheckUtils.checkTaskNodeParameters(taskNode));
        dataxParameters.setCustomConfig(0);
        dataxParameters.setDataSource(111);
        dataxParameters.setDataTarget(333);
        dataxParameters.setSql(TaskType.SQL.getDesc());
        dataxParameters.setTargetTable("tar");
        taskNode.setParams(JSONUtils.toJsonString(dataxParameters));
        taskNode.setType(TaskType.DATAX.getDesc());
        assertTrue(CheckUtils.checkTaskNodeParameters(taskNode));
    }

}
=======
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(mapreduceParameters), TaskType.MR.toString()));

        // SparkParameters
        SparkParameters sparkParameters = new SparkParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(sparkParameters), TaskType.SPARK.toString()));
        sparkParameters.setMainJar(new ResourceInfo());
        sparkParameters.setProgramType(ProgramType.SCALA);
        sparkParameters.setSparkVersion("1.1.1");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(sparkParameters), TaskType.SPARK.toString()));

        // PythonParameters
        PythonParameters pythonParameters = new PythonParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(pythonParameters), TaskType.PYTHON.toString()));
        pythonParameters.setRawScript("ss");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(pythonParameters), TaskType.PYTHON.toString()));

        // DependentParameters
        DependentParameters dependentParameters = new DependentParameters();
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(dependentParameters), TaskType.DEPENDENT.toString()));

        // FlinkParameters
        FlinkParameters flinkParameters = new FlinkParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(flinkParameters), TaskType.FLINK.toString()));
        flinkParameters.setMainJar(new ResourceInfo());
        flinkParameters.setProgramType(ProgramType.JAVA);
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(flinkParameters), TaskType.FLINK.toString()));

        // HTTP
        HttpParameters httpParameters = new HttpParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(httpParameters), TaskType.HTTP.toString()));
        httpParameters.setUrl("httpUrl");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(httpParameters), TaskType.HTTP.toString()));

        // DataxParameters
        DataxParameters dataxParameters = new DataxParameters();
        assertFalse(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(dataxParameters), TaskType.DATAX.toString()));
        dataxParameters.setCustomConfig(0);
        dataxParameters.setDataSource(111);
        dataxParameters.setDataTarget(333);
        dataxParameters.setSql("sql");
        dataxParameters.setTargetTable("tar");
        assertTrue(CheckUtils.checkTaskNodeParameters(JSONUtils.toJsonString(dataxParameters), TaskType.DATAX.toString()));
    }

}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
