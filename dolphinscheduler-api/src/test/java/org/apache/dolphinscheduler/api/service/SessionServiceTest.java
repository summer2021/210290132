/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.service.impl.SessionServiceImpl;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.dao.entity.Session;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.SessionMapper;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

=======
package org.apache.dolphinscheduler.api.service;

import java.util.Calendar;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.common.utils.StringUtils;
import org.apache.dolphinscheduler.dao.entity.Session;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.SessionMapper;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockCookie;
import org.springframework.mock.web.MockHttpServletRequest;

<<<<<<< HEAD
/**
 * session service test
 */
=======
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RunWith(MockitoJUnitRunner.class)
public class SessionServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(SessionServiceTest.class);

    @InjectMocks
<<<<<<< HEAD
    private SessionServiceImpl sessionService;
=======
    private SessionService sessionService;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    @Mock
    private SessionMapper sessionMapper;

<<<<<<< HEAD
    private String sessionId = "aaaaaaaaaaaaaaaaaa";
=======
    private String sessionId ="aaaaaaaaaaaaaaaaaa";
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    @Before
    public void setUp() {
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @After
    public void after(){
    }

    /**
     * create session
     */
    @Test
<<<<<<< HEAD
    public void testGetSession() {
=======
    public void testGetSession(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Mockito.when(sessionMapper.selectById(sessionId)).thenReturn(getSession());
        // get sessionId from  header
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.addHeader(Constants.SESSION_ID,sessionId);
        mockHttpServletRequest.addHeader("HTTP_X_FORWARDED_FOR","127.0.0.1");
        //query
        Session session = sessionService.getSession(mockHttpServletRequest);
        Assert.assertNotNull(session);
        logger.info("session ip {}",session.getIp());

        // get sessionId from cookie
        mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.addHeader("HTTP_X_FORWARDED_FOR","127.0.0.1");
        MockCookie mockCookie = new MockCookie(Constants.SESSION_ID,sessionId);
        mockHttpServletRequest.setCookies(mockCookie);
        //query
        session = sessionService.getSession(mockHttpServletRequest);
        Assert.assertNotNull(session);
        logger.info("session ip {}",session.getIp());
        Assert.assertEquals(session.getIp(),"127.0.0.1");
<<<<<<< HEAD
=======


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * create session
     */
    @Test
<<<<<<< HEAD
    public void testCreateSession() {
=======
    public void testCreateSession(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String ip = "127.0.0.1";
        User user = new User();
        user.setUserType(UserType.GENERAL_USER);
        user.setId(1);
        Mockito.when(sessionMapper.queryByUserId(1)).thenReturn(getSessions());
        String sessionId = sessionService.createSession(user, ip);
<<<<<<< HEAD
        logger.info("createSessionId is " + sessionId);
        Assert.assertTrue(!StringUtils.isEmpty(sessionId));
    }

=======
        logger.info("createSessionId is "+sessionId);
        Assert.assertTrue(StringUtils.isNotEmpty(sessionId));
    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * sign out
     * remove ip restrictions
     */
    @Test
<<<<<<< HEAD
    public void testSignOut() {
=======
    public void testSignOut(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        int userId = 88888888;
        String ip = "127.0.0.1";
        User user = new User();
        user.setId(userId);

        Mockito.when(sessionMapper.queryByUserIdAndIp(userId,ip)).thenReturn(getSession());

<<<<<<< HEAD
        sessionService.signOut(ip,user);

    }

    private Session getSession() {
=======
        sessionService.signOut(ip ,user);

    }

    private Session getSession(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Session session = new Session();
        session.setId(sessionId);
        session.setIp("127.0.0.1");
        session.setLastLoginTime(DateUtils.add(new Date(), Calendar.DAY_OF_MONTH, 40));
        session.setUserId(1);
        return session;
    }

<<<<<<< HEAD
    private List<Session> getSessions() {
        List<Session> sessionList = new ArrayList<>();
        sessionList.add(getSession());
        return sessionList;
    }
}
=======
    private List<Session> getSessions(){
        List<Session> sessionList = new ArrayList<>();
       sessionList.add(getSession());
        return sessionList;
    }


}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
