/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.dolphinscheduler.api.controller;

import org.apache.dolphinscheduler.api.enums.Status;
<<<<<<< HEAD
import org.apache.dolphinscheduler.api.service.impl.ProcessDefinitionServiceImpl;
=======
import org.apache.dolphinscheduler.api.service.ProcessDefinitionService;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.utils.PageInfo;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.ReleaseState;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.dao.entity.ProcessDefinition;
<<<<<<< HEAD
import org.apache.dolphinscheduler.dao.entity.ProcessDefinitionLog;
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.dao.entity.Resource;
import org.apache.dolphinscheduler.dao.entity.User;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
<<<<<<< HEAD
=======
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.springframework.mock.web.MockHttpServletResponse;

/**
 * process definition controller test
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class ProcessDefinitionControllerTest {

<<<<<<< HEAD
=======
    private static Logger logger = LoggerFactory.getLogger(ProcessDefinitionControllerTest.class);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @InjectMocks
    private ProcessDefinitionController processDefinitionController;

    @Mock
<<<<<<< HEAD
    private ProcessDefinitionServiceImpl processDefinitionService;
=======
    private ProcessDefinitionService processDefinitionService;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    protected User user;

    @Before
    public void before() {
        User loginUser = new User();
        loginUser.setId(1);
        loginUser.setUserType(UserType.GENERAL_USER);
        loginUser.setUserName("admin");
<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        user = loginUser;
    }

    @Test
<<<<<<< HEAD
    public void testCreateProcessDefinition() {
        String relationJson = "[{\"name\":\"\",\"pre_task_code\":0,\"pre_task_version\":0,\"post_task_code\":123456789,\"post_task_version\":1,"
                + "\"condition_type\":0,\"condition_params\":\"{}\"},{\"name\":\"\",\"pre_task_code\":123456789,\"pre_task_version\":1,"
                + "\"post_task_code\":123451234,\"post_task_version\":1,\"condition_type\":0,\"condition_params\":\"{}\"}]";
        String taskDefinitionJson = "[{\"name\":\"detail_up\",\"description\":\"\",\"taskType\":\"SHELL\",\"taskParams\":"
            + "\"{\\\"resourceList\\\":[],\\\"localParams\\\":[{\\\"prop\\\":\\\"datetime\\\",\\\"direct\\\":\\\"IN\\\","
            + "\\\"type\\\":\\\"VARCHAR\\\",\\\"value\\\":\\\"${system.datetime}\\\"}],\\\"rawScript\\\":"
            + "\\\"echo ${datetime}\\\",\\\"conditionResult\\\":\\\"{\\\\\\\"successNode\\\\\\\":[\\\\\\\"\\\\\\\"],"
            + "\\\\\\\"failedNode\\\\\\\":[\\\\\\\"\\\\\\\"]}\\\",\\\"dependence\\\":{}}\",\"flag\":0,\"taskPriority\":0,"
            + "\"workerGroup\":\"default\",\"failRetryTimes\":0,\"failRetryInterval\":0,\"timeoutFlag\":0,"
            + "\"timeoutNotifyStrategy\":0,\"timeout\":0,\"delayTime\":0,\"resourceIds\":\"\"}]";
        long projectCode = 1L;
        String name = "dag_test";
        String description = "desc test";
        String globalParams = "[]";
        String locations = "[]";
        int timeout = 0;
        String tenantCode = "root";
        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);
        result.put(Constants.DATA_LIST, 1);

        Mockito.when(processDefinitionService.createProcessDefinition(user, projectCode, name, description, globalParams,
                locations, timeout, tenantCode, relationJson, taskDefinitionJson)).thenReturn(result);

        Result response = processDefinitionController.createProcessDefinition(user, projectCode, name, description, globalParams,
                locations, timeout, tenantCode, relationJson, taskDefinitionJson);
        Assert.assertEquals(Status.SUCCESS.getCode(), response.getCode().intValue());
=======
    public void testCreateProcessDefinition() throws Exception {
        String json = "{\"globalParams\":[],"
                + "\"tasks\":["
                + "{\"type\":\"SHELL\",\"id\":\"tasks-36196\",\"name\":\"ssh_test1\",\"params\":{\"resourceList\":[],"
                + "\"localParams\":[],\"rawScript\":\"aa=\\\"1234\\\"\\necho ${aa}\"},\"desc\":\"\",\"runFlag\":\"NORMAL\",\"dependence\":{},"
                + "\"maxRetryTimes\":\"0\",\"retryInterval\":\"1\",\"timeout\":{\"strategy\":\"\",\"interval\":null,\"enable\":false},"
                + "\"taskInstancePriority\":\"MEDIUM\",\"workerGroupId\":-1,\"preTasks\":[]}]"
                + ",\"tenantId\":-1,\"timeout\":0}";
        String locations = "{\"tasks-36196\":{\"name\":\"ssh_test1\",\"targetarr\":\"\",\"x\":141,\"y\":70}}";

        String projectName = "test";
        String name = "dag_test";
        String description = "desc test";
        String connects = "[]";
        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);
        result.put("processDefinitionId",1);

        Mockito.when(processDefinitionService.createProcessDefinition(user, projectName, name, json,
                description, locations, connects)).thenReturn(result);

        Result response = processDefinitionController.createProcessDefinition(user, projectName, name, json,
                locations, connects, description);
        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    private void putMsg(Map<String, Object> result, Status status, Object... statusParams) {
        result.put(Constants.STATUS, status);
        if (statusParams != null && statusParams.length > 0) {
            result.put(Constants.MSG, MessageFormat.format(status.getMsg(), statusParams));
        } else {
            result.put(Constants.MSG, status.getMsg());
        }
    }

<<<<<<< HEAD
    public void putMsg(Result result, Status status, Object... statusParams) {
        result.setCode(status.getCode());
        if (statusParams != null && statusParams.length > 0) {
            result.setMsg(MessageFormat.format(status.getMsg(), statusParams));
        } else {
            result.setMsg(status.getMsg());
        }
    }

    @Test
    public void testVerifyProcessDefinitionName() {
        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.PROCESS_DEFINITION_NAME_EXIST);
        long projectCode = 1L;
        String name = "dag_test";

        Mockito.when(processDefinitionService.verifyProcessDefinitionName(user, projectCode, name)).thenReturn(result);

        Result response = processDefinitionController.verifyProcessDefinitionName(user, projectCode, name);
        Assert.assertTrue(response.isStatus(Status.PROCESS_DEFINITION_NAME_EXIST));
    }

    @Test
    public void updateProcessDefinition() {
        String relationJson = "[{\"name\":\"\",\"pre_task_code\":0,\"pre_task_version\":0,\"post_task_code\":123456789,\"post_task_version\":1,"
            + "\"condition_type\":0,\"condition_params\":\"{}\"},{\"name\":\"\",\"pre_task_code\":123456789,\"pre_task_version\":1,"
            + "\"post_task_code\":123451234,\"post_task_version\":1,\"condition_type\":0,\"condition_params\":\"{}\"}]";
        String taskDefinitionJson = "[{\"name\":\"detail_up\",\"description\":\"\",\"taskType\":\"SHELL\",\"taskParams\":"
            + "\"{\\\"resourceList\\\":[],\\\"localParams\\\":[{\\\"prop\\\":\\\"datetime\\\",\\\"direct\\\":\\\"IN\\\","
            + "\\\"type\\\":\\\"VARCHAR\\\",\\\"value\\\":\\\"${system.datetime}\\\"}],\\\"rawScript\\\":"
            + "\\\"echo ${datetime}\\\",\\\"conditionResult\\\":\\\"{\\\\\\\"successNode\\\\\\\":[\\\\\\\"\\\\\\\"],"
            + "\\\\\\\"failedNode\\\\\\\":[\\\\\\\"\\\\\\\"]}\\\",\\\"dependence\\\":{}}\",\"flag\":0,\"taskPriority\":0,"
            + "\"workerGroup\":\"default\",\"failRetryTimes\":0,\"failRetryInterval\":0,\"timeoutFlag\":0,"
            + "\"timeoutNotifyStrategy\":0,\"timeout\":0,\"delayTime\":0,\"resourceIds\":\"\"}]";
        String locations = "{\"tasks-36196\":{\"name\":\"ssh_test1\",\"targetarr\":\"\",\"x\":141,\"y\":70}}";
        long projectCode = 1L;
        String name = "dag_test";
        String description = "desc test";
        String globalParams = "[]";
        int timeout = 0;
        String tenantCode = "root";
        long code = 123L;
        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);
        result.put("processDefinitionId", 1);

        Mockito.when(processDefinitionService.updateProcessDefinition(user, projectCode, name, code, description, globalParams,
                locations, timeout, tenantCode, relationJson, taskDefinitionJson)).thenReturn(result);

        Result response = processDefinitionController.updateProcessDefinition(user, projectCode, name, code, description, globalParams,
                locations, timeout, tenantCode, relationJson, taskDefinitionJson, ReleaseState.OFFLINE);
        Assert.assertEquals(Status.SUCCESS.getCode(), response.getCode().intValue());
    }

    @Test
    public void testReleaseProcessDefinition() {
        long projectCode = 1L;
        int id = 1;
        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.releaseProcessDefinition(user, projectCode, id, ReleaseState.OFFLINE)).thenReturn(result);
        Result response = processDefinitionController.releaseProcessDefinition(user, projectCode, id, ReleaseState.OFFLINE);
        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testQueryProcessDefinitionByCode() {
        String locations = "{\"tasks-36196\":{\"name\":\"ssh_test1\",\"targetarr\":\"\",\"x\":141,\"y\":70}}";
        long projectCode = 1L;
        String name = "dag_test";
        String description = "desc test";
        long code = 1L;

        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setProjectCode(projectCode);
        processDefinition.setDescription(description);
        processDefinition.setCode(code);
        processDefinition.setLocations(locations);
        processDefinition.setName(name);

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);
        result.put(Constants.DATA_LIST, processDefinition);

        Mockito.when(processDefinitionService.queryProcessDefinitionByCode(user, projectCode, code)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionByCode(user, projectCode, code);

        Assert.assertEquals(Status.SUCCESS.getCode(), response.getCode().intValue());
    }

    @Test
    public void testBatchCopyProcessDefinition() {
        long projectCode = 1L;
        long targetProjectCode = 2L;
        String code = "1";

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.batchCopyProcessDefinition(user, projectCode, code, targetProjectCode)).thenReturn(result);
        Result response = processDefinitionController.copyProcessDefinition(user, projectCode, code, targetProjectCode);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testBatchMoveProcessDefinition() {
        long projectCode = 1L;
        long targetProjectCode = 2L;
        String id = "1";

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.batchMoveProcessDefinition(user, projectCode, id, targetProjectCode)).thenReturn(result);
        Result response = processDefinitionController.moveProcessDefinition(user, projectCode, id, targetProjectCode);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testQueryProcessDefinitionList() {
        long projectCode = 1L;
        List<ProcessDefinition> resourceList = getDefinitionList();

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);
        result.put(Constants.DATA_LIST, resourceList);

        Mockito.when(processDefinitionService.queryProcessDefinitionList(user, projectCode)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionList(user, projectCode);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    public List<ProcessDefinition> getDefinitionList() {
        List<ProcessDefinition> resourceList = new ArrayList<>();
=======
    @Test
    public void testVerifyProcessDefinitionName() throws Exception {

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.VERIFY_PROCESS_DEFINITION_NAME_UNIQUE_ERROR);
        String projectName = "test";
        String name = "dag_test";

        Mockito.when(processDefinitionService.verifyProcessDefinitionName(user,projectName,name)).thenReturn(result);

        Result response = processDefinitionController.verifyProcessDefinitionName(user,projectName,name);
        Assert.assertEquals(Status.VERIFY_PROCESS_DEFINITION_NAME_UNIQUE_ERROR.getCode(), response.getCode().intValue());

    }

    @Test
    public void updateProcessDefinition() throws Exception {

        String json = "{\"globalParams\":[],"
                + "\"tasks\":[{\"type\":\"SHELL\",\"id\":\"tasks-36196\",\"name\":\"ssh_test1\","
                + "\"params\":{\"resourceList\":[],\"localParams\":[],"
                + "\"rawScript\":\"aa=\\\"1234\\\"\\necho ${aa}\"},\"desc\":\"\",\"runFlag\":\"NORMAL\",\"dependence\":{},"
                + "\"maxRetryTimes\":\"0\",\"retryInterval\":\"1\","
                + "\"timeout\":{\"strategy\":\"\",\"interval\":null,\"enable\":false},"
                + "\"taskInstancePriority\":\"MEDIUM\",\"workerGroupId\":-1,\"preTasks\":[]}],\"tenantId\":-1,\"timeout\":0}";
        String locations = "{\"tasks-36196\":{\"name\":\"ssh_test1\",\"targetarr\":\"\",\"x\":141,\"y\":70}}";
        String projectName = "test";
        String name = "dag_test";
        String description = "desc test";
        String connects = "[]";
        int id = 1;
        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);
        result.put("processDefinitionId",1);

        Mockito.when(processDefinitionService.updateProcessDefinition(user, projectName, id,name, json,
                description, locations, connects)).thenReturn(result);

        Result response = processDefinitionController.updateProcessDefinition(user, projectName, name,id, json,
                locations, connects, description);
        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testReleaseProcessDefinition() throws Exception {
        String projectName = "test";
        int id = 1;
        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.releaseProcessDefinition(user, projectName,id,ReleaseState.OFFLINE.ordinal())).thenReturn(result);
        Result response = processDefinitionController.releaseProcessDefinition(user, projectName,id,ReleaseState.OFFLINE.ordinal());
        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testQueryProcessDefinitionById() throws Exception {

        String json = "{\"globalParams\":[],"
                + "\"tasks\":[{\"type\":\"SHELL\",\"id\":\"tasks-36196\","
                + "\"name\":\"ssh_test1\","
                + "\"params\":{\"resourceList\":[],\"localParams\":[],"
                + "\"rawScript\":\"aa=\\\"1234\\\"\\necho ${aa}\"},\"desc\":\"\",\"runFlag\":\"NORMAL\",\"dependence\":{},"
                + "\"maxRetryTimes\":\"0\",\"retryInterval\":\"1\",\"timeout\":{\"strategy\":\"\",\"interval\":null,\"enable\":false},"
                + "\"taskInstancePriority\":\"MEDIUM\",\"workerGroupId\":-1,\"preTasks\":[]}],\"tenantId\":-1,\"timeout\":0}";
        String locations = "{\"tasks-36196\":{\"name\":\"ssh_test1\",\"targetarr\":\"\",\"x\":141,\"y\":70}}";
        String projectName = "test";
        String name = "dag_test";
        String description = "desc test";
        String connects = "[]";
        int id = 1;

        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setProjectName(projectName);
        processDefinition.setConnects(connects);
        processDefinition.setDescription(description);
        processDefinition.setId(id);
        processDefinition.setLocations(locations);
        processDefinition.setName(name);
        processDefinition.setProcessDefinitionJson(json);

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);
        result.put(Constants.DATA_LIST, processDefinition);

        Mockito.when(processDefinitionService.queryProcessDefinitionById(user, projectName,id)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionById(user, projectName,id);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testCopyProcessDefinition() throws Exception {

        String projectName = "test";
        int id = 1;

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.copyProcessDefinition(user, projectName,id)).thenReturn(result);
        Result response = processDefinitionController.copyProcessDefinition(user, projectName,id);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testQueryProcessDefinitionList() throws Exception {

        String projectName = "test";
        List<ProcessDefinition> resourceList =  getDefinitionList();

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);
        result.put(Constants.DATA_LIST, resourceList);

        Mockito.when(processDefinitionService.queryProcessDefinitionList(user, projectName)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionList(user, projectName);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    public List<ProcessDefinition> getDefinitionList() {

        List<ProcessDefinition> resourceList = new ArrayList<>();

        String json = "{\"globalParams\":[],"
                + "\"tasks\":[{\"type\":\"SHELL\",\"id\":\"tasks-36196\","
                + "\"name\":\"ssh_test1\","
                + "\"params\":{\"resourceList\":[],\"localParams\":[],"
                + "\"rawScript\":\"aa=\\\"1234\\\"\\necho ${aa}\"},\"desc\":\"\",\"runFlag\":\"NORMAL\",\"dependence\":{},"
                + "\"maxRetryTimes\":\"0\",\"retryInterval\":\"1\",\"timeout\":{\"strategy\":\"\",\"interval\":null,\"enable\":false},"
                + "\"taskInstancePriority\":\"MEDIUM\",\"workerGroupId\":-1,\""
                + "preTasks\":[]}],\"tenantId\":-1,\"timeout\":0}";
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String locations = "{\"tasks-36196\":{\"name\":\"ssh_test1\",\"targetarr\":\"\",\"x\":141,\"y\":70}}";
        String projectName = "test";
        String name = "dag_test";
        String description = "desc test";
<<<<<<< HEAD
=======
        String connects = "[]";
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        int id = 1;

        ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setProjectName(projectName);
<<<<<<< HEAD
=======
        processDefinition.setConnects(connects);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        processDefinition.setDescription(description);
        processDefinition.setId(id);
        processDefinition.setLocations(locations);
        processDefinition.setName(name);
<<<<<<< HEAD
=======
        processDefinition.setProcessDefinitionJson(json);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        String name2 = "dag_test";
        int id2 = 2;

        ProcessDefinition processDefinition2 = new ProcessDefinition();
        processDefinition2.setProjectName(projectName);
<<<<<<< HEAD
=======
        processDefinition2.setConnects(connects);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        processDefinition2.setDescription(description);
        processDefinition2.setId(id2);
        processDefinition2.setLocations(locations);
        processDefinition2.setName(name2);
<<<<<<< HEAD
=======
        processDefinition2.setProcessDefinitionJson(json);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        resourceList.add(processDefinition);
        resourceList.add(processDefinition2);

<<<<<<< HEAD
        return resourceList;
    }

    @Test
    public void testDeleteProcessDefinitionByCode() {
        long projectCode = 1L;
        long code = 1L;

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.deleteProcessDefinitionByCode(user, projectCode, code)).thenReturn(result);
        Result response = processDefinitionController.deleteProcessDefinitionByCode(user, projectCode, code);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testGetNodeListByDefinitionId() {
        long projectCode = 1L;
        Long code = 1L;

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.getTaskNodeListByDefinitionCode(user, projectCode, code)).thenReturn(result);
        Result response = processDefinitionController.getNodeListByDefinitionCode(user, projectCode, code);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testGetNodeListByDefinitionIdList() {
        long projectCode = 1L;
        String codeList = "1,2,3";

        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.getNodeListMapByDefinitionCodes(user, projectCode, codeList)).thenReturn(result);
        Result response = processDefinitionController.getNodeListMapByDefinitionCodes(user, projectCode, codeList);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testQueryProcessDefinitionAllByProjectId() {
        long projectCode = 1L;
        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.queryAllProcessDefinitionByProjectCode(user, projectCode)).thenReturn(result);
        Result response = processDefinitionController.queryAllProcessDefinitionByProjectCode(user, projectCode);

        Assert.assertTrue(response != null && response.isSuccess());
=======
        return  resourceList;
    }

    @Test
    public void testDeleteProcessDefinitionById() throws Exception {
        String projectName = "test";
        int id = 1;

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.deleteProcessDefinitionById(user, projectName,id)).thenReturn(result);
        Result response = processDefinitionController.deleteProcessDefinitionById(user, projectName,id);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testGetNodeListByDefinitionId() throws Exception {
        String projectName = "test";
        int id = 1;

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.getTaskNodeListByDefinitionId(id)).thenReturn(result);
        Result response = processDefinitionController.getNodeListByDefinitionId(user,projectName,id);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testGetNodeListByDefinitionIdList() throws Exception {
        String projectName = "test";
        String idList = "1,2,3";

        Map<String, Object> result = new HashMap<>(5);
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.getTaskNodeListByDefinitionIdList(idList)).thenReturn(result);
        Result response = processDefinitionController.getNodeListByDefinitionIdList(user,projectName,idList);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testQueryProcessDefinitionAllByProjectId() throws Exception {
        int projectId = 1;
        Map<String,Object> result = new HashMap<>();
        putMsg(result,Status.SUCCESS);

        Mockito.when(processDefinitionService.queryProcessDefinitionAllByProjectId(projectId)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionAllByProjectId(user,projectId);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testViewTree() throws Exception {
<<<<<<< HEAD
        long projectCode = 1L;
        int processId = 1;
        int limit = 2;
        Map<String, Object> result = new HashMap<>();
        putMsg(result, Status.SUCCESS);

        Mockito.when(processDefinitionService.viewTree(processId, limit)).thenReturn(result);
        Result response = processDefinitionController.viewTree(user, projectCode, processId, limit);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testQueryProcessDefinitionListPaging() {
        long projectCode = 1L;
=======
        String projectName = "test";
        int processId = 1;
        int limit = 2;
        Map<String,Object> result = new HashMap<>();
        putMsg(result,Status.SUCCESS);

        Mockito.when(processDefinitionService.viewTree(processId,limit)).thenReturn(result);
        Result response = processDefinitionController.viewTree(user,projectName,processId,limit);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testQueryProcessDefinitionListPaging() throws Exception {
        String projectName = "test";
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        int pageNo = 1;
        int pageSize = 10;
        String searchVal = "";
        int userId = 1;

<<<<<<< HEAD
        Result result = new Result();
        putMsg(result, Status.SUCCESS);
        result.setData(new PageInfo<Resource>(1, 10));

        Mockito.when(processDefinitionService.queryProcessDefinitionListPaging(user, projectCode, searchVal, userId, pageNo, pageSize)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionListPaging(user, projectCode, searchVal, userId, pageNo, pageSize);

        Assert.assertTrue(response != null && response.isSuccess());
    }

    @Test
    public void testBatchExportProcessDefinitionByCodes() {
        String processDefinitionIds = "1,2";
        long projectCode = 1L;
        HttpServletResponse response = new MockHttpServletResponse();
        Mockito.doNothing().when(this.processDefinitionService).batchExportProcessDefinitionByCodes(user, projectCode, processDefinitionIds, response);
        processDefinitionController.batchExportProcessDefinitionByCodes(user, projectCode, processDefinitionIds, response);
    }

    @Test
    public void testQueryProcessDefinitionVersions() {

        long projectCode = 1L;
        Result resultMap = new Result();
        putMsg(resultMap, Status.SUCCESS);
        resultMap.setData(new PageInfo<ProcessDefinitionLog>(1, 10));
        Mockito.when(processDefinitionService.queryProcessDefinitionVersions(
                user
                , projectCode
                , 1
                , 10
                , 1))
                .thenReturn(resultMap);
        Result result = processDefinitionController.queryProcessDefinitionVersions(
                user
                , projectCode
                , 1
                , 10
                , 1);

        Assert.assertEquals(Status.SUCCESS.getCode(), (int) result.getCode());
    }

    @Test
    public void testSwitchProcessDefinitionVersion() {
        long projectCode = 1L;
        Map<String, Object> resultMap = new HashMap<>();
        putMsg(resultMap, Status.SUCCESS);
        Mockito.when(processDefinitionService.switchProcessDefinitionVersion(user, projectCode, 1, 10)).thenReturn(resultMap);
        Result result = processDefinitionController.switchProcessDefinitionVersion(user, projectCode, 1, 10);

        Assert.assertEquals(Status.SUCCESS.getCode(), (int) result.getCode());
    }

    @Test
    public void testDeleteProcessDefinitionVersion() {
        long projectCode = 1L;
        Map<String, Object> resultMap = new HashMap<>();
        putMsg(resultMap, Status.SUCCESS);
        Mockito.when(processDefinitionService.deleteProcessDefinitionVersion(
                user, projectCode, 1, 10)).thenReturn(resultMap);
        Result result = processDefinitionController.deleteProcessDefinitionVersion(
                user, projectCode, 1, 10);
        Assert.assertEquals(Status.SUCCESS.getCode(), (int) result.getCode());
=======
        Map<String,Object> result = new HashMap<>();
        putMsg(result,Status.SUCCESS);
        result.put(Constants.DATA_LIST,new PageInfo<Resource>(1,10));

        Mockito.when(processDefinitionService.queryProcessDefinitionListPaging(user,projectName, searchVal, pageNo, pageSize, userId)).thenReturn(result);
        Result response = processDefinitionController.queryProcessDefinitionListPaging(user,projectName,pageNo,searchVal,userId,pageSize);

        Assert.assertEquals(Status.SUCCESS.getCode(),response.getCode().intValue());
    }

    @Test
    public void testBatchExportProcessDefinitionByIds() throws Exception {

        String processDefinitionIds = "1,2";
        String projectName = "test";
        HttpServletResponse response = new MockHttpServletResponse();
        ProcessDefinitionService service = new ProcessDefinitionService();
        ProcessDefinitionService spy = Mockito.spy(service);
        Mockito.doNothing().when(spy).batchExportProcessDefinitionByIds(user, projectName, processDefinitionIds, response);
        processDefinitionController.batchExportProcessDefinitionByIds(user, projectName, processDefinitionIds, response);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

}
