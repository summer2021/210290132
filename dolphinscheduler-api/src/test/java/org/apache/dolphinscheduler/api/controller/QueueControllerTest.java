/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.utils.JSONUtils;

=======
package org.apache.dolphinscheduler.api.controller;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

<<<<<<< HEAD
/**
 * queue controller test
 */
public class QueueControllerTest extends AbstractControllerTest {

    private static Logger logger = LoggerFactory.getLogger(QueueControllerTest.class);

    private static final String QUEUE_CREATE_STRING = "queue1";

    @Test
    public void testQueryList() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/queues/list")
=======
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * queue controller test
 */
public class QueueControllerTest extends AbstractControllerTest{

    private static Logger logger = LoggerFactory.getLogger(QueueControllerTest.class);

    @Test
    public void testQueryList() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/queue/list")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
<<<<<<< HEAD
        logger.info("query list queue return result:{}", mvcResult.getResponse().getContentAsString());
=======
        logger.info(mvcResult.getResponse().getContentAsString());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testQueryQueueListPaging() throws Exception {

        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
<<<<<<< HEAD
=======
        //paramsMap.add("processInstanceId","1380");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        paramsMap.add("searchVal","");
        paramsMap.add("pageNo","1");
        paramsMap.add("pageSize","20");

<<<<<<< HEAD
        MvcResult mvcResult = mockMvc.perform(get("/queues")
=======
        MvcResult mvcResult = mockMvc.perform(get("/queue/list-paging")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
<<<<<<< HEAD
        logger.info("query list-paging queue return result:{}", mvcResult.getResponse().getContentAsString());

    }

=======
        logger.info(mvcResult.getResponse().getContentAsString());
    }




>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Test
    public void testCreateQueue() throws Exception {

        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
<<<<<<< HEAD
        paramsMap.add("queue", QUEUE_CREATE_STRING);
        paramsMap.add("queueName","root.queue1");

        MvcResult mvcResult = mockMvc.perform(post("/queues")
=======
        paramsMap.add("queue","ait");
        paramsMap.add("queueName","ait");

        MvcResult mvcResult = mockMvc.perform(post("/queue/create")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertEquals(Status.SUCCESS.getCode(), result.getCode().intValue());
        logger.info("create queue return result:{}", mvcResult.getResponse().getContentAsString());
=======
//        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testUpdateQueue() throws Exception {

        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
<<<<<<< HEAD
        paramsMap.add("id","1");
        paramsMap.add("queue","queue2");
        paramsMap.add("queueName","root.queue2");

        MvcResult mvcResult = mockMvc.perform(put("/queues/{id}", 1)
=======
        paramsMap.add("id","2");
        paramsMap.add("queue","ait12");
        paramsMap.add("queueName","aitName");

        MvcResult mvcResult = mockMvc.perform(post("/queue/update")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info("update queue return result:{}", mvcResult.getResponse().getContentAsString());
=======
        //Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testVerifyQueue() throws Exception {

<<<<<<< HEAD
        // queue value exist
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("queue",QUEUE_CREATE_STRING);
        paramsMap.add("queueName","queue.name");

        MvcResult mvcResult = mockMvc.perform(post("/queues/verify")
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.QUEUE_VALUE_EXIST.getCode(),result.getCode().intValue());

        // success
        paramsMap.clear();
        paramsMap.add("queue","ait123");
        paramsMap.add("queueName","aitName");

        mvcResult = mockMvc.perform(post("/queues/verify")
=======
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("queue","ait123");
        paramsMap.add("queueName","aitName");

        MvcResult mvcResult = mockMvc.perform(post("/queue/verify-queue")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
<<<<<<< HEAD
        result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());
        logger.info("verify queue return result:{}", mvcResult.getResponse().getContentAsString());
=======
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        //Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }
}
