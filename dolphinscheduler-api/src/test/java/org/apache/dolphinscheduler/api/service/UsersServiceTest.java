/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.service.impl.UsersServiceImpl;
=======
package org.apache.dolphinscheduler.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dolphinscheduler.api.enums.Status;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.utils.PageInfo;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.ResourceType;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.CollectionUtils;
import org.apache.dolphinscheduler.common.utils.EncryptionUtils;
<<<<<<< HEAD
import org.apache.dolphinscheduler.dao.entity.AlertGroup;
import org.apache.dolphinscheduler.dao.entity.Project;
import org.apache.dolphinscheduler.dao.entity.Resource;
import org.apache.dolphinscheduler.dao.entity.Tenant;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.AccessTokenMapper;
import org.apache.dolphinscheduler.dao.mapper.AlertGroupMapper;
import org.apache.dolphinscheduler.dao.mapper.DataSourceUserMapper;
import org.apache.dolphinscheduler.dao.mapper.ProjectMapper;
import org.apache.dolphinscheduler.dao.mapper.ProjectUserMapper;
import org.apache.dolphinscheduler.dao.mapper.ResourceMapper;
import org.apache.dolphinscheduler.dao.mapper.ResourceUserMapper;
import org.apache.dolphinscheduler.dao.mapper.TenantMapper;
import org.apache.dolphinscheduler.dao.mapper.UDFUserMapper;
import org.apache.dolphinscheduler.dao.mapper.UserMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

=======
import org.apache.dolphinscheduler.dao.entity.Resource;
import org.apache.dolphinscheduler.dao.entity.Tenant;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;

/**
 * users service test
 */
@RunWith(MockitoJUnitRunner.class)
public class UsersServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(UsersServiceTest.class);

    @InjectMocks
    private UsersServiceImpl usersService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private AccessTokenMapper accessTokenMapper;

    @Mock
    private TenantMapper tenantMapper;

    @Mock
    private ResourceMapper resourceMapper;

    @Mock
    private AlertGroupMapper alertGroupMapper;

    @Mock
    private DataSourceUserMapper datasourceUserMapper;

    @Mock
    private ProjectUserMapper projectUserMapper;

    @Mock
    private ResourceUserMapper resourceUserMapper;

    @Mock
    private UDFUserMapper udfUserMapper;

    @Mock
    private ProjectMapper projectMapper;

    private String queueName = "UsersServiceTestQueue";

    @Before
    public void before() {
    }

    @After
    public void after() {

    }

    @Test
    public void testCreateUserForLdap() {
        String userName = "user1";
        String email = "user1@ldap.com";
        User user = usersService.createUser(UserType.ADMIN_USER, userName, email);
        Assert.assertNotNull(user);
    }

    @Test
    public void testCreateUser() {
=======
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(UsersServiceTest.class);

    @InjectMocks
    private UsersService usersService;
    @Mock
    private UserMapper userMapper;
    @Mock
    private TenantMapper tenantMapper;
    @Mock
    private ProjectUserMapper projectUserMapper;
    @Mock
    private ResourceUserMapper resourcesUserMapper;
    @Mock
    private UDFUserMapper udfUserMapper;
    @Mock
    private DataSourceUserMapper datasourceUserMapper;
    @Mock
    private AlertGroupMapper alertGroupMapper;
    @Mock
    private ResourceMapper resourceMapper;

    private String queueName ="UsersServiceTestQueue";


    @Before
    public void before(){


    }
    @After
    public  void after(){

    }


    @Test
    public void testCreateUser(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserType(UserType.ADMIN_USER);
        String userName = "userTest0001~";
        String userPassword = "userTest";
        String email = "123@qq.com";
        int tenantId = Integer.MAX_VALUE;
<<<<<<< HEAD
        String phone = "13456432345";
        int state = 1;
        try {
            //userName error
            Map<String, Object> result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName, state);
=======
        String phone= "13456432345";
        try {
            //userName error
            Map<String, Object> result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            userName = "userTest0001";
            userPassword = "userTest000111111111111111";
            //password error
<<<<<<< HEAD
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName, state);
=======
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            userPassword = "userTest0001";
            email = "1q.com";
            //email error
<<<<<<< HEAD
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName, state);
=======
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            email = "122222@qq.com";
<<<<<<< HEAD
            phone = "2233";
            //phone error
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName, state);
=======
            phone ="2233";
            //phone error
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            phone = "13456432345";
            //tenantId not exists
<<<<<<< HEAD
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName, state);
=======
            result = usersService.createUser(user, userName, userPassword, email, tenantId, phone, queueName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.TENANT_NOT_EXIST, result.get(Constants.STATUS));
            //success
            Mockito.when(tenantMapper.queryById(1)).thenReturn(getTenant());
<<<<<<< HEAD
            result = usersService.createUser(user, userName, userPassword, email, 1, phone, queueName, state);
=======
            result = usersService.createUser(user, userName, userPassword, email, 1, phone, queueName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));

        } catch (Exception e) {
<<<<<<< HEAD
            logger.error(Status.CREATE_USER_ERROR.getMsg(), e);
=======
            logger.error(Status.CREATE_USER_ERROR.getMsg(),e);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            Assert.assertTrue(false);
        }
    }

    @Test
<<<<<<< HEAD
    public void testQueryUser() {
        String userName = "userTest0001";
        String userPassword = "userTest0001";
        when(userMapper.queryUserByNamePassword(userName, EncryptionUtils.getMd5(userPassword))).thenReturn(getGeneralUser());
        User queryUser = usersService.queryUser(userName, userPassword);
        logger.info(queryUser.toString());
        Assert.assertTrue(queryUser != null);
    }

    @Test
    public void testSelectByIds() {
        List<Integer> ids = new ArrayList<>();
        List<User> users = usersService.queryUser(ids);
        Assert.assertTrue(users.isEmpty());
        ids.add(1);
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        when(userMapper.selectByIds(ids)).thenReturn(userList);
        List<User> userList1 = usersService.queryUser(ids);
        Assert.assertFalse(userList1.isEmpty());
=======
    public void testQueryUser(){

        String userName = "userTest0001";
        String userPassword = "userTest0001";
        when(userMapper.queryUserByNamePassword(userName,EncryptionUtils.getMd5(userPassword))).thenReturn(getGeneralUser());
        User queryUser = usersService.queryUser(userName,  userPassword);
        logger.info(queryUser.toString());
        Assert.assertTrue(queryUser!=null);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testGetUserIdByName() {
        User user = new User();
        user.setId(1);
        user.setUserType(UserType.ADMIN_USER);
        user.setUserName("test_user");

        //user name null
        int userId = usersService.getUserIdByName("");
        Assert.assertEquals(0, userId);

        //user not exist
        when(usersService.queryUser(user.getUserName())).thenReturn(null);
        int userNotExistId = usersService.getUserIdByName(user.getUserName());
        Assert.assertEquals(-1, userNotExistId);

        //user exist
        when(usersService.queryUser(user.getUserName())).thenReturn(user);
        int userExistId = usersService.getUserIdByName(user.getUserName());
        Assert.assertEquals(user.getId(), userExistId);
    }

<<<<<<< HEAD
    @Test
    public void testQueryUserList() {
=======


    @Test
    public void testQueryUserList(){


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();

        //no operate
        Map<String, Object> result = usersService.queryUserList(user);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));

        //success
        user.setUserType(UserType.ADMIN_USER);
<<<<<<< HEAD
        when(userMapper.selectList(null)).thenReturn(getUserList());
        result = usersService.queryUserList(user);
        List<User> userList = (List<User>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(userList.size() > 0);
    }

    @Test
    public void testQueryUserListPage() {
        User user = new User();
        IPage<User> page = new Page<>(1, 10);
=======
        when(userMapper.selectList(null )).thenReturn(getUserList());
        result = usersService.queryUserList(user);
        List<User> userList = (List<User>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(userList.size()>0);
    }

    @Test
    public void testQueryUserListPage(){


        User user = new User();
        IPage<User> page = new Page<>(1,10);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        page.setRecords(getUserList());
        when(userMapper.queryUserPaging(any(Page.class), eq("userTest"))).thenReturn(page);

        //no operate
<<<<<<< HEAD
        Result result = usersService.queryUserList(user, "userTest", 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM.getCode(), (int) result.getCode());

        //success
        user.setUserType(UserType.ADMIN_USER);
        result = usersService.queryUserList(user, "userTest", 1, 10);
        Assert.assertEquals(Status.SUCCESS.getCode(), (int) result.getCode());
        PageInfo<User> pageInfo = (PageInfo<User>) result.getData();
        Assert.assertTrue(pageInfo.getTotalList().size() > 0);
    }

    @Test
    public void testUpdateUser() {
=======
        Map<String, Object> result = usersService.queryUserList(user,"userTest",1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));

        //success
        user.setUserType(UserType.ADMIN_USER);
        result = usersService.queryUserList(user,"userTest",1,10);
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        PageInfo<User> pageInfo  = (PageInfo<User>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(pageInfo.getLists().size()>0);
    }

    @Test
    public void testUpdateUser(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String userName = "userTest0001";
        String userPassword = "userTest0001";
        try {
            //user not exist
<<<<<<< HEAD
            Map<String, Object> result = usersService.updateUser(getLoginUser(), 0, userName, userPassword, "3443@qq.com", 1, "13457864543", "queue", 1);
=======
            Map<String, Object> result = usersService.updateUser(getLoginUser(), 0,userName,userPassword,"3443@qq.com",1,"13457864543","queue");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));
            logger.info(result.toString());

            //success
            when(userMapper.selectById(1)).thenReturn(getUser());
<<<<<<< HEAD
            result = usersService.updateUser(getLoginUser(), 1, userName, userPassword, "32222s@qq.com", 1, "13457864543", "queue", 1);
            logger.info(result.toString());
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        } catch (Exception e) {
            logger.error("update user error", e);
=======
            result = usersService.updateUser(getLoginUser(), 1,userName,userPassword,"32222s@qq.com",1,"13457864543","queue");
            logger.info(result.toString());
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        } catch (Exception e) {
            logger.error("update user error",e);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            Assert.assertTrue(false);
        }
    }

    @Test
<<<<<<< HEAD
    public void testDeleteUserById() {
=======
    public void testDeleteUserById(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User loginUser = new User();
        try {
            when(userMapper.queryTenantCodeByUserId(1)).thenReturn(getUser());
            when(userMapper.selectById(1)).thenReturn(getUser());
<<<<<<< HEAD
            when(accessTokenMapper.deleteAccessTokenByUserId(1)).thenReturn(0);
            //no operate
            Map<String, Object> result = usersService.deleteUserById(loginUser, 3);
=======

            //no operate
            Map<String, Object> result = usersService.deleteUserById(loginUser,3);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
            logger.info(result.toString());
            Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));

            // user not exist
            loginUser.setUserType(UserType.ADMIN_USER);
<<<<<<< HEAD
            result = usersService.deleteUserById(loginUser, 3);
            logger.info(result.toString());
            Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));

            // user is project owner
            Mockito.when(projectMapper.queryProjectCreatedByUser(1)).thenReturn(Lists.newArrayList(new Project()));
            result = usersService.deleteUserById(loginUser, 1);
            Assert.assertEquals(Status.TRANSFORM_PROJECT_OWNERSHIP, result.get(Constants.STATUS));

            //success
            Mockito.when(projectMapper.queryProjectCreatedByUser(1)).thenReturn(null);
            result = usersService.deleteUserById(loginUser, 1);
            logger.info(result.toString());
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        } catch (Exception e) {
            logger.error("delete user error", e);
            Assert.assertTrue(false);
        }

    }

    @Test
    public void testGrantProject() {
        when(userMapper.selectById(1)).thenReturn(getUser());
        User loginUser = new User();
        String projectIds = "100000,120000";
=======
            result = usersService.deleteUserById(loginUser,3);
            logger.info(result.toString());
            Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));

            //success
            result = usersService.deleteUserById(loginUser,1);
            logger.info(result.toString());
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        } catch (Exception e) {
           logger.error("delete user error",e);
           Assert.assertTrue(false);
        }


    }

    @Test
    public void testGrantProject(){

        when(userMapper.selectById(1)).thenReturn(getUser());
        User loginUser = new User();
        String  projectIds= "100000,120000";
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = usersService.grantProject(loginUser, 1, projectIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //user not exist
        loginUser.setUserType(UserType.ADMIN_USER);
        result = usersService.grantProject(loginUser, 2, projectIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));
        //success
        result = usersService.grantProject(loginUser, 1, projectIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
    }

    @Test
<<<<<<< HEAD
    public void testGrantResources() {
=======
    public void testGrantResources(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String resourceIds = "100000,120000";
        when(userMapper.selectById(1)).thenReturn(getUser());
        User loginUser = new User();
        Map<String, Object> result = usersService.grantResources(loginUser, 1, resourceIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //user not exist
        loginUser.setUserType(UserType.ADMIN_USER);
        result = usersService.grantResources(loginUser, 2, resourceIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));
        //success
<<<<<<< HEAD
        when(resourceMapper.selectById(Mockito.anyInt())).thenReturn(getResource());
        when(resourceUserMapper.deleteResourceUser(1, 0)).thenReturn(1);
=======
        when(resourceMapper.queryAuthorizedResourceList(1)).thenReturn(new ArrayList<Resource>());

        when(resourceMapper.selectById(Mockito.anyInt())).thenReturn(getResource());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        result = usersService.grantResources(loginUser, 1, resourceIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));

    }

<<<<<<< HEAD
    @Test
    public void testGrantUDFFunction() {
=======

    @Test
    public void testGrantUDFFunction(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String udfIds = "100000,120000";
        when(userMapper.selectById(1)).thenReturn(getUser());
        User loginUser = new User();
        Map<String, Object> result = usersService.grantUDFFunction(loginUser, 1, udfIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //user not exist
        loginUser.setUserType(UserType.ADMIN_USER);
        result = usersService.grantUDFFunction(loginUser, 2, udfIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));
        //success
<<<<<<< HEAD
        when(udfUserMapper.deleteByUserId(1)).thenReturn(1);
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        result = usersService.grantUDFFunction(loginUser, 1, udfIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
    }

    @Test
<<<<<<< HEAD
    public void testGrantDataSource() {
=======
    public void testGrantDataSource(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        String datasourceIds = "100000,120000";
        when(userMapper.selectById(1)).thenReturn(getUser());
        User loginUser = new User();
        Map<String, Object> result = usersService.grantDataSource(loginUser, 1, datasourceIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //user not exist
        loginUser.setUserType(UserType.ADMIN_USER);
        result = usersService.grantDataSource(loginUser, 2, datasourceIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));
        //success
<<<<<<< HEAD
        when(datasourceUserMapper.deleteByUserId(Mockito.anyInt())).thenReturn(1);
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        result = usersService.grantDataSource(loginUser, 1, datasourceIds);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));

    }

<<<<<<< HEAD
    private User getLoginUser() {
=======
    private User getLoginUser(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User loginUser = new User();
        loginUser.setId(1);
        loginUser.setUserType(UserType.ADMIN_USER);
        return loginUser;
    }

    @Test
<<<<<<< HEAD
    public void getUserInfo() {
=======
    public void getUserInfo(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User loginUser = new User();
        loginUser.setUserName("admin");
        loginUser.setUserType(UserType.ADMIN_USER);
        // get admin user
        Map<String, Object> result = usersService.getUserInfo(loginUser);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        User tempUser = (User) result.get(Constants.DATA_LIST);
        //check userName
<<<<<<< HEAD
        Assert.assertEquals("admin", tempUser.getUserName());
=======
        Assert.assertEquals("admin",tempUser.getUserName());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //get general user
        loginUser.setUserType(null);
        loginUser.setId(1);
        when(userMapper.queryDetailsById(1)).thenReturn(getGeneralUser());
<<<<<<< HEAD
        when(alertGroupMapper.queryByUserId(1)).thenReturn(getAlertGroups());
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        result = usersService.getUserInfo(loginUser);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        tempUser = (User) result.get(Constants.DATA_LIST);
        //check userName
<<<<<<< HEAD
        Assert.assertEquals("userTest0001", tempUser.getUserName());
    }

    @Test
    public void testQueryAllGeneralUsers() {
=======
        Assert.assertEquals("userTest0001",tempUser.getUserName());
    }


    @Test
    public void testQueryAllGeneralUsers(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User loginUser = new User();
        //no operate
        Map<String, Object> result = usersService.queryAllGeneralUsers(loginUser);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //success
        loginUser.setUserType(UserType.ADMIN_USER);
        when(userMapper.queryAllGeneralUser()).thenReturn(getUserList());
        result = usersService.queryAllGeneralUsers(loginUser);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        List<User> userList = (List<User>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(userList));
    }

    @Test
<<<<<<< HEAD
    public void testVerifyUserName() {
=======
    public void testVerifyUserName(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //not exist user
        Result result = usersService.verifyUserName("admin89899");
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS.getMsg(), result.getMsg());
        //exist user
        when(userMapper.queryByUserNameAccurately("userTest0001")).thenReturn(getUser());
        result = usersService.verifyUserName("userTest0001");
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NAME_EXIST.getMsg(), result.getMsg());
    }

    @Test
<<<<<<< HEAD
    public void testUnauthorizedUser() {
        User loginUser = new User();
        when(userMapper.selectList(null)).thenReturn(getUserList());
        when(userMapper.queryUserListByAlertGroupId(2)).thenReturn(getUserList());
=======
    public void testUnauthorizedUser(){

        User loginUser = new User();
        when(userMapper.selectList(null )).thenReturn(getUserList());
        when( userMapper.queryUserListByAlertGroupId(2)).thenReturn(getUserList());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        //no operate
        Map<String, Object> result = usersService.unauthorizedUser(loginUser, 2);
        logger.info(result.toString());
        loginUser.setUserType(UserType.ADMIN_USER);
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //success
        result = usersService.unauthorizedUser(loginUser, 2);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
    }

<<<<<<< HEAD
    @Test
    public void testAuthorizedUser() {
=======

    @Test
    public void testAuthorizedUser(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User loginUser = new User();
        when(userMapper.queryUserListByAlertGroupId(2)).thenReturn(getUserList());
        //no operate
        Map<String, Object> result = usersService.authorizedUser(loginUser, 2);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        //success
        loginUser.setUserType(UserType.ADMIN_USER);
        result = usersService.authorizedUser(loginUser, 2);
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        List<User> userList = (List<User>) result.get(Constants.DATA_LIST);
        logger.info(result.toString());
        Assert.assertTrue(CollectionUtils.isNotEmpty(userList));
    }

<<<<<<< HEAD
    @Test
    public void testRegisterUser() {
        String userName = "userTest0002~";
        String userPassword = "userTest";
        String repeatPassword = "userTest";
        String email = "123@qq.com";
        try {
            //userName error
            Map<String, Object> result = usersService.registerUser(userName, userPassword, repeatPassword, email);
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            userName = "userTest0002";
            userPassword = "userTest000111111111111111";
            //password error
            result = usersService.registerUser(userName, userPassword, repeatPassword, email);
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            userPassword = "userTest0002";
            email = "1q.com";
            //email error
            result = usersService.registerUser(userName, userPassword, repeatPassword, email);
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            //repeatPassword error
            email = "7400@qq.com";
            repeatPassword = "userPassword";
            result = usersService.registerUser(userName, userPassword, repeatPassword, email);
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            //success
            repeatPassword = "userTest0002";
            result = usersService.registerUser(userName, userPassword, repeatPassword, email);
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));

        } catch (Exception e) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testActivateUser() {
        User user = new User();
        user.setUserType(UserType.GENERAL_USER);
        String userName = "userTest0002~";
        try {
            //not admin
            Map<String, Object> result = usersService.activateUser(user, userName);
            Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));

            //userName error
            user.setUserType(UserType.ADMIN_USER);
            result = usersService.activateUser(user, userName);
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            //user not exist
            userName = "userTest10013";
            result = usersService.activateUser(user, userName);
            Assert.assertEquals(Status.USER_NOT_EXIST, result.get(Constants.STATUS));

            //user state error
            userName = "userTest0001";
            when(userMapper.queryByUserNameAccurately(userName)).thenReturn(getUser());
            result = usersService.activateUser(user, userName);
            Assert.assertEquals(Status.REQUEST_PARAMS_NOT_VALID_ERROR, result.get(Constants.STATUS));

            //success
            when(userMapper.queryByUserNameAccurately(userName)).thenReturn(getDisabledUser());
            result = usersService.activateUser(user, userName);
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        } catch (Exception e) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testBatchActivateUser() {
        User user = new User();
        user.setUserType(UserType.GENERAL_USER);
        List<String> userNames = new ArrayList<>();
        userNames.add("userTest0001");
        userNames.add("userTest0002");
        userNames.add("userTest0003~");
        userNames.add("userTest0004");

        try {
            //not admin
            Map<String, Object> result = usersService.batchActivateUser(user, userNames);
            Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));

            //batch activate user names
            user.setUserType(UserType.ADMIN_USER);
            when(userMapper.queryByUserNameAccurately("userTest0001")).thenReturn(getUser());
            when(userMapper.queryByUserNameAccurately("userTest0002")).thenReturn(getDisabledUser());
            result = usersService.batchActivateUser(user, userNames);
            Map<String, Object> responseData = (Map<String, Object>) result.get(Constants.DATA_LIST);
            Map<String, Object> successData = (Map<String, Object>) responseData.get("success");
            int totalSuccess = (Integer) successData.get("sum");

            Map<String, Object> failedData = (Map<String, Object>) responseData.get("failed");
            int totalFailed = (Integer) failedData.get("sum");

            Assert.assertEquals(1, totalSuccess);
            Assert.assertEquals(3, totalFailed);
            Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
        } catch (Exception e) {
            Assert.assertTrue(false);
        }
    }

    /**
     * get disabled user
     */
    private User getDisabledUser() {
        User user = new User();
        user.setUserType(UserType.GENERAL_USER);
        user.setUserName("userTest0001");
        user.setUserPassword("userTest0001");
        user.setState(0);
        return user;
    }

    /**
     * get user
     */
    private User getGeneralUser() {
=======
    /**
     * get user
     * @return
     */
    private User getGeneralUser(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserType(UserType.GENERAL_USER);
        user.setUserName("userTest0001");
        user.setUserPassword("userTest0001");
        return user;
    }

<<<<<<< HEAD
    private List<User> getUserList() {
=======

    private List<User> getUserList(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<User> userList = new ArrayList<>();
        userList.add(getGeneralUser());
        return userList;
    }

    /**
     * get user
     */
<<<<<<< HEAD
    private User getUser() {
=======
    private User getUser(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        User user = new User();
        user.setUserType(UserType.ADMIN_USER);
        user.setUserName("userTest0001");
        user.setUserPassword("userTest0001");
<<<<<<< HEAD
        user.setState(1);
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return user;
    }

    /**
     * get tenant
<<<<<<< HEAD
     *
     * @return tenant
     */
    private Tenant getTenant() {
=======
     * @return tenant
     */
    private Tenant getTenant(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Tenant tenant = new Tenant();
        tenant.setId(1);
        return tenant;
    }

    /**
     * get resource
<<<<<<< HEAD
     *
     * @return resource
     */
    private Resource getResource() {
=======
     * @return resource
     */
    private Resource getResource(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Resource resource = new Resource();
        resource.setPid(-1);
        resource.setUserId(1);
        resource.setDescription("ResourcesServiceTest.jar");
        resource.setAlias("ResourcesServiceTest.jar");
        resource.setFullName("/ResourcesServiceTest.jar");
        resource.setType(ResourceType.FILE);
        return resource;
    }

<<<<<<< HEAD
    private List<AlertGroup> getAlertGroups() {
        List<AlertGroup> alertGroups = new ArrayList<>();
        AlertGroup alertGroup = new AlertGroup();
        alertGroups.add(alertGroup);
        return alertGroups;
    }

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}