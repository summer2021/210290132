/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.service.impl.AlertGroupServiceImpl;
import org.apache.dolphinscheduler.api.utils.PageInfo;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
=======
package org.apache.dolphinscheduler.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.PageInfo;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.AlertType;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.CollectionUtils;
import org.apache.dolphinscheduler.dao.entity.AlertGroup;
import org.apache.dolphinscheduler.dao.entity.User;
import org.apache.dolphinscheduler.dao.mapper.AlertGroupMapper;
<<<<<<< HEAD
import org.apache.dolphinscheduler.dao.vo.AlertGroupVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
=======
import org.apache.dolphinscheduler.dao.mapper.UserAlertGroupMapper;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import static org.mockito.ArgumentMatchers.*;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
<<<<<<< HEAD
import org.springframework.dao.DuplicateKeyException;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * alert group service test
 */
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RunWith(MockitoJUnitRunner.class)
public class AlertGroupServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(AlertGroupServiceTest.class);

    @InjectMocks
<<<<<<< HEAD
    private AlertGroupServiceImpl alertGroupService;

    @Mock
    private AlertGroupMapper alertGroupMapper;

    private String groupName = "AlertGroupServiceTest";

    @Test
    public void testQueryAlertGroup() {

        Mockito.when(alertGroupMapper.queryAllGroupList()).thenReturn(getList());
        Map<String, Object> result = alertGroupService.queryAlertgroup();
=======
    private AlertGroupService alertGroupService;
    @Mock
    private AlertGroupMapper alertGroupMapper;
    @Mock
    private UserAlertGroupMapper userAlertGroupMapper;
    @Mock
    UserAlertGroupService userAlertGroupService;

    private String groupName = "AlertGroupServiceTest";

    @Before
    public void setUp() {
    }


    @After
    public void after(){

    }



    @Test
    public  void testQueryAlertgroup(){

        Mockito.when(alertGroupMapper.queryAllGroupList()).thenReturn(getList());
        HashMap<String, Object> result= alertGroupService.queryAlertgroup();
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(result.toString());
        List<AlertGroup> alertGroups = (List<AlertGroup>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(alertGroups));
    }
<<<<<<< HEAD

    @Test
    public void testListPaging() {
        IPage<AlertGroupVo> page = new Page<>(1, 10);
        page.setTotal(1L);
        page.setRecords(getAlertGroupVoList());
        Mockito.when(alertGroupMapper.queryAlertGroupVo(any(Page.class), eq(groupName))).thenReturn(page);
        User user = new User();
        // no operate
        Result result = alertGroupService.listPaging(user, groupName, 1, 10);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM.getCode(), (int) result.getCode());
        //success
        user.setUserType(UserType.ADMIN_USER);
        result = alertGroupService.listPaging(user, groupName, 1, 10);
        logger.info(result.toString());
        PageInfo<AlertGroupVo> pageInfo = (PageInfo<AlertGroupVo>) result.getData();
        Assert.assertTrue(CollectionUtils.isNotEmpty(pageInfo.getTotalList()));

    }

    @Test
    public void testCreateAlertgroup() {
=======
    @Test
    public  void testListPaging(){
        IPage<AlertGroup> page = new Page<>(1,10);
        page.setTotal(1L);
        page.setRecords(getList());
        Mockito.when(alertGroupMapper.queryAlertGroupPage(any(Page.class),eq(groupName))).thenReturn(page);
        User user = new User();
        // no operate
        Map<String, Object> result = alertGroupService.listPaging(user,groupName,1,10);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
        //success
        user.setUserType(UserType.ADMIN_USER);
        result = alertGroupService.listPaging(user,groupName,1,10);
        logger.info(result.toString());
        PageInfo<AlertGroup> pageInfo = (PageInfo<AlertGroup>) result.get(Constants.DATA_LIST);
        Assert.assertTrue(CollectionUtils.isNotEmpty(pageInfo.getLists()));

    }
    @Test
    public  void testCreateAlertgroup(){

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Mockito.when(alertGroupMapper.insert(any(AlertGroup.class))).thenReturn(2);
        User user = new User();
        //no operate
<<<<<<< HEAD
        Map<String, Object> result = alertGroupService.createAlertgroup(user, groupName, groupName, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        user.setUserType(UserType.ADMIN_USER);
        //success
        result = alertGroupService.createAlertgroup(user, groupName, groupName, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
    }

    @Test
    public void testCreateAlertgroupDuplicate() {

        Mockito.when(alertGroupMapper.insert(any(AlertGroup.class))).thenThrow(new DuplicateKeyException("group name exist"));
        User user = new User();
        user.setUserType(UserType.ADMIN_USER);
        Map<String, Object> result = alertGroupService.createAlertgroup(user, groupName, groupName, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.ALERT_GROUP_EXIST, result.get(Constants.STATUS));
    }

    @Test
    public void testUpdateAlertgroup() {

        User user = new User();
        // no operate
        Map<String, Object> result = alertGroupService.updateAlertgroup(user, 1, groupName, groupName, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        user.setUserType(UserType.ADMIN_USER);
        // not exist
        result = alertGroupService.updateAlertgroup(user, 1, groupName, groupName, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.ALERT_GROUP_NOT_EXIST, result.get(Constants.STATUS));
        //success
        Mockito.when(alertGroupMapper.selectById(2)).thenReturn(getEntity());
        result = alertGroupService.updateAlertgroup(user, 2, groupName, groupName, null);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));

    }

    @Test
    public void testUpdateAlertgroupDuplicate() {
        User user = new User();
        user.setUserType(UserType.ADMIN_USER);
        Mockito.when(alertGroupMapper.selectById(2)).thenReturn(getEntity());
        Mockito.when(alertGroupMapper.updateById(Mockito.any())).thenThrow(new DuplicateKeyException("group name exist"));
        Map<String, Object> result = alertGroupService.updateAlertgroup(user, 2, groupName, groupName, null);
        Assert.assertEquals(Status.ALERT_GROUP_EXIST, result.get(Constants.STATUS));
    }

    @Test
    public void testDelAlertgroupById() {

        User user = new User();
        // no operate
        Map<String, Object> result = alertGroupService.delAlertgroupById(user, 1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM, result.get(Constants.STATUS));
        user.setUserType(UserType.ADMIN_USER);
        // not exist
        result = alertGroupService.delAlertgroupById(user, 2);
        logger.info(result.toString());
        Assert.assertEquals(Status.ALERT_GROUP_NOT_EXIST, result.get(Constants.STATUS));
        //success
        Mockito.when(alertGroupMapper.selectById(2)).thenReturn(getEntity());
        result = alertGroupService.delAlertgroupById(user, 2);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS, result.get(Constants.STATUS));

=======
        Map<String, Object>  result = alertGroupService.createAlertgroup(user,groupName, AlertType.EMAIL,groupName);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
        user.setUserType(UserType.ADMIN_USER);
        //success
        result = alertGroupService.createAlertgroup(user,groupName, AlertType.EMAIL,groupName);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
    }
    @Test
    public  void testUpdateAlertgroup(){

        User user = new User();
        // no operate
        Map<String, Object>  result = alertGroupService.updateAlertgroup(user,1,groupName, AlertType.SMS,groupName);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
        user.setUserType(UserType.ADMIN_USER);
        // not exist
        result = alertGroupService.updateAlertgroup(user,1,groupName, AlertType.SMS,groupName);
        logger.info(result.toString());
        Assert.assertEquals(Status.ALERT_GROUP_NOT_EXIST,result.get(Constants.STATUS));
        //success
        Mockito.when(alertGroupMapper.selectById(2)).thenReturn(getEntity());
        result = alertGroupService.updateAlertgroup(user,2,groupName, AlertType.SMS,groupName);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));

    }
    @Test
    public  void testDelAlertgroupById(){

        User user = new User();
        // no operate
        Map<String, Object>  result = alertGroupService.delAlertgroupById(user,1);
        logger.info(result.toString());
        Assert.assertEquals(Status.USER_NO_OPERATION_PERM,result.get(Constants.STATUS));
        user.setUserType(UserType.ADMIN_USER);
        // not exist
        result = alertGroupService.delAlertgroupById(user,2);
        logger.info(result.toString());
        Assert.assertEquals(Status.ALERT_GROUP_NOT_EXIST,result.get(Constants.STATUS));
        //success
        Mockito.when(alertGroupMapper.selectById(2)).thenReturn(getEntity());
        result = alertGroupService.delAlertgroupById(user,2);
        logger.info(result.toString());
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));


    }

    @Test
    public void testGrantUser() {

        Integer groupId = 1;

        ArgumentCaptor<Integer> groupArgument = ArgumentCaptor.forClass(Integer.class);

        Mockito.when(userAlertGroupService.deleteByAlertGroupId(anyInt())).thenReturn(true);

        Map<String, Object> result = alertGroupService.grantUser(getLoginUser(), groupId, "123,321");
        Mockito.verify(userAlertGroupService).deleteByAlertGroupId(groupArgument.capture());

        logger.info(result.toString());
        assertEquals(groupArgument.getValue(), groupId);
        assertEquals(Status.SUCCESS, result.get(Constants.STATUS));
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    @Test
    public void testVerifyGroupName() {
        //group name not exist
        boolean result = alertGroupService.existGroupName(groupName);
        Assert.assertFalse(result);
<<<<<<< HEAD
        Mockito.when(alertGroupMapper.existGroupName(groupName)).thenReturn(true);
=======
        Mockito.when(alertGroupMapper.queryByGroupName(groupName)).thenReturn(getList());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        //group name exist
        result = alertGroupService.existGroupName(groupName);
        Assert.assertTrue(result);
    }

<<<<<<< HEAD
    /**
     * create admin user
     */
    private User getLoginUser() {
=======

    /**
     * create admin user
     * @return
     */
    private User getLoginUser(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        User loginUser = new User();
        loginUser.setUserType(UserType.ADMIN_USER);
        loginUser.setId(99999999);
        return loginUser;
    }

    /**
     * get list
<<<<<<< HEAD
     */
    private List<AlertGroup> getList() {
=======
     * @return
     */
    private List<AlertGroup> getList(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        List<AlertGroup> alertGroups = new ArrayList<>();
        alertGroups.add(getEntity());
        return alertGroups;
    }

    /**
     * get entity
<<<<<<< HEAD
     */
    private AlertGroup getEntity() {
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setId(1);
        alertGroup.setGroupName(groupName);
        return alertGroup;
    }

    /**
     * get AlertGroupVo list
     */
    private List<AlertGroupVo> getAlertGroupVoList() {
        List<AlertGroupVo> alertGroupVos = new ArrayList<>();
        alertGroupVos.add(getAlertGroupVoEntity());
        return alertGroupVos;
    }

    /**
     * get AlertGroupVo entity
     */
    private AlertGroupVo getAlertGroupVoEntity() {
        AlertGroupVo alertGroupVo = new AlertGroupVo();
        alertGroupVo.setId(1);
        alertGroupVo.setGroupName(groupName);
        return alertGroupVo;
    }

=======
     * @return
     */
    private AlertGroup getEntity(){
        AlertGroup alertGroup = new AlertGroup();
        alertGroup.setId(1);
        alertGroup.setGroupName(groupName);
        alertGroup.setGroupType(AlertType.EMAIL);
        return alertGroup;
    }

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
