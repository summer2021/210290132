/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.service.impl.BaseServiceImpl;
=======
package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.enums.Status;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.HadoopUtils;
import org.apache.dolphinscheduler.dao.entity.User;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * base service test
 */
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"sun.security.*", "javax.net.*"})
@PrepareForTest({HadoopUtils.class})
public class BaseServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(BaseServiceTest.class);

<<<<<<< HEAD
    private BaseServiceImpl baseService;
=======
    private BaseService baseService;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    @Mock
    private HadoopUtils hadoopUtils;

    @Before
    public void setUp() {
<<<<<<< HEAD
        baseService = new BaseServiceImpl();
    }

    @Test
    public void testIsAdmin() {
=======
        baseService = new BaseService();
    }

    @Test
    public void testIsAdmin(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        User user = new User();
        user.setUserType(UserType.ADMIN_USER);
        //ADMIN_USER
<<<<<<< HEAD
        boolean isAdmin = baseService.isAdmin(user);
=======
         boolean isAdmin = baseService.isAdmin(user);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Assert.assertTrue(isAdmin);
        //GENERAL_USER
        user.setUserType(UserType.GENERAL_USER);
        isAdmin = baseService.isAdmin(user);
        Assert.assertFalse(isAdmin);

    }

    @Test
<<<<<<< HEAD
    public void testPutMsg() {
=======
    public void testPutMsg(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Map<String, Object> result = new HashMap<>();
        baseService.putMsg(result, Status.SUCCESS);
        Assert.assertEquals(Status.SUCCESS,result.get(Constants.STATUS));
        //has params
        baseService.putMsg(result, Status.PROJECT_NOT_FOUNT,"test");

    }
<<<<<<< HEAD

    @Test
    public void testPutMsgTwo() {
=======
    @Test
    public void testPutMsgTwo(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Result result = new Result();
        baseService.putMsg(result, Status.SUCCESS);
        Assert.assertEquals(Status.SUCCESS.getMsg(),result.getMsg());
        //has params
        baseService.putMsg(result,Status.PROJECT_NOT_FOUNT,"test");
    }
<<<<<<< HEAD

    @Test
    public void testCreateTenantDirIfNotExists() {
=======
    @Test
    public void testCreateTenantDirIfNotExists(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        PowerMockito.mockStatic(HadoopUtils.class);
        PowerMockito.when(HadoopUtils.getInstance()).thenReturn(hadoopUtils);

        try {
            baseService.createTenantDirIfNotExists("test");
        } catch (Exception e) {
            Assert.assertTrue(false);
            logger.error("CreateTenantDirIfNotExists error ",e);
            e.printStackTrace();
        }

    }
<<<<<<< HEAD

    @Test
    public void testHasPerm() {
=======
    @Test
    public void testHasPerm(){
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        User user = new User();
        user.setId(1);
        //create user
        boolean hasPerm = baseService.hasPerm(user,1);
        Assert.assertTrue(hasPerm);

        //admin
        user.setId(2);
        user.setUserType(UserType.ADMIN_USER);
        hasPerm = baseService.hasPerm(user,1);
        Assert.assertTrue(hasPerm);

    }

}
