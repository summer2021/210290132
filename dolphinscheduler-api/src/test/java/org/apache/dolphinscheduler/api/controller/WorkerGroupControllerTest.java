<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.dolphinscheduler.api.utils.RegistryCenterUtils;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.NodeType;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.apache.dolphinscheduler.dao.entity.WorkerGroup;
import org.apache.dolphinscheduler.dao.mapper.ProcessInstanceMapper;
import org.apache.dolphinscheduler.dao.mapper.WorkerGroupMapper;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
=======
 package org.apache.dolphinscheduler.api.controller;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

<<<<<<< HEAD
/**
 * worker group controller test
 */
public class WorkerGroupControllerTest extends AbstractControllerTest {

    private static Logger logger = LoggerFactory.getLogger(WorkerGroupControllerTest.class);

    @MockBean
    private WorkerGroupMapper workerGroupMapper;

    @MockBean
    private ProcessInstanceMapper processInstanceMapper;

    @Test
    public void testSaveWorkerGroup() throws Exception {
        Map<String, String> serverMaps = new HashMap<>();
        serverMaps.put("192.168.0.1", "192.168.0.1");
        serverMaps.put("192.168.0.2", "192.168.0.2");
        PowerMockito.when(RegistryCenterUtils.getServerMaps(NodeType.WORKER, true)).thenReturn(serverMaps);

        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("name","cxc_work_group");
        paramsMap.add("addrList","192.168.0.1,192.168.0.2");
=======
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class WorkerGroupControllerTest extends AbstractControllerTest{
    private static Logger logger = LoggerFactory.getLogger(WorkerGroupControllerTest.class);

    @Test
    public void testSaveWorkerGroup() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("name","cxc_work_group");
        paramsMap.add("ipList","192.16.12,192.168,10,12");
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        MvcResult mvcResult = mockMvc.perform(post("/worker-group/save")
                .header("sessionId", sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertTrue(result != null && result.isSuccess());
=======
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testQueryAllWorkerGroupsPaging() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("pageNo","2");
        paramsMap.add("searchVal","cxc");
        paramsMap.add("pageSize","2");
        MvcResult mvcResult = mockMvc.perform(get("/worker-group/list-paging")
                .header("sessionId", sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertTrue(result != null && result.isSuccess());
=======
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testQueryAllWorkerGroups() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        MvcResult mvcResult = mockMvc.perform(get("/worker-group/all-groups")
                .header("sessionId", sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertTrue(result != null && result.isSuccess());
=======
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testDeleteById() throws Exception {
<<<<<<< HEAD
        WorkerGroup workerGroup = new WorkerGroup();
        workerGroup.setId(12);
        workerGroup.setName("测试");
        Mockito.when(workerGroupMapper.selectById(12)).thenReturn(workerGroup);
        Mockito.when(processInstanceMapper.queryByWorkerGroupNameAndStatus("测试", Constants.NOT_TERMINATED_STATES)).thenReturn(null);
        Mockito.when(workerGroupMapper.deleteById(12)).thenReturn(1);
        Mockito.when(processInstanceMapper.updateProcessInstanceByWorkerGroupName("测试", "")).thenReturn(1);

        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("id","12");
        MvcResult mvcResult = mockMvc.perform(post("/worker-group/delete-by-id")
=======
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("id","12");
        MvcResult mvcResult = mockMvc.perform(get("/worker-group/delete-by-id")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header("sessionId", sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertTrue(result != null && result.isSuccess());
=======
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());
    }
}
