/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.utils.JSONUtils;

=======
package org.apache.dolphinscheduler.api.controller;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

<<<<<<< HEAD
/**
 * tenant controller test
 */
public class TenantControllerTest extends AbstractControllerTest {

=======
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * tenant controller test
 */
public class TenantControllerTest extends AbstractControllerTest{
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    private static Logger logger = LoggerFactory.getLogger(TenantControllerTest.class);

    @Test
    public void testCreateTenant() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
<<<<<<< HEAD
        paramsMap.add("tenantCode","hayden");
        paramsMap.add("queueId","1");
        paramsMap.add("description","tenant description");

        MvcResult mvcResult = mockMvc.perform(post("/tenants/")
=======
        paramsMap.add("tenantCode","tenantCode");
        paramsMap.add("tenantName","tenantName");
        paramsMap.add("queueId","1");
        paramsMap.add("description","tenant description");

        MvcResult mvcResult = mockMvc.perform(post("/tenant/create")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testQueryTenantlistPaging() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("pageNo","1");
        paramsMap.add("searchVal","tenant");
        paramsMap.add("pageSize","30");

<<<<<<< HEAD
        MvcResult mvcResult = mockMvc.perform(get("/tenants/")
=======
        MvcResult mvcResult = mockMvc.perform(get("/tenant/list-paging")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testUpdateTenant() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("id","9");
        paramsMap.add("tenantCode","cxc_te");
<<<<<<< HEAD
        paramsMap.add("queueId","1");
        paramsMap.add("description","tenant description");

        MvcResult mvcResult = mockMvc.perform(put("/tenants/{id}", 9)
=======
        paramsMap.add("tenantName","tenant_update_2");
        paramsMap.add("queueId","1");
        paramsMap.add("description","tenant description");

        MvcResult mvcResult = mockMvc.perform(post("/tenant/update")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getCode(),result.getCode().intValue());
=======
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());

    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Test
    public void testVerifyTenantCode() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("tenantCode","cxc_test");

<<<<<<< HEAD
        MvcResult mvcResult = mockMvc.perform(get("/tenants/verify-code")
=======
        MvcResult mvcResult = mockMvc.perform(get("/tenant/verify-tenant-code")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testVerifyTenantCodeExists() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
<<<<<<< HEAD
        paramsMap.add("tenantCode", "hayden");

        MvcResult mvcResult = mockMvc.perform(get("/tenants/verify-code")
=======
        paramsMap.add("tenantCode", "tenantCode");

        MvcResult mvcResult = mockMvc.perform(get("/tenant/verify-tenant-code")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertEquals(Status.OS_TENANT_CODE_EXIST.getCode(), result.getCode().intValue());
=======
        Assert.assertEquals(Status.TENANT_NAME_EXIST.getCode(), result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void testQueryTenantlist() throws Exception {

<<<<<<< HEAD
        MvcResult mvcResult = mockMvc.perform(get("/tenants/list")
=======
        MvcResult mvcResult = mockMvc.perform(get("/tenant/list")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
        logger.info(mvcResult.getResponse().getContentAsString());
        logger.info(mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void testDeleteTenantById() throws Exception {
        MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
        paramsMap.add("id","64");

<<<<<<< HEAD
        MvcResult mvcResult = mockMvc.perform(delete("/tenants/{id}", 64)
=======
        MvcResult mvcResult = mockMvc.perform(post("/tenant/delete")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
                .header(SESSION_ID, sessionId)
                .params(paramsMap))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
<<<<<<< HEAD
        Assert.assertEquals(Status.TENANT_NOT_EXIST.getCode(),result.getCode().intValue());
=======
        Assert.assertEquals(Status.SUCCESS.getCode(),result.getCode().intValue());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        logger.info(mvcResult.getResponse().getContentAsString());
    }
}
