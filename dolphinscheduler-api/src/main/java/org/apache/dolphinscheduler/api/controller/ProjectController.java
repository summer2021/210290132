/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.apache.dolphinscheduler.api.enums.Status.CREATE_PROJECT_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.DELETE_PROJECT_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.LOGIN_USER_QUERY_PROJECT_LIST_PAGING_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_AUTHORIZED_AND_USER_CREATED_PROJECT_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_AUTHORIZED_PROJECT;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_PROJECT_DETAILS_BY_CODE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_UNAUTHORIZED_PROJECT_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.UPDATE_PROJECT_ERROR;

import org.apache.dolphinscheduler.api.aspect.AccessLogAnnotation;
import org.apache.dolphinscheduler.api.exceptions.ApiException;
=======
package org.apache.dolphinscheduler.api.controller;


import org.apache.dolphinscheduler.api.exceptions.ApiException;
import org.apache.dolphinscheduler.api.service.ProcessDefinitionService;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.service.ProjectService;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.utils.ParameterUtils;
import org.apache.dolphinscheduler.dao.entity.User;
<<<<<<< HEAD

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
<<<<<<< HEAD
import springfox.documentation.annotations.ApiIgnore;

/**
 * project controller
 */
@Api(tags = "PROJECT_TAG")
=======
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.ws.rs.Path;
import java.util.Map;

import static org.apache.dolphinscheduler.api.enums.Status.*;

/**
 * project controller
 */
@Api(tags = "PROJECT_TAG", position = 1)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RestController
@RequestMapping("projects")
public class ProjectController extends BaseController {

<<<<<<< HEAD
    @Autowired
    private ProjectService projectService;

    /**
     * create project
     *
     * @param loginUser login user
=======
    private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProcessDefinitionService processDefinitionService;

    /**
     * create project
     *
     * @param loginUser   login user
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param projectName project name
     * @param description description
     * @return returns an error if it exists
     */
<<<<<<< HEAD
    @ApiOperation(value = "create", notes = "CREATE_PROJECT_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "projectName", value = "PROJECT_NAME", dataType = "String"),
        @ApiImplicitParam(name = "description", value = "PROJECT_DESC", dataType = "String")
    })
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(CREATE_PROJECT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result createProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam("projectName") String projectName,
                                @RequestParam(value = "description", required = false) String description) {
=======
    @ApiOperation(value = "createProject", notes = "CREATE_PROJECT_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectName", value = "PROJECT_NAME", dataType = "String"),
            @ApiImplicitParam(name = "description", value = "PROJECT_DESC", dataType = "String")
    })
    @PostMapping(value = "")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(CREATE_PROJECT_ERROR)
    public Result createProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam("projectName") String projectName,
                                @RequestParam(value = "description", required = false) String description) {

        logger.info("login user {}, create project name: {}, desc: {}", loginUser.getUserName(), projectName, description);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = projectService.createProject(loginUser, projectName, description);
        return returnDataList(result);
    }

    /**
<<<<<<< HEAD
     * update project
     *
     * @param loginUser login user
     * @param code project code
=======
     * updateProcessInstance project
     *
     * @param loginUser   login user
     * @param projectId   project id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param projectName project name
     * @param description description
     * @return update result code
     */
<<<<<<< HEAD
    @ApiOperation(value = "update", notes = "UPDATE_PROJECT_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "code", value = "PROJECT_CODE", dataType = "Long", example = "123456"),
        @ApiImplicitParam(name = "projectName", value = "PROJECT_NAME", dataType = "String"),
        @ApiImplicitParam(name = "description", value = "PROJECT_DESC", dataType = "String"),
        @ApiImplicitParam(name = "userName", value = "USER_NAME", dataType = "String"),
    })
    @PutMapping(value = "/{code}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(UPDATE_PROJECT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result updateProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @PathVariable("code") Long code,
                                @RequestParam("projectName") String projectName,
                                @RequestParam(value = "description", required = false) String description,
                                @RequestParam(value = "userName") String userName) {
        Map<String, Object> result = projectService.update(loginUser, code, projectName, description, userName);
=======
    @ApiOperation(value = "updateProject", notes = "UPDATE_PROJECT_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100"),
            @ApiImplicitParam(name = "projectName", value = "PROJECT_NAME", dataType = "String"),
            @ApiImplicitParam(name = "description", value = "PROJECT_DESC", dataType = "String")
    })
    @PatchMapping(value = "/{projectId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(UPDATE_PROJECT_ERROR)
    public Result updateProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @PathVariable("projectId") Integer projectId,
                                @RequestParam("projectName") String projectName,
                                @RequestParam(value = "description", required = false) String description) {
        logger.info("login user {} , updateProcessInstance project name: {}, desc: {}", loginUser.getUserName(), projectName, description);
        Map<String, Object> result = projectService.update(loginUser, projectId, projectName, description);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
<<<<<<< HEAD
     * query project details by code
     *
     * @param loginUser login user
     * @param code project code
     * @return project detail information
     */
    @ApiOperation(value = "queryProjectByCode", notes = "QUERY_PROJECT_BY_ID_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "code", value = "PROJECT_CODE", dataType = "Long", example = "123456")
    })
    @GetMapping(value = "/{code}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_PROJECT_DETAILS_BY_CODE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryProjectByCode(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                     @PathVariable("code") long code) {
        Map<String, Object> result = projectService.queryByCode(loginUser, code);
=======
     * query project details by id
     *
     * @param loginUser login user
     * @param projectId project id
     * @return project detail information
     */
    @ApiOperation(value = "queryProjectById", notes = "QUERY_PROJECT_BY_ID_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100")
    })
    @GetMapping(value = "/{projectId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_PROJECT_DETAILS_BY_ID_ERROR)
    public Result queryProjectById(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @PathVariable("projectId") Integer projectId) {
        logger.info("login user {}, query project by id: {}", loginUser.getUserName(), projectId);

        Map<String, Object> result = projectService.queryById(projectId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * query project list paging
     *
     * @param loginUser login user
     * @param searchVal search value
<<<<<<< HEAD
     * @param pageSize page size
     * @param pageNo page number
=======
     * @param pageSize  page size
     * @param pageNo    page number
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return project list which the login user have permission to see
     */
    @ApiOperation(value = "queryProjectListPaging", notes = "QUERY_PROJECT_LIST_PAGING_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
        @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", required = true, dataType = "Int", example = "10"),
        @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", required = true, dataType = "Int", example = "1")
    })
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    @ApiException(LOGIN_USER_QUERY_PROJECT_LIST_PAGING_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
            @ApiImplicitParam(name = "projectId", value = "PAGE_SIZE", dataType = "Int", example = "20"),
            @ApiImplicitParam(name = "projectId", value = "PAGE_NO", dataType = "Int", example = "1")
    })
    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(LOGIN_USER_QUERY_PROJECT_LIST_PAGING_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result queryProjectListPaging(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                         @RequestParam(value = "searchVal", required = false) String searchVal,
                                         @RequestParam("pageSize") Integer pageSize,
                                         @RequestParam("pageNo") Integer pageNo
    ) {

<<<<<<< HEAD
        Result result = checkPageParams(pageNo, pageSize);
        if (!result.checkResult()) {
            return result;
        }
        searchVal = ParameterUtils.handleEscapes(searchVal);
        result = projectService.queryProjectListPaging(loginUser, pageSize, pageNo, searchVal);
        return result;
    }

    /**
     * delete project by code
     *
     * @param loginUser login user
     * @param code project code
     * @return delete result code
     */
    @ApiOperation(value = "delete", notes = "DELETE_PROJECT_BY_ID_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "code", value = "PROJECT_CODE", dataType = "Long", example = "123456")
    })
    @DeleteMapping(value = "/{code}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(DELETE_PROJECT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result deleteProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @PathVariable("code") Long code) {
        Map<String, Object> result = projectService.deleteProject(loginUser, code);
=======
        logger.info("login user {}, query project list paging", loginUser.getUserName());
        searchVal = ParameterUtils.handleEscapes(searchVal);
        Map<String, Object> result = projectService.queryProjectListPaging(loginUser, pageSize, pageNo, searchVal);
        return returnDataListPaging(result);
    }

    /**
     * delete project by id
     *
     * @param loginUser login user
     * @param projectId project id
     * @return delete result code
     */
    @ApiOperation(value = "deleteProjectById", notes = "DELETE_PROJECT_BY_ID_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100")
    })
    @DeleteMapping(value = "/{projectId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(DELETE_PROJECT_ERROR)
    public Result deleteProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @PathVariable("projectId") Integer projectId
    ) {

        logger.info("login user {}, delete project: {}.", loginUser.getUserName(), projectId);
        Map<String, Object> result = projectService.deleteProject(loginUser, projectId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * query unauthorized project
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return the projects which user have not permission to see
     */
    @ApiOperation(value = "queryUnauthorizedProject", notes = "QUERY_UNAUTHORIZED_PROJECT_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", dataType = "Int", example = "100")
    })
    @GetMapping(value = "/unauth-project")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_UNAUTHORIZED_PROJECT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryUnauthorizedProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                           @RequestParam("userId") Integer userId) {
=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", dataType = "Int", example = "100")
    })
    @GetMapping(value = "/unauth")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_UNAUTHORIZED_PROJECT_ERROR)
    public Result queryUnauthorizedProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                           @RequestParam("userId") Integer userId) {
        logger.info("login user {}, query unauthorized project by user id: {}.", loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = projectService.queryUnauthorizedProject(loginUser, userId);
        return returnDataList(result);
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * query authorized project
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return projects which the user have permission to see, Except for items created by this user
     */
    @ApiOperation(value = "queryAuthorizedProject", notes = "QUERY_AUTHORIZED_PROJECT_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", dataType = "Int", example = "100")
    })
    @GetMapping(value = "/authed-project")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_AUTHORIZED_PROJECT)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryAuthorizedProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                         @RequestParam("userId") Integer userId) {
=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", dataType = "Int", example = "100")
    })
    @GetMapping(value = "/authed")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_AUTHORIZED_PROJECT)
    public Result queryAuthorizedProject(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                         @RequestParam("userId") Integer userId) {
        logger.info("login user {}, query authorized project by user id: {}.", loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = projectService.queryAuthorizedProject(loginUser, userId);
        return returnDataList(result);
    }

    /**
<<<<<<< HEAD
     * query authorized and user created project
     *
     * @param loginUser login user
     * @return projects which the user create and authorized
     */
    @ApiOperation(value = "queryProjectCreatedAndAuthorizedByUser", notes = "QUERY_AUTHORIZED_AND_USER_CREATED_PROJECT_NOTES")
    @GetMapping(value = "/created-and-authed")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_AUTHORIZED_AND_USER_CREATED_PROJECT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryProjectCreatedAndAuthorizedByUser(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {
        Map<String, Object> result = projectService.queryProjectCreatedAndAuthorizedByUser(loginUser);
=======
     * import process definition
     *
     * @param loginUser   login user
     * @param file        resource file
     * @param projectName project name
     * @return import result code
     */

    @ApiOperation(value = "importProcessDefinition", notes= "EXPORT_PROCESS_DEFINITION_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "RESOURCE_FILE", required = true, dataType = "MultipartFile")
    })
    @PostMapping(value = "/import-definition")
    @ApiException(IMPORT_PROCESS_DEFINE_ERROR)
    public Result importProcessDefinition(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                          @RequestParam("file") MultipartFile file,
                                          @RequestParam("projectName") String projectName) {
        logger.info("import process definition by id, login user:{}, project: {}",
                loginUser.getUserName(), projectName);
        Map<String, Object> result = processDefinitionService.importProcessDefinition(loginUser, file, projectName);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * query all project list
     *
     * @param loginUser login user
     * @return all project list
     */
    @ApiOperation(value = "queryAllProjectList", notes = "QUERY_ALL_PROJECT_LIST_NOTES")
    @GetMapping(value = "/list")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(LOGIN_USER_QUERY_PROJECT_LIST_PAGING_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryAllProjectList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {
        Map<String, Object> result = projectService.queryAllProjectList();
        return returnDataList(result);
    }
=======
    public Result queryAllProjectList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {
        logger.info("login user {}, query all project list", loginUser.getUserName());
        Map<String, Object> result = projectService.queryAllProjectList();
        return returnDataList(result);
    }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
