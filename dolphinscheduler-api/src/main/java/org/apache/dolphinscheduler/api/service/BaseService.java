/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
package org.apache.dolphinscheduler.api.service;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
<<<<<<< HEAD
import org.apache.dolphinscheduler.dao.entity.User;

import java.io.IOException;
import java.util.Map;
=======
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.UserType;
import org.apache.dolphinscheduler.common.utils.DateUtils;
import org.apache.dolphinscheduler.common.utils.HadoopUtils;
import org.apache.dolphinscheduler.common.utils.StringUtils;
import org.apache.dolphinscheduler.dao.entity.User;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

/**
 * base service
 */
<<<<<<< HEAD
public interface BaseService {
=======
public class BaseService {
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * check admin
     *
     * @param user input user
     * @return ture if administrator, otherwise return false
     */
<<<<<<< HEAD
    boolean isAdmin(User user);

    /**
     * isNotAdmin
     *
     * @param loginUser login user
     * @param result result code
     * @return true if not administrator, otherwise false
     */
    boolean isNotAdmin(User loginUser, Map<String, Object> result);
=======
    protected boolean isAdmin(User user) {
        return user.getUserType() == UserType.ADMIN_USER;
    }

    /**
     * check admin
     *
     * @param loginUser login user
     * @param result result code
     * @return true if administrator, otherwise false
     */
    protected boolean checkAdmin(User loginUser, Map<String, Object> result) {
        //only admin can operate
        if (!isAdmin(loginUser)) {
            putMsg(result, Status.USER_NO_OPERATION_PERM);
            return true;
        }
        return false;
    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * put message to map
     *
     * @param result result code
     * @param status status
     * @param statusParams status message
     */
<<<<<<< HEAD
    void putMsg(Map<String, Object> result, Status status, Object... statusParams);
=======
    protected void putMsg(Map<String, Object> result, Status status, Object... statusParams) {
        result.put(Constants.STATUS, status);
        if (statusParams != null && statusParams.length > 0) {
            result.put(Constants.MSG, MessageFormat.format(status.getMsg(), statusParams));
        } else {
            result.put(Constants.MSG, status.getMsg());
        }
    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * put message to result object
     *
     * @param result result code
     * @param status status
     * @param statusParams status message
     */
<<<<<<< HEAD
    void putMsg(Result<Object> result, Status status, Object... statusParams);
=======
    protected void putMsg(Result result, Status status, Object... statusParams) {
        result.setCode(status.getCode());

        if (statusParams != null && statusParams.length > 0) {
            result.setMsg(MessageFormat.format(status.getMsg(), statusParams));
        } else {
            result.setMsg(status.getMsg());
        }

    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * check
     *
     * @param result result
     * @param bool bool
     * @param userNoOperationPerm status
     * @return check result
     */
<<<<<<< HEAD
    boolean check(Map<String, Object> result, boolean bool, Status userNoOperationPerm);

    /**
     * create tenant dir if not exists
     *
     * @param tenantCode tenant code
     * @throws IOException if hdfs operation exception
     */
    void createTenantDirIfNotExists(String tenantCode) throws IOException;

    /**
     * has perm
     *
     * @param operateUser operate user
     * @param createUserId create user id
     */
    boolean hasPerm(User operateUser, int createUserId);
=======
    protected boolean check(Map<String, Object> result, boolean bool, Status userNoOperationPerm) {
        //only admin can operate
        if (bool) {
            result.put(Constants.STATUS, userNoOperationPerm);
            result.put(Constants.MSG, userNoOperationPerm.getMsg());
            return true;
        }
        return false;
    }

    /**
     * create tenant dir if not exists
     * @param tenantCode tenant code
     * @throws Exception if hdfs operation exception
     */
    protected void createTenantDirIfNotExists(String tenantCode)throws Exception{

        String resourcePath = HadoopUtils.getHdfsResDir(tenantCode);
        String udfsPath = HadoopUtils.getHdfsUdfDir(tenantCode);
        /**
         * init resource path and udf path
         */
        HadoopUtils.getInstance().mkdir(resourcePath);
        HadoopUtils.getInstance().mkdir(udfsPath);
    }

    protected boolean hasPerm(User operateUser, int createUserId){
        return operateUser.getId() == createUserId || isAdmin(operateUser);
    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    /**
     * check and parse date parameters
     *
     * @param startDateStr start date string
     * @param endDateStr end date string
     * @return map<status,startDate,endDate>
     */
<<<<<<< HEAD
    Map<String, Object> checkAndParseDateParameters(String startDateStr, String endDateStr);
=======
    Map<String, Object> checkAndParseDateParameters(String startDateStr, String endDateStr){
        Map<String, Object> result = new HashMap<>();
        Date start = null;
        if (StringUtils.isNotEmpty(startDateStr)) {
            start = DateUtils.getScheduleDate(startDateStr);
            if (Objects.isNull(start)) {
                putMsg(result, Status.REQUEST_PARAMS_NOT_VALID_ERROR, Constants.START_END_DATE);
                return result;
            }
        }
        result.put(Constants.START_TIME, start);

        Date end = null;
        if (StringUtils.isNotEmpty(endDateStr)) {
            end = DateUtils.getScheduleDate(endDateStr);
            if (Objects.isNull(end)) {
                putMsg(result, Status.REQUEST_PARAMS_NOT_VALID_ERROR, Constants.START_END_DATE);
                return result;
            }
        }
        result.put(Constants.END_TIME, end);

        putMsg(result, Status.SUCCESS);
        return result;
    }
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
