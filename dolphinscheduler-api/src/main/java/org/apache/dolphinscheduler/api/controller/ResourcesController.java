/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.apache.dolphinscheduler.api.enums.Status.AUTHORIZED_FILE_RESOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.AUTHORIZED_UDF_FUNCTION_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.AUTHORIZE_RESOURCE_TREE;
import static org.apache.dolphinscheduler.api.enums.Status.CREATE_RESOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.CREATE_RESOURCE_FILE_ON_LINE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.CREATE_UDF_FUNCTION_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.DELETE_RESOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.DELETE_UDF_FUNCTION_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.DOWNLOAD_RESOURCE_FILE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.EDIT_RESOURCE_FILE_ON_LINE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_DATASOURCE_BY_TYPE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_RESOURCES_LIST_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_RESOURCES_LIST_PAGING;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_UDF_FUNCTION_LIST_PAGING_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.RESOURCE_FILE_IS_EMPTY;
import static org.apache.dolphinscheduler.api.enums.Status.RESOURCE_NOT_EXIST;
import static org.apache.dolphinscheduler.api.enums.Status.UNAUTHORIZED_UDF_FUNCTION_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.UPDATE_RESOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.UPDATE_UDF_FUNCTION_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.VERIFY_RESOURCE_BY_NAME_AND_TYPE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.VERIFY_UDF_FUNCTION_NAME_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.VIEW_RESOURCE_FILE_ON_LINE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.VIEW_UDF_FUNCTION_ERROR;

import org.apache.dolphinscheduler.api.aspect.AccessLogAnnotation;
=======
package org.apache.dolphinscheduler.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.exceptions.ApiException;
import org.apache.dolphinscheduler.api.service.ResourcesService;
import org.apache.dolphinscheduler.api.service.UdfFuncService;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.common.enums.ProgramType;
import org.apache.dolphinscheduler.common.enums.ResourceType;
import org.apache.dolphinscheduler.common.enums.UdfType;
import org.apache.dolphinscheduler.common.utils.ParameterUtils;
import org.apache.dolphinscheduler.dao.entity.User;
<<<<<<< HEAD

import org.apache.commons.lang.StringUtils;

import java.util.Map;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * resources controller
 */
@Api(tags = "RESOURCES_TAG")
=======
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.ws.rs.Path;
import java.util.Map;

import static org.apache.dolphinscheduler.api.enums.Status.*;

/**
 * resources controller
 */
@Api(tags = "RESOURCES_TAG", position = 1)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
@RestController
@RequestMapping("resources")
public class ResourcesController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(ResourcesController.class);

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Autowired
    private ResourcesService resourceService;
    @Autowired
    private UdfFuncService udfFuncService;

    /**
<<<<<<< HEAD
     * @param loginUser login user
     * @param type type
     * @param alias alias
     * @param description description
     * @param pid parent id
     * @param currentDir current directory
=======
     * create directory
     *
     * @param loginUser   login user
     * @param type        type
     * @param alias       alias
     * @param description description
     * @param pid         parent id
     * @param currentDir  current directory
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return create result code
     */
    @ApiOperation(value = "createDirctory", notes = "CREATE_RESOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "name", value = "RESOURCE_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
        @ApiImplicitParam(name = "pid", value = "RESOURCE_PID", required = true, dataType = "Int", example = "10"),
        @ApiImplicitParam(name = "currentDir", value = "RESOURCE_CURRENTDIR", required = true, dataType = "String")
    })
    @PostMapping(value = "/directory")
    @ApiException(CREATE_RESOURCE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "name", value = "RESOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pid", value = "PARENT_RESOURCE_ID", dataType = "String"),
            @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
            @ApiImplicitParam(name = "file", value = "RESOURCE_FILE", required = true, dataType = "MultipartFile")
    })
    @PostMapping(value = "/{pid}/directories")
    @ApiException(CREATE_RESOURCE_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result createDirectory(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                  @RequestParam(value = "type") ResourceType type,
                                  @RequestParam(value = "name") String alias,
                                  @RequestParam(value = "description", required = false) String description,
<<<<<<< HEAD
                                  @RequestParam(value = "pid") int pid,
                                  @RequestParam(value = "currentDir") String currentDir) {
=======
                                  @PathVariable(value = "pid") int pid,
                                  @RequestParam(value = "currentDir") String currentDir) {
        logger.info("login user {}, create resource, type: {}, resource alias: {}, desc: {}, file: {},{}",
                loginUser.getUserName(), type, alias, description, pid, currentDir);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return resourceService.createDirectory(loginUser, alias, description, type, pid, currentDir);
    }

    /**
     * create resource
     *
<<<<<<< HEAD
=======
     * @param loginUser   login user
     * @param alias       alias
     * @param description description
     * @param type        type
     * @param file        file
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return create result code
     */
    @ApiOperation(value = "createResource", notes = "CREATE_RESOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "name", value = "RESOURCE_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
        @ApiImplicitParam(name = "file", value = "RESOURCE_FILE", required = true, dataType = "MultipartFile"),
        @ApiImplicitParam(name = "pid", value = "RESOURCE_PID", required = true, dataType = "Int", example = "10"),
        @ApiImplicitParam(name = "currentDir", value = "RESOURCE_CURRENTDIR", required = true, dataType = "String")
    })
    @PostMapping()
    @ApiException(CREATE_RESOURCE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "name", value = "RESOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
            @ApiImplicitParam(name = "file", value = "RESOURCE_FILE", required = true, dataType = "MultipartFile")
    })
    @PostMapping(value = "")
    @ApiException(CREATE_RESOURCE_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result createResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @RequestParam(value = "type") ResourceType type,
                                 @RequestParam(value = "name") String alias,
                                 @RequestParam(value = "description", required = false) String description,
                                 @RequestParam("file") MultipartFile file,
                                 @RequestParam(value = "pid") int pid,
                                 @RequestParam(value = "currentDir") String currentDir) {
<<<<<<< HEAD
=======
        logger.info("login user {}, create resource, type: {}, resource alias: {}, desc: {}, file: {},{}",
                loginUser.getUserName(), type, alias, description, file.getName(), file.getOriginalFilename());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return resourceService.createResource(loginUser, alias, description, type, file, pid, currentDir);
    }

    /**
     * update resource
     *
<<<<<<< HEAD
     * @param loginUser login user
     * @param alias alias
     * @param resourceId resource id
     * @param type resource type
     * @param description description
     * @param file resource file
=======
     * @param loginUser   login user
     * @param alias       alias
     * @param resourceId  resource id
     * @param type        resource type
     * @param description description
     * @param file        resource file
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return update result code
     */
    @ApiOperation(value = "updateResource", notes = "UPDATE_RESOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100"),
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "name", value = "RESOURCE_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
        @ApiImplicitParam(name = "file", value = "RESOURCE_FILE", required = true, dataType = "MultipartFile")
    })
    @PutMapping(value = "/{id}")
    @ApiException(UPDATE_RESOURCE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100"),
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "name", value = "RESOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
            @ApiImplicitParam(name = "file", value = "RESOURCE_FILE", required = true, dataType = "MultipartFile")
    })
    @PatchMapping(value = "/{id}")
    @ApiException(UPDATE_RESOURCE_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result updateResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @PathVariable(value = "id") int resourceId,
                                 @RequestParam(value = "type") ResourceType type,
                                 @RequestParam(value = "name") String alias,
                                 @RequestParam(value = "description", required = false) String description,
<<<<<<< HEAD
                                 @RequestParam(value = "file", required = false) MultipartFile file) {
=======
                                 @RequestParam(value = "file" ,required = false) MultipartFile file) {
        logger.info("login user {}, update resource, type: {}, resource alias: {}, desc: {}, file: {}",
                loginUser.getUserName(), type, alias, description, file);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return resourceService.updateResource(loginUser, resourceId, alias, description, type, file);
    }

    /**
     * query resources list
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param type resource type
=======
     * @param type      resource type
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return resource list
     */
    @ApiOperation(value = "queryResourceList", notes = "QUERY_RESOURCE_LIST_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType")
=======
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/list")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_RESOURCES_LIST_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryResourceList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam(value = "type") ResourceType type
    ) {
=======
    public Result queryResourceList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam(value = "type") ResourceType type
    ) {
        logger.info("query resource list, login user:{}, resource type:{}", loginUser.getUserName(), type);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = resourceService.queryResourceList(loginUser, type);
        return returnDataList(result);
    }

    /**
     * query resources list paging
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param type resource type
     * @param searchVal search value
     * @param pageNo page number
     * @param pageSize page size
=======
     * @param type      resource type
     * @param searchVal search value
     * @param pageNo    page number
     * @param pageSize  page size
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return resource list page
     */
    @ApiOperation(value = "queryResourceListPaging", notes = "QUERY_RESOURCE_LIST_PAGING_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "int", example = "10"),
        @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
        @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", required = true, dataType = "Int", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", required = true, dataType = "Int", example = "20")
    })
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_RESOURCES_LIST_PAGING)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "int"),
            @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", dataType = "Int", example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", dataType = "Int", example = "20")
    })
    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_RESOURCES_LIST_PAGING)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result queryResourceListPaging(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                          @RequestParam(value = "type") ResourceType type,
                                          @RequestParam(value = "id") int id,
                                          @RequestParam("pageNo") Integer pageNo,
                                          @RequestParam(value = "searchVal", required = false) String searchVal,
                                          @RequestParam("pageSize") Integer pageSize
    ) {
<<<<<<< HEAD
        Result result = checkPageParams(pageNo, pageSize);
        if (!result.checkResult()) {
            return result;
=======
        logger.info("query resource list, login user:{}, resource type:{}, search value:{}",
                loginUser.getUserName(), type, searchVal);
        Map<String, Object> result = checkPageParams(pageNo, pageSize);
        if (result.get(Constants.STATUS) != Status.SUCCESS) {
            return returnDataListPaging(result);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        }

        searchVal = ParameterUtils.handleEscapes(searchVal);
        result = resourceService.queryResourceListPaging(loginUser, id, type, searchVal, pageNo, pageSize);
<<<<<<< HEAD
        return result;
=======
        return returnDataListPaging(result);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }


    /**
     * delete resource
     *
<<<<<<< HEAD
     * @param loginUser login user
=======
     * @param loginUser  login user
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param resourceId resource id
     * @return delete result code
     */
    @ApiOperation(value = "deleteResource", notes = "DELETE_RESOURCE_BY_ID_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")
=======
            @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(DELETE_RESOURCE_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result deleteResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @PathVariable(value = "id") int resourceId
    ) throws Exception {
=======
    public Result deleteResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @PathVariable(value = "id") int resourceId
    ) throws Exception {
        logger.info("login user {}, delete resource id: {}",
                loginUser.getUserName(), resourceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return resourceService.delete(loginUser, resourceId);
    }


    /**
     * verify resource by alias and type
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param fullName resource full name
     * @param type resource type
=======
     * @param fullName  resource full name
     * @param type      resource type
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return true if the resource name not exists, otherwise return false
     */
    @ApiOperation(value = "verifyResourceName", notes = "VERIFY_RESOURCE_NAME_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "fullName", value = "RESOURCE_FULL_NAME", required = true, dataType = "String")
    })
    @GetMapping(value = "/verify-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VERIFY_RESOURCE_BY_NAME_AND_TYPE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "fullName", value = "RESOURCE_FULL_NAME", required = true, dataType = "String")
    })
    @PostMapping(value = "/verify-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VERIFY_RESOURCE_BY_NAME_AND_TYPE_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result verifyResourceName(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                     @RequestParam(value = "fullName") String fullName,
                                     @RequestParam(value = "type") ResourceType type
    ) {
<<<<<<< HEAD
=======
        logger.info("login user {}, verfiy resource alias: {},resource type: {}",
                loginUser.getUserName(), fullName, type);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return resourceService.verifyResourceName(fullName, type, loginUser);
    }

    /**
<<<<<<< HEAD
     * query resources by type
     *
     * @param loginUser login user
     * @param type resource type
     * @return resource list
     */
    @ApiOperation(value = "queryResourceByProgramType", notes = "QUERY_RESOURCE_LIST_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType")
    })
    @GetMapping(value = "/query-by-type")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_RESOURCES_LIST_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryResourceJarList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                       @RequestParam(value = "type") ResourceType type,
                                       @RequestParam(value = "programType", required = false) ProgramType programType
    ) {
        Map<String, Object> result = resourceService.queryResourceByProgramType(loginUser, type, programType);
=======
     * query resources jar list
     *
     * @param loginUser login user
     * @param type      resource type
     * @return resource list
     */
    @ApiOperation(value = "queryResourceByProgramType", notes = "QUERY_BY_PROGRAM_TYPE_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType")
    })
    @GetMapping(value = "/select-by-type")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_RESOURCES_LIST_ERROR)
    public Result queryResourceJarList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                       @RequestParam(value = "type") ResourceType type,
                                       @RequestParam(value = "programType",required = false) ProgramType programType
    ) {
        String programTypeName = programType == null ? "" : programType.name();
        String userName = loginUser.getUserName();
        userName = userName.replaceAll("[\n|\r|\t]", "_");
        logger.info("query resource list, login user:{}, resource type:{}, program type:{}", userName,programTypeName);
        Map<String, Object> result = resourceService.queryResourceByProgramType(loginUser, type,programType);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * query resource by full name and type
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param fullName resource full name
     * @param type resource type
     * @param id resource id
     * @return true if the resource name not exists, otherwise return false
     */
    @ApiOperation(value = "queryResource", notes = "QUERY_BY_RESOURCE_NAME")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "fullName", value = "RESOURCE_FULL_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = false, dataType = "Int", example = "10")
    })
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(RESOURCE_NOT_EXIST)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam(value = "fullName", required = false) String fullName,
                                @PathVariable(value = "id", required = false) Integer id,
                                @RequestParam(value = "type") ResourceType type
    ) {
=======
     * @param fullName  resource full name
     * @param type      resource type
     * @return true if the resource name not exists, otherwise return false
     */
    @ApiOperation(value = "queryResourceByName", notes = "QUERY_BY_RESOURCE_NAME")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "fullName", value = "RESOURCE_FULL_NAME", required = true, dataType = "String")
    })
    @GetMapping(value = "/select-by-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(RESOURCE_NOT_EXIST)
    public Result queryResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam(value = "fullName", required = false) String fullName,
                                @RequestParam(value = "id", required = false) Integer id,
                                @RequestParam(value = "type") ResourceType type
    ) {
        logger.info("login user {}, query resource by full name: {} or id: {},resource type: {}",
                loginUser.getUserName(), fullName, id, type);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        return resourceService.queryResource(fullName, id, type);
    }

    /**
     * view resource file online
     *
<<<<<<< HEAD
     * @param loginUser login user
     * @param resourceId resource id
     * @param skipLineNum skip line number
     * @param limit limit
     * @return resource content
     */
    @ApiOperation(value = "viewResource", notes = "VIEW_RESOURCE_BY_ID_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100"),
        @ApiImplicitParam(name = "skipLineNum", value = "SKIP_LINE_NUM", required = true, dataType = "Int", example = "100"),
        @ApiImplicitParam(name = "limit", value = "LIMIT", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/{id}/view")
    @ApiException(VIEW_RESOURCE_FILE_ON_LINE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result viewResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                               @PathVariable(value = "id") int resourceId,
                               @RequestParam(value = "skipLineNum") int skipLineNum,
                               @RequestParam(value = "limit") int limit
    ) {
=======
     * @param loginUser   login user
     * @param resourceId  resource id
     * @param skipLineNum skip line number
     * @param limit       limit
     * @return resource content
     */
    @ApiOperation(value = "queryResourceById", notes = "VIEW_RESOURCE_BY_ID_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100"),
            @ApiImplicitParam(name = "skipLineNum", value = "SKIP_LINE_NUM", required = true, dataType = "Int", example = "100"),
            @ApiImplicitParam(name = "limit", value = "LIMIT", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/{resourceId}")
    @ApiException(VIEW_RESOURCE_FILE_ON_LINE_ERROR)
    public Result viewResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                               @PathVariable(value = "resourceId") int resourceId,
                               @RequestParam(value = "skipLineNum") int skipLineNum,
                               @RequestParam(value = "limit") int limit
    ) {
        logger.info("login user {}, view resource : {}, skipLineNum {} , limit {}",
                loginUser.getUserName(), resourceId, skipLineNum, limit);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return resourceService.readResource(resourceId, skipLineNum, limit);
    }

    /**
     * create resource file online
     *
<<<<<<< HEAD
=======
     * @param loginUser   login user
     * @param type        resource type
     * @param fileName    file name
     * @param fileSuffix  file suffix
     * @param description description
     * @param content     content
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return create result code
     */
    @ApiOperation(value = "onlineCreateResource", notes = "ONLINE_CREATE_RESOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
        @ApiImplicitParam(name = "fileName", value = "RESOURCE_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "suffix", value = "SUFFIX", required = true, dataType = "String"),
        @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
        @ApiImplicitParam(name = "content", value = "CONTENT", required = true, dataType = "String"),
        @ApiImplicitParam(name = "pid", value = "RESOURCE_PID", required = true, dataType = "Int", example = "10"),
        @ApiImplicitParam(name = "currentDir", value = "RESOURCE_CURRENTDIR", required = true, dataType = "String")
    })
    @PostMapping(value = "/online-create")
    @ApiException(CREATE_RESOURCE_FILE_ON_LINE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "type", value = "RESOURCE_TYPE", required = true, dataType = "ResourceType"),
            @ApiImplicitParam(name = "fileName", value = "RESOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "suffix", value = "SUFFIX", required = true, dataType = "String"),
            @ApiImplicitParam(name = "description", value = "RESOURCE_DESC", dataType = "String"),
            @ApiImplicitParam(name = "content", value = "CONTENT", required = true, dataType = "String")
    })
    @PostMapping(value = "/online-create")
    @ApiException(CREATE_RESOURCE_FILE_ON_LINE_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result onlineCreateResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                       @RequestParam(value = "type") ResourceType type,
                                       @RequestParam(value = "fileName") String fileName,
                                       @RequestParam(value = "suffix") String fileSuffix,
                                       @RequestParam(value = "description", required = false) String description,
                                       @RequestParam(value = "content") String content,
                                       @RequestParam(value = "pid") int pid,
                                       @RequestParam(value = "currentDir") String currentDir
    ) {
<<<<<<< HEAD
=======
        logger.info("login user {}, online create resource! fileName : {}, type : {}, suffix : {},desc : {},content : {}",
                loginUser.getUserName(), fileName, type, fileSuffix, description, content, pid, currentDir);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        if (StringUtils.isEmpty(content)) {
            logger.error("resource file contents are not allowed to be empty");
            return error(Status.RESOURCE_FILE_IS_EMPTY.getCode(), RESOURCE_FILE_IS_EMPTY.getMsg());
        }
        return resourceService.onlineCreateResource(loginUser, type, fileName, fileSuffix, description, content, pid, currentDir);
    }

    /**
     * edit resource file online
     *
<<<<<<< HEAD
     * @param loginUser login user
     * @param resourceId resource id
     * @param content content
=======
     * @param loginUser  login user
     * @param resourceId resource id
     * @param content    content
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return update result code
     */
    @ApiOperation(value = "updateResourceContent", notes = "UPDATE_RESOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100"),
        @ApiImplicitParam(name = "content", value = "CONTENT", required = true, dataType = "String")
    })
    @PutMapping(value = "/{id}/update-content")
    @ApiException(EDIT_RESOURCE_FILE_ON_LINE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result updateResourceContent(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @PathVariable(value = "id") int resourceId,
                                        @RequestParam(value = "content") String content
    ) {
=======
            @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100"),
            @ApiImplicitParam(name = "content", value = "CONTENT", required = true, dataType = "String")
    })
    @PostMapping(value = "/{resourceId}/content")
    @ApiException(EDIT_RESOURCE_FILE_ON_LINE_ERROR)
    public Result updateResourceContent(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @PathVariable(value = "resourceId") int resourceId,
                                        @RequestParam(value = "content") String content
    ) {
        logger.info("login user {}, updateProcessInstance resource : {}",
                loginUser.getUserName(), resourceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        if (StringUtils.isEmpty(content)) {
            logger.error("The resource file contents are not allowed to be empty");
            return error(Status.RESOURCE_FILE_IS_EMPTY.getCode(), RESOURCE_FILE_IS_EMPTY.getMsg());
        }
        return resourceService.updateResourceContent(resourceId, content);
    }

    /**
     * download resource file
     *
<<<<<<< HEAD
     * @param loginUser login user
=======
     * @param loginUser  login user
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @param resourceId resource id
     * @return resource content
     */
    @ApiOperation(value = "downloadResource", notes = "DOWNLOAD_RESOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/{id}/download")
    @ResponseBody
    @ApiException(DOWNLOAD_RESOURCE_FILE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public ResponseEntity downloadResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                           @PathVariable(value = "id") int resourceId) throws Exception {
=======
            @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")
    })
    @PostMapping(value = "/{resourceId}/download")
    @ResponseBody
    @ApiException(DOWNLOAD_RESOURCE_FILE_ERROR)
    public ResponseEntity downloadResource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                           @PathVariable(value = "resourceId") int resourceId) throws Exception {
        logger.info("login user {}, download resource : {}",
                loginUser.getUserName(), resourceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Resource file = resourceService.downloadResource(resourceId);
        if (file == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Status.RESOURCE_NOT_EXIST.getMsg());
        }
        return ResponseEntity
<<<<<<< HEAD
            .ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
            .body(file);
=======
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }


    /**
     * create udf function
     *
<<<<<<< HEAD
     * @param loginUser login user
     * @param type udf type
     * @param funcName function name
     * @param argTypes argument types
     * @param database database
     * @param description description
     * @param className class name
     * @param resourceId resource id
=======
     * @param loginUser   login user
     * @param type        udf type
     * @param funcName    function name
     * @param argTypes    argument types
     * @param database    database
     * @param description description
     * @param className   class name
     * @param resourceId  resource id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return create result code
     */
    @ApiOperation(value = "createUdfFunc", notes = "CREATE_UDF_FUNCTION_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "UDF_TYPE", required = true, dataType = "UdfType"),
        @ApiImplicitParam(name = "funcName", value = "FUNC_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "className", value = "CLASS_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "argTypes", value = "ARG_TYPES", dataType = "String"),
        @ApiImplicitParam(name = "database", value = "DATABASE_NAME", dataType = "String"),
        @ApiImplicitParam(name = "description", value = "UDF_DESC", dataType = "String"),
        @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")
=======
            @ApiImplicitParam(name = "type", value = "UDF_TYPE", required = true, dataType = "UdfType"),
            @ApiImplicitParam(name = "funcName", value = "FUNC_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "suffix", value = "CLASS_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "argTypes", value = "ARG_TYPES", dataType = "String"),
            @ApiImplicitParam(name = "database", value = "DATABASE_NAME", dataType = "String"),
            @ApiImplicitParam(name = "description", value = "UDF_DESC", dataType = "String"),
            @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    })
    @PostMapping(value = "/{resourceId}/udf-func")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(CREATE_UDF_FUNCTION_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result createUdfFunc(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam(value = "type") UdfType type,
                                @RequestParam(value = "funcName") String funcName,
                                @RequestParam(value = "className") String className,
                                @RequestParam(value = "argTypes", required = false) String argTypes,
                                @RequestParam(value = "database", required = false) String database,
                                @RequestParam(value = "description", required = false) String description,
                                @PathVariable(value = "resourceId") int resourceId) {
<<<<<<< HEAD
=======
        logger.info("login user {}, create udf function, type: {},  funcName: {},argTypes: {} ,database: {},desc: {},resourceId: {}",
                loginUser.getUserName(), type, funcName, argTypes, database, description, resourceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return udfFuncService.createUdfFunction(loginUser, funcName, className, argTypes, database, description, type, resourceId);
    }

    /**
     * view udf function
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param id resource id
     * @return udf function detail
     */
    @ApiOperation(value = "viewUIUdfFunction", notes = "VIEW_UDF_FUNCTION_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")

    })
    @GetMapping(value = "/{id}/udf-func")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VIEW_UDF_FUNCTION_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result viewUIUdfFunction(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @PathVariable("id") int id) {
=======
     * @param id        resource id
     * @return udf function detail
     */
    @ApiOperation(value = "showUIUdfFunction", notes = "SHOW_UDF_FUNCTION_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")

    })
    @GetMapping(value = "/udf-func/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VIEW_UDF_FUNCTION_ERROR)
    public Result viewUIUdfFunction(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam("id") int id) {
        logger.info("login user {}, query udf{}",
                loginUser.getUserName(), id);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> map = udfFuncService.queryUdfFuncDetail(id);
        return returnDataList(map);
    }

    /**
     * update udf function
     *
<<<<<<< HEAD
     * @param loginUser login user
     * @param type resource type
     * @param funcName function name
     * @param argTypes argument types
     * @param database data base
     * @param description description
     * @param resourceId resource id
     * @param className class name
     * @param udfFuncId udf function id
=======
     * @param loginUser   login user
     * @param type        resource type
     * @param funcName    function name
     * @param argTypes    argument types
     * @param database    data base
     * @param description description
     * @param resourceId  resource id
     * @param className   class name
     * @param udfFuncId   udf function id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return update result code
     */
    @ApiOperation(value = "updateUdfFunc", notes = "UPDATE_UDF_FUNCTION_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "UDF_ID", required = true, dataType = "Int"),
        @ApiImplicitParam(name = "type", value = "UDF_TYPE", required = true, dataType = "UdfType"),
        @ApiImplicitParam(name = "funcName", value = "FUNC_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "className", value = "CLASS_NAME", required = true, dataType = "String"),
        @ApiImplicitParam(name = "argTypes", value = "ARG_TYPES", dataType = "String"),
        @ApiImplicitParam(name = "database", value = "DATABASE_NAME", dataType = "String"),
        @ApiImplicitParam(name = "description", value = "UDF_DESC", dataType = "String"),
        @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")

    })
    @PutMapping(value = "/{resourceId}/udf-func/{id}")
    @ApiException(UPDATE_UDF_FUNCTION_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "id", value = "UDF_ID", required = true, dataType = "udfId"),
            @ApiImplicitParam(name = "type", value = "UDF_TYPE", required = true, dataType = "UdfType"),
            @ApiImplicitParam(name = "funcName", value = "FUNC_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "suffix", value = "CLASS_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "argTypes", value = "ARG_TYPES", dataType = "String"),
            @ApiImplicitParam(name = "database", value = "DATABASE_NAME", dataType = "String"),
            @ApiImplicitParam(name = "description", value = "UDF_DESC", dataType = "String"),
            @ApiImplicitParam(name = "resourceId", value = "RESOURCE_ID", required = true, dataType = "Int", example = "100")

    })
    @PatchMapping(value = "/udf-func/{id}")
    @ApiException(UPDATE_UDF_FUNCTION_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result updateUdfFunc(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @PathVariable(value = "id") int udfFuncId,
                                @RequestParam(value = "type") UdfType type,
                                @RequestParam(value = "funcName") String funcName,
                                @RequestParam(value = "className") String className,
                                @RequestParam(value = "argTypes", required = false) String argTypes,
                                @RequestParam(value = "database", required = false) String database,
                                @RequestParam(value = "description", required = false) String description,
<<<<<<< HEAD
                                @PathVariable(value = "resourceId") int resourceId) {
=======
                                @RequestParam(value = "resourceId") int resourceId) {
        logger.info("login user {}, updateProcessInstance udf function id: {},type: {},  funcName: {},argTypes: {} ,database: {},desc: {},resourceId: {}",
                loginUser.getUserName(), udfFuncId, type, funcName, argTypes, database, description, resourceId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = udfFuncService.updateUdfFunc(udfFuncId, funcName, className, argTypes, database, description, type, resourceId);
        return returnDataList(result);
    }

    /**
     * query udf function list paging
     *
     * @param loginUser login user
     * @param searchVal search value
<<<<<<< HEAD
     * @param pageNo page number
     * @param pageSize page size
=======
     * @param pageNo    page number
     * @param pageSize  page size
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return udf function list page
     */
    @ApiOperation(value = "queryUdfFuncListPaging", notes = "QUERY_UDF_FUNCTION_LIST_PAGING_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
        @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", required = true, dataType = "Int", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", required = true, dataType = "Int", example = "20")
=======
            @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", dataType = "Int", example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", dataType = "Int", example = "20")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/udf-func")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_UDF_FUNCTION_LIST_PAGING_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result<Object> queryUdfFuncListPaging(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                                 @RequestParam("pageNo") Integer pageNo,
                                                 @RequestParam(value = "searchVal", required = false) String searchVal,
                                                 @RequestParam("pageSize") Integer pageSize
    ) {
        Result result = checkPageParams(pageNo, pageSize);
        if (!result.checkResult()) {
            return result;

        }
        result = udfFuncService.queryUdfFuncListPaging(loginUser, searchVal, pageNo, pageSize);
        return result;
=======
    public Result<Object> queryUdfFuncListPaging(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @RequestParam("pageNo") Integer pageNo,
                                   @RequestParam(value = "searchVal", required = false) String searchVal,
                                   @RequestParam("pageSize") Integer pageSize
    ) {
        logger.info("query udf functions list, login user:{},search value:{}",
                loginUser.getUserName(), searchVal);
        Map<String, Object> result = checkPageParams(pageNo, pageSize);
        if (result.get(Constants.STATUS) != Status.SUCCESS) {
            return returnDataListPaging(result);
        }

        result = udfFuncService.queryUdfFuncListPaging(loginUser, searchVal, pageNo, pageSize);
        return returnDataListPaging(result);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * query udf func list by type
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param type resource type
=======
     * @param type      resource type
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return resource list
     */
    @ApiOperation(value = "queryUdfFuncList", notes = "QUERY_UDF_FUNC_LIST_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "UDF_TYPE", required = true, dataType = "UdfType")
=======
            @ApiImplicitParam(name = "type", value = "UDF_TYPE", required = true, dataType = "UdfType")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/udf-func/list")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_DATASOURCE_BY_TYPE_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result<Object> queryUdfFuncList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                           @RequestParam("type") UdfType type) {
=======
    public Result<Object> queryUdfFuncList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam("type") UdfType type) {
        String userName = loginUser.getUserName();
        userName = userName.replaceAll("[\n|\r|\t]", "_");
        logger.info("query udf func list, user:{}, type:{}", userName, type);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = udfFuncService.queryUdfFuncList(loginUser, type.ordinal());
        return returnDataList(result);
    }

    /**
     * verify udf function name can use or not
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param name name
=======
     * @param name      name
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return true if the name can user, otherwise return false
     */
    @ApiOperation(value = "verifyUdfFuncName", notes = "VERIFY_UDF_FUNCTION_NAME_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "name", value = "FUNC_NAME", required = true, dataType = "String")

    })
    @GetMapping(value = "/udf-func/verify-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VERIFY_UDF_FUNCTION_NAME_ERROR)
    @AccessLogAnnotation
    public Result verifyUdfFuncName(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam(value = "name") String name
    ) {
=======
            @ApiImplicitParam(name = "name", value = "FUNC_NAME", required = true, dataType = "String")

    })
    @PostMapping(value = "/udf-func/verify-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VERIFY_UDF_FUNCTION_NAME_ERROR)
    public Result verifyUdfFuncName(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam(value = "name") String name
    ) {
        logger.info("login user {}, verfiy udf function name: {}",
                loginUser.getUserName(), name);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        return udfFuncService.verifyUdfFuncByName(name);
    }

    /**
     * delete udf function
     *
     * @param loginUser login user
     * @param udfFuncId udf function id
     * @return delete result code
     */
    @ApiOperation(value = "deleteUdfFunc", notes = "DELETE_UDF_FUNCTION_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "UDF_FUNC_ID", required = true, dataType = "Int", example = "100")
=======
            @ApiImplicitParam(name = "udfFuncId", value = "UDF_FUNC_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @DeleteMapping(value = "/udf-func/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(DELETE_UDF_FUNCTION_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation
    public Result deleteUdfFunc(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @PathVariable(value = "id") int udfFuncId
    ) {
=======
    public Result deleteUdfFunc(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam(value = "id") int udfFuncId
    ) {
        logger.info("login user {}, delete udf function id: {}", loginUser.getUserName(), udfFuncId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return udfFuncService.delete(udfFuncId);
    }

    /**
     * authorized file resource list
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return authorized result
     */
    @ApiOperation(value = "authorizedFile", notes = "AUTHORIZED_FILE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/authed-file")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(AUTHORIZED_FILE_RESOURCE_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation
    public Result authorizedFile(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @RequestParam("userId") Integer userId) {
=======
    public Result authorizedFile(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @RequestParam("userId") Integer userId) {
        logger.info("authorized file resource, user: {}, user id:{}", loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = resourceService.authorizedFile(loginUser, userId);
        return returnDataList(result);
    }


    /**
     * unauthorized file resource list
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return unauthorized result code
     */
    @ApiOperation(value = "authorizeResourceTree", notes = "AUTHORIZE_RESOURCE_TREE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/authed-resource-tree")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(AUTHORIZE_RESOURCE_TREE)
    @AccessLogAnnotation
    public Result authorizeResourceTree(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @RequestParam("userId") Integer userId) {
=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/authorize-resource-tree")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(AUTHORIZE_RESOURCE_TREE)
    public Result authorizeResourceTree(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @RequestParam("userId") Integer userId) {
        logger.info("all resource file, user:{}, user id:{}", loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = resourceService.authorizeResourceTree(loginUser, userId);
        return returnDataList(result);
    }


    /**
     * unauthorized udf function
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return unauthorized result code
     */
    @ApiOperation(value = "unauthUDFFunc", notes = "UNAUTHORIZED_UDF_FUNC_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/unauth-udf-func")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(UNAUTHORIZED_UDF_FUNCTION_ERROR)
    @AccessLogAnnotation
    public Result unauthUDFFunc(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam("userId") Integer userId) {
=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/udf-func/unauth")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(UNAUTHORIZED_UDF_FUNCTION_ERROR)
    public Result unauthUDFFunc(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                @RequestParam("userId") Integer userId) {
        logger.info("unauthorized udf function, login user:{}, unauthorized user id:{}", loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

        Map<String, Object> result = resourceService.unauthorizedUDFFunction(loginUser, userId);
        return returnDataList(result);
    }


    /**
     * authorized udf function
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return authorized result code
     */
    @ApiOperation(value = "authUDFFunc", notes = "AUTHORIZED_UDF_FUNC_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/authed-udf-func")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(AUTHORIZED_UDF_FUNCTION_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation
    public Result authorizedUDFFunction(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @RequestParam("userId") Integer userId) {
        Map<String, Object> result = resourceService.authorizedUDFFunction(loginUser, userId);
        return returnDataList(result);
    }
}
=======
    public Result authorizedUDFFunction(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @RequestParam("userId") Integer userId) {
        logger.info("auth udf function, login user:{}, auth user id:{}", loginUser.getUserName(), userId);
        Map<String, Object> result = resourceService.authorizedUDFFunction(loginUser, userId);
        return returnDataList(result);
    }
}
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
