/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.apache.dolphinscheduler.api.enums.Status.COMMAND_STATE_COUNT_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.COUNT_PROCESS_DEFINITION_USER_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.COUNT_PROCESS_INSTANCE_STATE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.QUEUE_COUNT_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.TASK_INSTANCE_STATE_COUNT_ERROR;

import org.apache.dolphinscheduler.api.aspect.AccessLogAnnotation;
=======
package org.apache.dolphinscheduler.api.controller;


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.exceptions.ApiException;
import org.apache.dolphinscheduler.api.service.DataAnalysisService;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
import org.apache.dolphinscheduler.dao.entity.User;
<<<<<<< HEAD

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
<<<<<<< HEAD
import springfox.documentation.annotations.ApiIgnore;

/**
 * data analysis controller
 */
@Api(tags = "DATA_ANALYSIS_TAG")
@RestController
@RequestMapping("projects/analysis")
public class DataAnalysisController extends BaseController {

=======
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

import static org.apache.dolphinscheduler.api.enums.Status.*;

/**
 * data analysis controller
 */
@Api(tags = "DATA_ANALYSIS_TAG", position = 1)
@RestController
@RequestMapping("/projects/{projectId}/analysis")
public class DataAnalysisController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(DataAnalysisController.class);


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Autowired
    DataAnalysisService dataAnalysisService;

    /**
     * statistical task instance status data
     *
     * @param loginUser login user
     * @param startDate count start date
<<<<<<< HEAD
     * @param endDate count end date
     * @param projectCode project code
=======
     * @param endDate   count end date
     * @param projectId project id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return task instance count data
     */
    @ApiOperation(value = "countTaskState", notes = "COUNT_TASK_STATE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "startDate", value = "START_DATE", dataType = "String"),
        @ApiImplicitParam(name = "endDate", value = "END_DATE", dataType = "String"),
        @ApiImplicitParam(name = "projectCode", value = "PROJECT_CODE", dataType = "Long", example = "100")
=======
            @ApiImplicitParam(name = "startDate", value = "START_DATE", dataType = "String"),
            @ApiImplicitParam(name = "endDate", value = "END_DATE", dataType = "String"),
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100", defaultValue = "0", required = true)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/task-state-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(TASK_INSTANCE_STATE_COUNT_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result countTaskState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @RequestParam(value = "startDate", required = false) String startDate,
                                 @RequestParam(value = "endDate", required = false) String endDate,
                                 @RequestParam(value = "projectCode", required = false, defaultValue = "0") long projectCode) {

        Map<String, Object> result = dataAnalysisService.countTaskStateByProject(loginUser, projectCode, startDate, endDate);
=======
    public Result countTaskState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @RequestParam(value = "startDate", required = false) String startDate,
                                 @RequestParam(value = "endDate", required = false) String endDate,
                                 @PathVariable(value = "projectId", required = false) int projectId) {
        logger.info("count task state, user:{}, start date: {}, end date:{}, project id {}",
                loginUser.getUserName(), startDate, endDate, projectId);
        Map<String, Object> result = dataAnalysisService.countTaskStateByProject(loginUser, projectId, startDate, endDate);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * statistical process instance status data
     *
     * @param loginUser login user
     * @param startDate start date
<<<<<<< HEAD
     * @param endDate end date
     * @param projectCode project code
=======
     * @param endDate   end date
     * @param projectId project id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return process instance data
     */
    @ApiOperation(value = "countProcessInstanceState", notes = "COUNT_PROCESS_INSTANCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "startDate", value = "START_DATE", dataType = "String"),
        @ApiImplicitParam(name = "endDate", value = "END_DATE", dataType = "String"),
        @ApiImplicitParam(name = "projectCode", value = "PROJECT_CODE", dataType = "Long", example = "100")
=======
            @ApiImplicitParam(name = "startDate", value = "START_DATE", dataType = "String"),
            @ApiImplicitParam(name = "endDate", value = "END_DATE", dataType = "String"),
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100", defaultValue = "0", required = true)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/process-state-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(COUNT_PROCESS_INSTANCE_STATE_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result countProcessInstanceState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                            @RequestParam(value = "startDate", required = false) String startDate,
                                            @RequestParam(value = "endDate", required = false) String endDate,
                                            @RequestParam(value = "projectCode", required = false, defaultValue = "0") long projectCode) {

        Map<String, Object> result = dataAnalysisService.countProcessInstanceStateByProject(loginUser, projectCode, startDate, endDate);
=======
    public Result countProcessInstanceState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                            @RequestParam(value = "startDate", required = false) String startDate,
                                            @RequestParam(value = "endDate", required = false) String endDate,
                                            @PathVariable(value = "projectId", required = false) int projectId) {
        logger.info("count process instance state, user:{}, start date: {}, end date:{}, project id:{}",
                loginUser.getUserName(), startDate, endDate, projectId);
        Map<String, Object> result = dataAnalysisService.countProcessInstanceStateByProject(loginUser, projectId, startDate, endDate);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * statistics the process definition quantities of certain person
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param projectCode project code
     * @return definition count in project code
     */
    @ApiOperation(value = "countDefinitionByUser", notes = "COUNT_PROCESS_DEFINITION_BY_USER_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "projectCode", value = "PROJECT_CODE", dataType = "Long", example = "100")
=======
     * @param projectId project id
     * @return definition count in project id
     */
    @ApiOperation(value = "countDefinitionByUser", notes = "COUNT_PROCESS_DEFINITION_BY_USER_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100", defaultValue = "0", required = true)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/define-user-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(COUNT_PROCESS_DEFINITION_USER_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result countDefinitionByUser(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @RequestParam(value = "projectCode", required = false, defaultValue = "0") long projectCode) {

        Map<String, Object> result = dataAnalysisService.countDefinitionByUser(loginUser, projectCode);
        return returnDataList(result);
    }

=======
    public Result countDefinitionByUser(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                        @PathVariable(value = "projectId", required = false) int projectId) {
        logger.info("count process definition , user:{}, project id:{}",
                loginUser.getUserName(), projectId);
        Map<String, Object> result = dataAnalysisService.countDefinitionByUser(loginUser, projectId);
        return returnDataList(result);
    }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * statistical command status data
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @return command state of user projects
     */
    @ApiOperation(value = "countCommandState", notes = "COUNT_COMMAND_STATE_NOTES")
    @GetMapping(value = "/command-state-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(COMMAND_STATE_COUNT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result countCommandState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {

        Map<String, Object> result = dataAnalysisService.countCommandState(loginUser);
=======
     * @param startDate start date
     * @param endDate   end date
     * @param projectId project id
     * @return command state in project id
     */
    @ApiOperation(value = "countCommandState", notes = "COUNT_COMMAND_STATE_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startDate", value = "START_DATE", dataType = "String"),
            @ApiImplicitParam(name = "endDate", value = "END_DATE", dataType = "String"),
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100", defaultValue = "0", required = true)
    })
    @GetMapping(value = "/command-state-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(COMMAND_STATE_COUNT_ERROR)
    public Result countCommandState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam(value = "startDate", required = false) String startDate,
                                    @RequestParam(value = "endDate", required = false) String endDate,
                                    @PathVariable(value = "projectId", required = false) int projectId) {
        logger.info("count command state, user:{}, start date: {}, end date:{}, project id {}",
                loginUser.getUserName(), startDate, endDate, projectId);
        Map<String, Object> result = dataAnalysisService.countCommandState(loginUser, projectId, startDate, endDate);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return returnDataList(result);
    }

    /**
     * queue count
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @return queue state count
     */
    @ApiOperation(value = "countQueueState", notes = "COUNT_QUEUE_STATE_NOTES")
    @GetMapping(value = "/queue-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUEUE_COUNT_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result countQueueState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {

        Map<String, Object> result = dataAnalysisService.countQueueState(loginUser);
        return returnDataList(result);
    }
=======
     * @param projectId project id
     * @return queue state count
     */
    @ApiOperation(value = "countQueueState", notes = "COUNT_QUEUE_STATE_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "PROJECT_ID", dataType = "Int", example = "100", defaultValue = "0", required = true)
    })
    @GetMapping(value = "/queue-count")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUEUE_COUNT_ERROR)
    public Result countQueueState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                  @PathVariable(value = "projectId", required = false) int projectId) {
        logger.info("count command state, user:{}, project id {}",
                loginUser.getUserName(), projectId);
        Map<String, Object> result = dataAnalysisService.countQueueState(loginUser, projectId);
        return returnDataList(result);
    }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
}
