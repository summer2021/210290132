/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
<<<<<<< HEAD

package org.apache.dolphinscheduler.api.controller;

import static org.apache.dolphinscheduler.api.enums.Status.AUTHORIZED_DATA_SOURCE;
import static org.apache.dolphinscheduler.api.enums.Status.CONNECTION_TEST_FAILURE;
import static org.apache.dolphinscheduler.api.enums.Status.CONNECT_DATASOURCE_FAILURE;
import static org.apache.dolphinscheduler.api.enums.Status.CREATE_DATASOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.DELETE_DATA_SOURCE_FAILURE;
import static org.apache.dolphinscheduler.api.enums.Status.KERBEROS_STARTUP_STATE;
import static org.apache.dolphinscheduler.api.enums.Status.QUERY_DATASOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.UNAUTHORIZED_DATASOURCE;
import static org.apache.dolphinscheduler.api.enums.Status.UPDATE_DATASOURCE_ERROR;
import static org.apache.dolphinscheduler.api.enums.Status.VERIFY_DATASOURCE_NAME_FAILURE;

import org.apache.dolphinscheduler.api.aspect.AccessLogAnnotation;
=======
package org.apache.dolphinscheduler.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.exceptions.ApiException;
import org.apache.dolphinscheduler.api.service.DataSourceService;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.Constants;
<<<<<<< HEAD
import org.apache.dolphinscheduler.common.datasource.BaseDataSourceParamDTO;
import org.apache.dolphinscheduler.common.datasource.ConnectionParam;
import org.apache.dolphinscheduler.common.datasource.DatasourceUtil;
=======
import org.apache.dolphinscheduler.common.enums.DbConnectType;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
import org.apache.dolphinscheduler.common.enums.DbType;
import org.apache.dolphinscheduler.common.utils.CommonUtils;
import org.apache.dolphinscheduler.common.utils.ParameterUtils;
import org.apache.dolphinscheduler.dao.entity.User;
<<<<<<< HEAD

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

/**
 * data source controller
 */
@Api(tags = "DATA_SOURCE_TAG")
@RestController
@RequestMapping("datasources")
public class DataSourceController extends BaseController {

=======
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.ws.rs.Path;
import java.util.Map;

import static org.apache.dolphinscheduler.api.enums.Status.*;

/**
 * data source controller
 */
@Api(tags = "DATA_SOURCE_TAG", position = 3)
@RestController
@RequestMapping("datasource")
public class DataSourceController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceController.class);

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    @Autowired
    private DataSourceService dataSourceService;

    /**
     * create data source
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param dataSourceParam datasource param
     * @return create result code
     */
    @ApiOperation(value = "createDataSource", notes = "CREATE_DATA_SOURCE_NOTES")
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(CREATE_DATASOURCE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result createDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @ApiParam(name = "dataSourceParam", value = "DATA_SOURCE_PARAM", required = true)
                                   @RequestBody BaseDataSourceParamDTO dataSourceParam) {
        return dataSourceService.createDataSource(loginUser, dataSourceParam);
    }

=======
     * @param name      data source name
     * @param note      data source description
     * @param type      data source type
     * @param host      host
     * @param port      port
     * @param database  data base
     * @param principal principal
     * @param userName  user name
     * @param password  password
     * @param other     other arguments
     * @return create result code
     */
    @ApiOperation(value = "createDataSource", notes = "CREATE_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "DATA_SOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "note", value = "DATA_SOURCE_NOTE", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "DB_TYPE", required = true, dataType = "DbType"),
            @ApiImplicitParam(name = "host", value = "DATA_SOURCE_HOST", required = true, dataType = "String"),
            @ApiImplicitParam(name = "port", value = "DATA_SOURCE_PORT", required = true, dataType = "String"),
            @ApiImplicitParam(name = "database", value = "DATABASE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "userName", value = "USER_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "PASSWORD", dataType = "String"),
            @ApiImplicitParam(name = "connectType", value = "CONNECT_TYPE", dataType = "DbConnectType"),
            @ApiImplicitParam(name = "other", value = "DATA_SOURCE_OTHER", dataType = "String")
    })
    @PostMapping(value = "")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiException(CREATE_DATASOURCE_ERROR)
    public Result createDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @RequestParam("name") String name,
                                   @RequestParam(value = "note", required = false) String note,
                                   @RequestParam(value = "type") DbType type,
                                   @RequestParam(value = "host") String host,
                                   @RequestParam(value = "port") String port,
                                   @RequestParam(value = "database") String database,
                                   @RequestParam(value = "principal") String principal,
                                   @RequestParam(value = "userName") String userName,
                                   @RequestParam(value = "password") String password,
                                   @RequestParam(value = "connectType") DbConnectType connectType,
                                   @RequestParam(value = "other") String other) {
        logger.info("login user {} create datasource name: {}, note: {}, type: {}, host: {}, port: {}, database : {}, principal: {}, userName : {}, connectType: {}, other: {}",
                loginUser.getUserName(), name, note, type, host, port, database, principal, userName, connectType, other);
        String parameter = dataSourceService.buildParameter(name, note, type, host, port, database, principal, userName, password, connectType, other);
        Map<String, Object> result = dataSourceService.createDataSource(loginUser, name, note, type, parameter);
        return returnDataList(result);
    }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * updateProcessInstance data source
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param id datasource id
     * @param dataSourceParam datasource param
=======
     * @param name      data source name
     * @param note      description
     * @param type      data source type
     * @param other     other arguments
     * @param id        data source di
     * @param host      host
     * @param port      port
     * @param database  database
     * @param principal principal
     * @param userName  user name
     * @param password  password
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return update result code
     */
    @ApiOperation(value = "updateDataSource", notes = "UPDATE_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Integer"),
        @ApiImplicitParam(name = "dataSourceParam", value = "DATA_SOURCE_PARAM", required = true, dataType = "BaseDataSourceParamDTO")
    })
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(UPDATE_DATASOURCE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result updateDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @PathVariable(value = "id") Integer id,
                                   @RequestBody BaseDataSourceParamDTO dataSourceParam) {
        dataSourceParam.setId(id);
        return dataSourceService.updateDataSource(dataSourceParam.getId(), loginUser, dataSourceParam);
=======
            @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100"),
            @ApiImplicitParam(name = "name", value = "DATA_SOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "note", value = "DATA_SOURCE_NOTE", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "DB_TYPE", required = true, dataType = "DbType"),
            @ApiImplicitParam(name = "host", value = "DATA_SOURCE_HOST", required = true, dataType = "String"),
            @ApiImplicitParam(name = "port", value = "DATA_SOURCE_PORT", required = true, dataType = "String"),
            @ApiImplicitParam(name = "database", value = "DATABASE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "userName", value = "USER_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "PASSWORD", dataType = "String"),
            @ApiImplicitParam(name = "connectType", value = "CONNECT_TYPE", dataType = "DbConnectType"),
            @ApiImplicitParam(name = "other", value = "DATA_SOURCE_OTHER", dataType = "String")
    })
    @PatchMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(UPDATE_DATASOURCE_ERROR)
    public Result updateDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @PathVariable("id") int id,
                                   @RequestParam("name") String name,
                                   @RequestParam(value = "note", required = false) String note,
                                   @RequestParam(value = "type") DbType type,
                                   @RequestParam(value = "host") String host,
                                   @RequestParam(value = "port") String port,
                                   @RequestParam(value = "database") String database,
                                   @RequestParam(value = "principal") String principal,
                                   @RequestParam(value = "userName") String userName,
                                   @RequestParam(value = "password") String password,
                                   @RequestParam(value = "connectType") DbConnectType connectType,
                                   @RequestParam(value = "other") String other) {
        logger.info("login user {} updateProcessInstance datasource name: {}, note: {}, type: {}, connectType: {}, other: {}",
                loginUser.getUserName(), name, note, type, connectType, other);
        String parameter = dataSourceService.buildParameter(name, note, type, host, port, database, principal, userName, password, connectType, other);
        Map<String, Object> dataSource = dataSourceService.updateDataSource(id, loginUser, name, note, type, parameter);
        return returnDataList(dataSource);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * query data source detail
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param id datasource id
=======
     * @param id        datasource id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return data source detail
     */
    @ApiOperation(value = "queryDataSource", notes = "QUERY_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100")
=======
            @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9

    })
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_DATASOURCE_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result queryDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                  @PathVariable("id") int id) {

=======
    public Result queryDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                  @RequestParam("id") int id) {
        logger.info("login user {}, query datasource: {}",
                loginUser.getUserName(), id);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = dataSourceService.queryDataSource(id);
        return returnDataList(result);
    }

    /**
<<<<<<< HEAD
     * query datasource by type
     *
     * @param loginUser login user
     * @param type data source type
=======
     * query datasouce by type
     *
     * @param loginUser login user
     * @param type      data source type
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return data source list page
     */
    @ApiOperation(value = "queryDataSourceList", notes = "QUERY_DATA_SOURCE_LIST_BY_TYPE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "type", value = "DB_TYPE", required = true, dataType = "DbType")
=======
            @ApiImplicitParam(name = "type", value = "DB_TYPE", required = true, dataType = "DbType")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @GetMapping(value = "/list")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_DATASOURCE_ERROR)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result queryDataSourceList(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                      @RequestParam("type") DbType type) {
        Map<String, Object> result = dataSourceService.queryDataSourceList(loginUser, type.ordinal());
        return returnDataList(result);
    }

    /**
     * query datasource with paging
     *
     * @param loginUser login user
     * @param searchVal search value
<<<<<<< HEAD
     * @param pageNo page number
     * @param pageSize page size
=======
     * @param pageNo    page number
     * @param pageSize  page size
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return data source list page
     */
    @ApiOperation(value = "queryDataSourceListPaging", notes = "QUERY_DATA_SOURCE_LIST_PAGING_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
        @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", required = true, dataType = "Int", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", required = true, dataType = "Int", example = "20")
    })
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_DATASOURCE_ERROR)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
=======
            @ApiImplicitParam(name = "searchVal", value = "SEARCH_VAL", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "PAGE_NO", dataType = "Int", example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "PAGE_SIZE", dataType = "Int", example = "20")
    })
    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(QUERY_DATASOURCE_ERROR)
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    public Result queryDataSourceListPaging(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                            @RequestParam(value = "searchVal", required = false) String searchVal,
                                            @RequestParam("pageNo") Integer pageNo,
                                            @RequestParam("pageSize") Integer pageSize) {
<<<<<<< HEAD
        Result result = checkPageParams(pageNo, pageSize);
        if (!result.checkResult()) {
            return result;
        }
        searchVal = ParameterUtils.handleEscapes(searchVal);
        return dataSourceService.queryDataSourceListPaging(loginUser, searchVal, pageNo, pageSize);
=======
        Map<String, Object> result = checkPageParams(pageNo, pageSize);
        if (result.get(Constants.STATUS) != Status.SUCCESS) {
            return returnDataListPaging(result);
        }
        searchVal = ParameterUtils.handleEscapes(searchVal);
        result = dataSourceService.queryDataSourceListPaging(loginUser, searchVal, pageNo, pageSize);
        return returnDataListPaging(result);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * connect datasource
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param dataSourceParam datasource param
=======
     * @param name      data source name
     * @param note      data soruce description
     * @param type      data source type
     * @param other     other parameters
     * @param host      host
     * @param port      port
     * @param database  data base
     * @param principal principal
     * @param userName  user name
     * @param password  password
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return connect result code
     */
    @ApiOperation(value = "connectDataSource", notes = "CONNECT_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "dataSourceParam", value = "DATA_SOURCE_PARAM", required = true, dataType = "BaseDataSourceParamDTO")
=======
            @ApiImplicitParam(name = "name", value = "DATA_SOURCE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "note", value = "DATA_SOURCE_NOTE", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "DB_TYPE", required = true, dataType = "DbType"),
            @ApiImplicitParam(name = "host", value = "DATA_SOURCE_HOST", required = true, dataType = "String"),
            @ApiImplicitParam(name = "port", value = "DATA_SOURCE_PORT", required = true, dataType = "String"),
            @ApiImplicitParam(name = "database", value = "DATABASE_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "userName", value = "USER_NAME", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "PASSWORD", dataType = "String"),
            @ApiImplicitParam(name = "connectType", value = "CONNECT_TYPE", dataType = "DbConnectType"),
            @ApiImplicitParam(name = "other", value = "DATA_SOURCE_OTHER", dataType = "String")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @PostMapping(value = "/connect")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(CONNECT_DATASOURCE_FAILURE)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result connectDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestBody BaseDataSourceParamDTO dataSourceParam) {
        DatasourceUtil.checkDatasourceParam(dataSourceParam);
        ConnectionParam connectionParams = DatasourceUtil.buildConnectionParams(dataSourceParam);
        return dataSourceService.checkConnection(dataSourceParam.getType(), connectionParams);
=======
    public Result connectDataSource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                    @RequestParam("name") String name,
                                    @RequestParam(value = "note", required = false) String note,
                                    @RequestParam(value = "type") DbType type,
                                    @RequestParam(value = "host") String host,
                                    @RequestParam(value = "port") String port,
                                    @RequestParam(value = "database") String database,
                                    @RequestParam(value = "principal") String principal,
                                    @RequestParam(value = "userName") String userName,
                                    @RequestParam(value = "password") String password,
                                    @RequestParam(value = "connectType") DbConnectType connectType,
                                    @RequestParam(value = "other") String other) {
        logger.info("login user {}, connect datasource: {}, note: {}, type: {}, connectType: {}, other: {}",
                loginUser.getUserName(), name, note, type, connectType, other);
        String parameter = dataSourceService.buildParameter(name, note, type, host, port, database, principal, userName, password, connectType, other);
        Boolean isConnection = dataSourceService.checkConnection(type, parameter);
        Result result = new Result();

        if (isConnection) {
            putMsg(result, SUCCESS);
        } else {
            putMsg(result, CONNECT_DATASOURCE_FAILURE);
        }
        return result;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * connection test
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param id data source id
=======
     * @param id        data source id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return connect result code
     */
    @ApiOperation(value = "connectionTest", notes = "CONNECT_DATA_SOURCE_TEST_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/{id}/connect-test")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(CONNECTION_TEST_FAILURE)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result connectionTest(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @PathVariable("id") int id) {
        return dataSourceService.connectionTest(id);
=======
            @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100")
    })
    @PostMapping(value = "/connect-by-id")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(CONNECTION_TEST_FAILURE)
    public Result connectionTest(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                 @RequestParam("id") int id) {
        logger.info("connection test, login user:{}, id:{}", loginUser.getUserName(), id);

        Boolean isConnection = dataSourceService.connectionTest(loginUser, id);
        Result result = new Result();

        if (isConnection) {
            putMsg(result, SUCCESS);
        } else {
            putMsg(result, CONNECTION_TEST_FAILURE);
        }
        return result;
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    }

    /**
     * delete datasource by id
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param id datasource id
     * @return delete result
     */
    @ApiOperation(value = "deleteDataSource", notes = "DELETE_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100")
=======
     * @param id        datasource id
     * @return delete result
     */
    @ApiOperation(value = "delete", notes = "DELETE_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "DATA_SOURCE_ID", required = true, dataType = "Int", example = "100")
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    })
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(DELETE_DATA_SOURCE_FAILURE)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result delete(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                         @PathVariable("id") int id) {
=======
    public Result delete(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                         @RequestParam("id") int id) {
        logger.info("delete datasource,login user:{}, id:{}", loginUser.getUserName(), id);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        return dataSourceService.delete(loginUser, id);
    }

    /**
     * verify datasource name
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param name data source name
=======
     * @param name      data source name
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return true if data source name not exists.otherwise return false
     */
    @ApiOperation(value = "verifyDataSourceName", notes = "VERIFY_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "name", value = "DATA_SOURCE_NAME", required = true, dataType = "String")
    })
    @GetMapping(value = "/verify-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VERIFY_DATASOURCE_NAME_FAILURE)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result verifyDataSourceName(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                       @RequestParam(value = "name") String name
    ) {
        return dataSourceService.verifyDataSourceName(name);
    }

=======
            @ApiImplicitParam(name = "name", value = "DATA_SOURCE_NAME", required = true, dataType = "String")
    })
    @PostMapping(value = "/verify-name")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(VERIFY_DATASOURCE_NAME_FAILURE)
    public Result verifyDataSourceName(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                       @RequestParam(value = "name") String name
    ) {
        logger.info("login user {}, verfiy datasource name: {}",
                loginUser.getUserName(), name);

        return dataSourceService.verifyDataSourceName(loginUser, name);
    }


>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * unauthorized datasource
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return unauthed data source result code
     */
    @ApiOperation(value = "unauthDatasource", notes = "UNAUTHORIZED_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/unauth-datasource")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(UNAUTHORIZED_DATASOURCE)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result unauthDatasource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @RequestParam("userId") Integer userId) {

=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/unauth")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(UNAUTHORIZED_DATASOURCE)
    public Result unauthDatasource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @RequestParam("userId") Integer userId) {
        logger.info("unauthorized datasource, login user:{}, unauthorized userId:{}",
                loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = dataSourceService.unauthDatasource(loginUser, userId);
        return returnDataList(result);
    }

<<<<<<< HEAD
=======

>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
    /**
     * authorized datasource
     *
     * @param loginUser login user
<<<<<<< HEAD
     * @param userId user id
=======
     * @param userId    user id
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
     * @return authorized result code
     */
    @ApiOperation(value = "authedDatasource", notes = "AUTHORIZED_DATA_SOURCE_NOTES")
    @ApiImplicitParams({
<<<<<<< HEAD
        @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/authed-datasource")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(AUTHORIZED_DATA_SOURCE)
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result authedDatasource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @RequestParam("userId") Integer userId) {

=======
            @ApiImplicitParam(name = "userId", value = "USER_ID", required = true, dataType = "Int", example = "100")
    })
    @GetMapping(value = "/authed")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(AUTHORIZED_DATA_SOURCE)
    public Result authedDatasource(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser,
                                   @RequestParam("userId") Integer userId) {
        logger.info("authorized data source, login user:{}, authorized useId:{}",
                loginUser.getUserName(), userId);
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        Map<String, Object> result = dataSourceService.authedDatasource(loginUser, userId);
        return returnDataList(result);
    }

    /**
     * get user info
     *
     * @param loginUser login user
     * @return user info data
     */
    @ApiOperation(value = "getKerberosStartupState", notes = "GET_USER_INFO_NOTES")
    @GetMapping(value = "/kerberos-startup-state")
    @ResponseStatus(HttpStatus.OK)
    @ApiException(KERBEROS_STARTUP_STATE)
<<<<<<< HEAD
    @AccessLogAnnotation(ignoreRequestArgs = "loginUser")
    public Result getKerberosStartupState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {
=======
    public Result getKerberosStartupState(@ApiIgnore @RequestAttribute(value = Constants.SESSION_USER) User loginUser) {
        logger.info("login user {}", loginUser.getUserName());
>>>>>>> 09b116348af34bf8a76a147d64a28edda0e223a9
        // if upload resource is HDFS and kerberos startup is true , else false
        return success(Status.SUCCESS.getMsg(), CommonUtils.getKerberosStartupState());
    }
}
